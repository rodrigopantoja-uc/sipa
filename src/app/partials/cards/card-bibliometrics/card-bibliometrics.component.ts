import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-bibliometrics',
  templateUrl: './card-bibliometrics.component.html',
  styleUrls: ['./card-bibliometrics.component.css']
})
export class CardBibliometricsComponent implements OnInit {

  @Input() cardData: any;

  constructor() { }

  ngOnInit() {
  }

}
