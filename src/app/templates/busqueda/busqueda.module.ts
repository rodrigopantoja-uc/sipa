import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusquedaRoutingModule } from './busqueda-routing.module';
import { BusquedaComponent } from './busqueda.component';
import { CardPublicationsComponent } from '../../partials/cards/card-publications/card-publications.component';

@NgModule({
  declarations: [
    BusquedaComponent,
    CardPublicationsComponent
  ],
  imports: [
    CommonModule,
    BusquedaRoutingModule
  ]
})
export class BusquedaModule { }
