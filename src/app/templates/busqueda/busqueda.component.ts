import { Component, OnInit, DoCheck, ɵConsole, EventEmitter, Output } from '@angular/core';
import { animate, state, style, transition, trigger }                 from '@angular/animations';
import { ActivatedRoute, Params }             from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router }                             from '@angular/router';
import json                                   from '../../../assets/json/search-advanced/search-advanced.json';
import { QueriesService }                     from '../../services/queries.service';
import { global }                             from '../../services/global';
import { DataService }                        from 'src/app/services/data.service.js';
import { HttpClient }                         from '@angular/common/http';
import { from } from 'rxjs';

declare var jQuery  :any;
declare var $       :any;

@Component({
  selector    : 'app-busqueda',
  templateUrl : './busqueda.component.html',
  styleUrls   : ['./busqueda.component.css'],
  animations  : [
    trigger('slide', [
      state('t1', style({ transform: 'translateX(0)' })),
      state('t2', style({ transform: 'translateX(-25%)' })),
      state('t3', style({ transform: 'translateX(-50%)' })),
      state('t4', style({ transform: 'translateX(-75%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class BusquedaComponent implements OnInit, DoCheck {

  @Output()
  propagar                        = new EventEmitter<Object>();
  public json: any                = json;
  public data: any                = [];
  public urlPhp;
  public urlImg;
  public urlFicha;
  public urldownload;
  public urlHandle;
  public urlvincular;
  public urlGuardar; 
  public urldescargar;
  public urlfile;
  public valorSearch              = "";
  public array_Filtros: any[]     = [];
  public array_resumen: any[]     = [];
  public array_email_search: any  = {
    mensaje   : null,
    nombre    : this.queriesService.getUser(),
    email     : this.queriesService.getToken(),
    archivos  : null
  }
  public filtro: any;
  public registros;
  public totalPage;
  public page;
  public npage: any               = 1;
  public cantidadReg;
  public paginacion               = [];
  public nextPage;
  public prevPage;

  public tipodoc: any             = [];
  public autores: any             = [];
  public tematicas: any           = [];
  public materias: any            = [];

  public facetas_desde;
  public facetas_hasta;
  public orden;
  public entrada;
  public cargando;
  public cargando_solicitar;
  public msj;
  public msj_solicitar;
  public msj_file;
  public condicion_publicar       = null;
  public modal                    = false;
  public modal_ficha              = false;
  public tituloModal;
  public index_ficha;
  public ver_mas=false;
  public array_vincular: any      = [];
  public emailSolicitar           = null;
  public activePane: any          = 't1';
  public msjEmail;
  public msjSend;
  public numenvios                = 0;
  public totalenvios              = 0;
  public pasos                    = false;
  public ordenlista               = 'pubrecientes';

  /* ficha */
  public foto;
  public embargo;
  public metadatos_simple:any     = [];
  public error                    = false;
  public tipodata                 = "sencillo";

  /* email */
  public urlEmail;
  public response: any            = {
    'show': false,
    'data': ''
  }
  public message;
  public usuario                  = this.queriesService.getUser()
  public correo                   = this.queriesService.getToken();
  public name_usuario;
  public codpers;
  public bolsaVincula             = true;
  public guardados;
  public array_btn_vincular: any  = [];
  public num_vinculaciones        = null;
  public myFiles:string []        = [];
  public form_help_search;
  public estado_user_sipa;

  private myTemplate: any = "";

  constructor(
    private queriesService        : QueriesService,
    private activatedRoute        : ActivatedRoute,
    public dataService            : DataService,
    private router                : Router,
    private http                  : HttpClient
  ) { 
    this.urlImg                   = global.php_img      + "/bitstream/handle";
    this.urlPhp                   = global.php          + "/discovery-sipa.php?";
    this.urlEmail                 = global.php          + "/mail-contacto/envio.php?";
    this.urlFicha                 = global.php          + "/ficha.php?";
    this.urlHandle                = global.dominio      + "/handle/";
    this.urldownload              = global.php_download + "/bitstream/handle";
    this.urlvincular              = global.php          + "/vinculacion/VincularSipaWos.php"; 
    this.urlGuardar               = global.php          + "/vinculacion/mis_publicaciones.php"; 
    this.urldescargar             = global.php          + "/vinculacion/descarga.php"; 
  }
  
  ngOnInit() {

    /* this.queriesService.getRefresh(); */

    /* USUARIOS SIN CODPERS */
    if(localStorage.getItem('estado_user_sipa') == "false"){
      this.router.navigate(['/mispublicaciones/confirmado']);
    }
  

    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);

    /* VALORIZACION */
    var URLactual = window.location.href;
    this.json["rating"]['star'].pagina = URLactual

    /* Datos usuario perfil Desde CAS */
    let split                 = this.usuario.split(',');
    this.name_usuario         = split[1];
    if(this.queriesService.getjsonAcademico()){
      let perfil              = JSON.parse(this.queriesService.getjsonAcademico());
      this.codpers            =  perfil.COD_PERS;
    }

    /* Declainer */
    this.dataService.entrada  = false;
    this.tituloModal          = "Términos y condiciones del servicio";
    if(localStorage.getItem("declainer")){
      this.condicion_publicar = localStorage.getItem("declainer");
      this.modal              = false;
    }else{
      this.condicion_publicar = null;
      this.modal              = true;
    }

    this.data['filtroActivo'] = false;
    this.npage                = 1;
    this.entrada              = false;

    /* Bolsa de vinculaciones */
    if(localStorage.getItem("vinculaciones")){
      this.num_vinculaciones  = JSON.parse(localStorage.getItem("vinculaciones")).length;
      this.array_vincular     = JSON.parse(localStorage.getItem("vinculaciones"));
    }
    var altura                = 600;   
    $(window).on('scroll', function(){
      if ( $(window).scrollTop() > altura && $(window).scrollTop()<document.body.offsetHeight-1500 && screen.width > 1200){
          $('.mainBolsa').addClass('mainBolsa-fixed');
          $('.headerBolsa').addClass('headerBolsa-fixed');
          $('.footerBolsa').addClass('footerBolsa-fixed');
      } else {
          $('.mainBolsa').removeClass('mainBolsa-fixed');
          $('.headerBolsa').removeClass('headerBolsa-fixed');
          $('.footerBolsa').removeClass('footerBolsa-fixed');
      }
    });

    this.mis_guardados();

  }

  ngDoCheck(){

  }

  toScroll(){
    document.getElementById('pasos').scrollIntoView({behavior: 'smooth'});
  }

  refresh(){
    this.queriesService.getRefresh();
  }

  seleccionarArchivo(event) {
    this.msj_file                       = null;
    /* Cargo archivos en array myFiles */
    if(!this.array_email_search.archivos){
      this.array_email_search.archivos  = [];
    }

    for (var i = 0; i < event.target.files.length; i++) {    
      if(event.target.files[i].size > 10000000){
        this.msj_file                   = "El archivo debe ser inferior a 10mb";
      }else{
        this.myFiles.push(event.target.files[i]);
        this.array_email_search.archivos.push({
          archivo: event.target.files[i].name
        });
      }
    }
  }

  removeFile(i: number) {
    this.array_email_search.archivos.splice(i, 1);
    this.myFiles.splice(i, 1);
    if(this.array_email_search.archivos.length == 0){
      this.array_email_search.archivos = null;
    }
  }

  resetFile(){
    for (var i = 0; i < this.array_email_search.archivos.length; i++) { 
      this.array_email_search.archivos[i].archivo = null;
    }
  }

  placeholder(){
      $('.label-default').addClass('label-top');
  }

  menu(event){
    this.pasos          = false;
    this.data['search'] = false;
    this.bolsaVincula   = false;
  }

  condiciones(event){
    if(event.target.checked){
      this.condicion_publicar = event.target.value;
      localStorage.setItem("declainer",this.condicion_publicar );
    }else{
      this.condicion_publicar = null;
      localStorage.removeItem("declainer");
      this.modal              = true;
    }
  }

  abreModal(){
    this.modal = true;
  }

  cerrarModal(){
    this.condicion_publicar = "condiciones";
    localStorage.setItem("declainer",'condiciones' );
    this.modal              = false;
  }

  abreModal_ficha(i,handle, titulo){
    this.modal_ficha = true;
    this.index_ficha = i;
    this.tituloModal = "";

    this.queriesService.queryGet(this.urlFicha +'handle=' + handle).then((data) => {
      if(data['code'] == 404){
        window.location.href=global.php+'/cas-log/cas-login.php';
      }else{
        if(data['response']['docs'].length>0){
          this.error                        = false;
          this.data['publics']              = data['response']['docs'][0];

          // Cambiar titulo por alternativo
          this.data['publics']['title'][0]  = titulo;

          //data['miniatura'];
          if(data['miniatura']){
            this.foto                       = this.urlImg +"/"+data['response']['docs'][0]['handle']+"/"+data['miniatura'];
          }else{
            this.foto                       = null;
          }

          // Archivo en embargo
          if(data['embargo'] == 200 || data['embargo'] == 201){
            this.embargo                    = false;
          }else{
            this.embargo                    = true;
          }

          // Construye tabla metadatos
          let list             = this.data['publics'];
          for(let i in list){
            if (i.substr(0,3)  == 'dc.' && i.indexOf('_') == -1){  
              /* cambiar nombres */
              var alias        = i;
              var link         = false;
              var mostrar      = true;
              
              if(i == "dc.areadearte"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.concurso"){
                alias          = "Concurso Arte y Cultura"; 
                link           = false;
              } 
              if(i == "dc.conservacion.estado"){
                alias          = "Conservación"; 
                link           = false;
              }    
              if(i == "dc.contributor"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.contributor.advisor"){
                alias          = "Profesor guía"; 
                link           = false;
              } 
              if(i == "dc.contributor.author"){
                alias          = "Autor";
                link           = false;
              } 
              if(i == "dc.contributor.other"){
                alias          = "Otro autor"; 
                link           = false;
              }  
              if(i == "dc.contributor.editor"){
                alias          = "Editor";
                link           = false;
              }  
              if(i == "dc.contributor.illustrator"){
                alias          = "Ilustrador";
                link           = false;
              }  
              if(i == "dc.coverage.spatial"){
                alias          = "Cobertura geográfica";
                link           = false;
              } 
              if(i == "dc.coverage.temporal"){
                alias          = "Cobertura temporal";
                link           = false;
              } 
              if(i == "dc.creator"){
                alias          = "nulo";
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.cultura"){
                alias          = "Cultura";
                link           = false;
              } 
              if(i == "dc.date"){
                alias          = "Fecha de embargo";
                link           = false;
                mostrar        = false;
              }
              if(i == "dc.date.accessioned"||i == "dc.date.available" || i == "dc.date.submitted"){ /* eliminar ??? */
                alias          = "nulo";
                link           = false;
                mostrar        = false;
              }
              if(i == "dc.date.concesion"){
                alias          = "Fecha de concesión";
                link           = false;
              }
              if(i == "dc.date.copyright"){
                alias          = "nulo";
                link           = false;
                mostrar        = false;
              }
              if(i == "dc.date.created"){
                alias          = "Fecha de creación";
                link           = false;
              }
              if(i == "dc.date.issued"){
                alias          = "Fecha de publicación";
                link           = false;
              }
              if(i == "dc.date.updated"){
                alias          = "nulo";
                link           = false;
                mostrar        = false;
              }
              if(i == "dc.description"){
                alias          = "Nota"; 
                link           = false;
              } 
              if(i == "dc.description.abstract"){
                alias          = "Resumen"; 
                link           = false;
              } 
              if(i == "dc.description.funder"){
                alias          = "Financiamiento"; 
                link           = false;
              } 
              if(i == "dc.description.provenance"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.description.sponsorship"){
                alias          = "Patrocinio"; 
                link           = false;
              } 
              if(i == "dc.description.statementofresponsibility"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.description.tableofcontents"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.description.uri"){
                alias          = "Video"; 
                link           = true;
              } 
              if(i == "dc.description.version"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.destinatario"){
                alias          = "Destinatario"; 
                link           = false;
              } 
              if(i == "dc.donante"){
                alias          = "Donante"; 
                link           = false;
              } 
              if(i == "dc.estado.patente"){
                alias          = "Estado de patentamiento"; 
                link           = false;
              } 
              if(i=="dc.estamento1" ||i=="dc.estamento2" ||i=="dc.estamento3" ||i=="dc.estamento4"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i=="dc.estamento.1" ||i=="dc.estamento.2" ||i=="dc.estamento.3" ||i=="dc.estamento.4"){
                alias          = "nulo"; 
                link           = false;
              } 
              if(i == "dc.format"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.format.extent"){
                alias          = "Paginación"; 
                link           = false;
              } 
              if(i == "dc.format.medium"){
                alias          = "Medio"; 
                link           = false;
              } 
              if(i == "dc.format.mimetype"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.fuente.origen"){ 
                alias          = "Fuente"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.funcion"){
                alias          = "Función"; 
                link           = false;
              } 
              if(i == "dc.identifier"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i=="dc.identifier.codUA"||i=="dc.identifier.codUA1"||i=="dc.identifier.codUA2"||i=="dc.identifier.codUA3"||i=="dc.identifier.codUA4"||i=="dc.identifier.codUA5"||i=="dc.identifier.codUA6"||i=="dc.identifier.codUA7"||i=="dc.identifier.codUA8"||i=="dc.identifier.codUA9"||i=="dc.identifier.codUA10"){
                alias          = "Unidad académica"; 
                link           = false;
              } 
              if(i == "dc.identifier.citation"){
                alias          = "Cómo citar este documento"; 
                link           = false;
              } 
              if(i == "dc.identifier.concesion"){
                alias          = "Número de concesión"; 
                link           = false;
              } 
              if(i == "dc.identifier.converisid"){
                alias          = "Id de publicación en Converis"; 
                link           = false;
              } 
              if(i == "dc.identifier.dialnetid"){
                alias          = "Id de publicación en Dialnet"; 
                link           = false;
              } 
              if(i == "dc.identifier.doi"){
                alias          = "DOI"; 
                link           = true;
              } 
              if(i == "dc.identifier.eisbn"){
                alias          = "ISBN electrónico"; 
                link           = false;
              } 
              if(i == "dc.identifier.eissn"){
                alias          = "ISSN electrónico"; 
                link           = false;
              } 
              if(i == "dc.identifier.govdoc"){
                alias          = "Número de solicitud"; 
                link           = false;
              } 
              if(i == "dc.identifier.isbn"){
                alias          = "ISBN"; 
                link           = false;
              } 
              if(i == "dc.identifier.ismn"){
                alias          = "nulo"; 
                link           = false;
                mostrar        = false;
              } 
              if(i == "dc.identifier.issn"){
                alias          = "ISSN";
                link           = false; 
              } 
              if(i == "dc.identifier.other"){
                alias          = "Identificador del recurso";
                link           = false; 
              }
              if(i == "dc.identifier.pubmedid"){
                alias = "Id de publicación en Pubmed";
                link = false; 
              } 
              if(i == "dc.identifier.scieloid"){
                alias = "Id de publicación en Scielo";
                link = false; 
              } 
              if(i == "dc.identifier.scopusid"){
                alias = "Id de publicación en Scopus";
                link = false; 
              } 
              if(i == "dc.identifier.sici"){
                alias = "nulo";
                link = false; 
                mostrar = false;
              } 
              if(i == "dc.identifier.slug"){
                alias = "nulo";
                link = false; 
                mostrar = false;
              } 
              if(i == "dc.identifier.UA"){
                alias = "Unidad Académica";
                link = false; 
              } 
              if(i == "dc.issue.numero"){
                alias = "Número de publicación"; 
                link = false;
              }
              if(i == "dc.identifier.orcid"||i == "dc.identifier.orcid1"||i == "dc.identifier.orcid2"||i == "dc.identifier.orcid3"||i == "dc.identifier.orcid4"||i == "dc.identifier.orcid5"||i == "dc.identifier.orcid6"||i == "dc.identifier.orcid7"||i == "dc.identifier.orcid8"||i == "dc.identifier.orcid9"||i == "dc.identifier.orcid10"){
                alias = "Número ORCID";
                link = false;
              } 
              if(i == "dc.identifier.uri"){
                alias = "Enlace";
                link = true;
              } 
              if(i == "dc.identifier.wosid"){
                alias = "Id de publicación en WoS";
                link = false;
              } 
              if(i == "dc.information.autoruc"){
                alias = "Información del autor UC";
                link = false;
                mostrar = false;
              } 
              if(i == "dc.language"){
                alias = "Idioma";
                link = false;
                mostrar = false;
              } 
              if(i == "dc.language.iso"){
                alias = "nulo"; // Código ISO del idioma de la publicación
                link = false;
              } 
              if(i == "dc.language.rfc3066"){
                alias = "nulo";
                link = false;
                mostrar = false;
              } 
              if(i == "dc.localidad"){
                alias = "Localidad";
                link = false;
              } 
              if(i == "dc.lugar.publicacion"){
                alias = "Lugar de publicación";
                link = false;
              } 
              if(i == "dc.material"){
                alias = "Material";
                link = false;
              } 
              if(i == "dc.nota"){
                alias = "nulo";
                link = false;
                mostrar = false;
              } 
              if(i == "dc.nota.acceso"){
                alias = "nulo";
                link = false;
                mostrar = false;
              } 
              if(i == "dc.pagina.final"){
                alias = "Página final"; 
                link = false;
              } 
              if(i == "dc.pagina.inicio"){
                alias = "Página inicio"; 
                link = false;
              } 
              if(i == "dc.pagina.pais"){
                alias = "Pais"; 
                link = false;
              } 
              if(i == "dc.pais"){
                alias = "Pais"; 
                link = false;
              } 
              if(i == "dc.presentacion"){
                alias = "Tipo de presentación de patente"; 
                link = false;
              } 
              if(i == "dc.provenance"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.publisher"){
                alias = "Editorial"; 
                link = false;
              } 
              if(i == "dc.region"){
                alias = "Región"; 
                link = false;
              } 
              if(i == "dc.relation"){
                alias = "Publicación relacionada"; 
                link = false;
              }
              if(i == "dc.relation.haspart"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              }
              if(i == "dc.relation.hasversion"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              }
              if(i == "dc.relation.isbasedon"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              }
              if(i == "dc.relation.isformatof"){
                alias = "Se encuentra en"; 
                link = false;
              } 
              if(i == "dc.relation.ispartofseries"){
                alias = "Serie"; 
                link = false;
              } 
              if(i == "dc.relation.isreferencedby"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.relation.isreplacedby"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.relation.isversionof"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.relation.replaces"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.relation.requires"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.relation.ispartof"){
                alias = "Publicado en / Colección"; 
                link = false;
              } 
              if(i == "dc.revista"){
                alias = "Revista"; 
                link = false;
              } 
              if(i == "dc.rights"){
                alias = "Derechos"; 
                link = false;
              } 
              if(i == "dc.rights.access"){
                alias = "Acceso"; 
                link = false;
              } 
              if(i == "dc.rights.holder"){
                alias = "Titular de los derechos"; 
                link = false;
              } 
              if(i == "dc.rights.license"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.rights.uri"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.rubro"){
                alias = "Rubro"; 
                link = false;
              } 
              if(i == "dc.solicitante"){
                alias = "Nombre de solicitante"; 
                link = false;
              } 
              if(i == "dc.source"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.source.uri"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.relation.uri"){
                alias = "Enlace relacionada";
                link = true;
              } 
              if(i == "dc.subject.classification"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.subject.ddc"){
                alias = "Número de clasificación Dewey"; 
                link = false;
              } 
              if(i == "dc.subject.dewey"){
                alias = "Temática"; 
                link = false;
              } 
              if(i == "dc.subject.lcc"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.subject.lcsh"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.subject.mesh"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.subject.other"){
                alias = "Materia"; 
                link = false;
              } 
              if(i == "dc.tecnica"){
                alias = "Técnica"; 
                link = false;
              } 
              if(i == "dc.territorialidad"){
                alias = "Territorialidad"; 
                link = false;
              } 
              if(i == "dc.subject"){
                alias = "Palabra clave"; 
                link = false;
              } 
              if(i == "dc.title.alternative"){
                alias = "Otro título"; 
                link = false;
              } 
              if(i == "dc.title"){
                /* list[i] = title;
                i = "title"; */
                
                alias = "Título"; 
                link = false;
              } 
              if(i == "dc.type"){
                alias = "Tipo de documento"; 
                link = false;
              } 
              if(i == "dc.type.qualificationlevel"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "dc.ubicacion"){
                alias = "Ubicación"; 
                link = false;
              } 
              if(i == "dc.uc.hito"){
                alias = "Hito UC"; 
                link = false;
              } 
              if(i == "dc.volumen"){
                alias = "Volumen"; 
                link = false;
              } 
              if(i == "dc.zcode"||i == "dc.zcode.1" || i == "dc.zcode.2" || i == "dc.zcode.3"|| i == "dc.zcode.4"|| i == "dc.zcode.5"|| i == "dc.zcode.6"|| i == "dc.zcode.7"|| i == "dc.zcode.8"|| i == "dc.zcode.9"|| i == "dc.zcode.10"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 
              if(i == "sipa.fechainicio"){
                alias = "Fecha de inicio"; 
                link = false;
              } 
              if(i == "sipa.fechatermino"){
                alias = "Fecha de término"; 
                link = false;
              } 
              if(i == "sipa.index"){
                alias = "Indización"; 
                link = false;
              } 
              if(i == "sipa.afi.uc"||i == "sipa.codpersvinculados" || i == "sipa.fecha.validacionbiblio" || i == "sipa.fecha.validavinculacion"|| i == "sipa.fecha.vinculacion"|| i == "sipa.identifier.ha"|| i == "sipa.identifier.propintelectual"|| i == "sipa.identifier.repositoriouc"|| i == "sipa.identifier.solicpropintelectual"|| i == "sipa.trazabilidad"|| i == "sipa.validacionbiblio"){
                alias = "nulo"; 
                link = false;
                mostrar = false;
              } 

              if(i == "dc.title"){
                this.metadatos_simple.push( { "metadato": alias, "cod":'title', "valor": [titulo], "link":link, "mostrar":mostrar });
              }else{
                this.metadatos_simple.push( { "metadato": alias, "cod":i, "valor": list[i], "link":link, "mostrar":mostrar });
              }
              
              
            }
          }

          this.metadatos_simple.sort((a, b) => (a.metadato > b.metadato) ? 1 : -1);

        }else{
          this.error = true;
        }
      }
      
    });

  }

  cerrarModal_ficha(){
    this.metadatos_simple = [];
    this.modal_ficha      = false;
  }
  cerrarModal_declainer(){
    this.modal = false;
  }
  metadatas(reg){
    this.tipodata = reg;
  }

  
  registra(){
    this.propagar.emit("registrar");
  }

  buscar(page,filtro, orden) {
    document.getElementById('buscador').scrollIntoView({behavior: 'smooth'});
    this.cargando             = true;
    this.msj                  = null;
    this.dataService.entrada  = true;
    let str_filtros:any       = '';
    let urlFiltro;
    this.npage                = page;
    localStorage.setItem('page',page );
    page                      = (page-1)*20;
    this.orden                = orden;

    // SI VIENE DE ESTRA PÁGINA
    if(filtro=="busqueda"){
      localStorage.setItem('search_form',this.valorSearch);
    }
    // SI VIENE DE HOME AREAS TEMATICAS Y PAGINAS TESIS Y PUBLICACIONES
    if(localStorage.getItem('searchAdvanced') == 'true'){
      this.data['title']        = 'Búsqueda avanzada';
      this.data['filtroActivo'] = true;
      this.array_Filtros        = JSON.parse(localStorage.getItem('json_filtros'));
      localStorage.removeItem('searchAdvanced');
    }
    
    this.valorSearch            = localStorage.getItem('search_form');
    localStorage.setItem('json_filtros',JSON.stringify(this.array_Filtros));

    // RECORRO ARRAY array_Filtros DE FILTROS
    for(let i=0; i < this.array_Filtros.length; i++){
      this.array_Filtros[i]['term'] =  this.array_Filtros[i]['term'];
      let termm                     = this.array_Filtros[i]['term'];
      let term                      = termm.replace(".", "").replace(":", "").replace("-", " ");
      // Construyo cadena de filtros
      switch (this.array_Filtros[i]['search_by']) {

        case 'titulo':
          
          if(this.array_Filtros[i]['contains']== 'no-contiene'){
            /* this.filtro = 'fq=-title:"'+ term +'"%26'; */
            /* this.filtro = 'fq=-dc.title:'+ term +'*%26'; */
            /* this.filtro = 'fq=-title:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=-dc.title:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'contiene'){
            /* this.filtro = 'fq=title:"'+this.array_Filtros[i]["term"] +'"%26'; */
            this.filtro = 'fq=dc.title:'+ term +'%26';
            /* this.filtro = 'fq=title:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
          }
          if(this.array_Filtros[i]['contains']== 'es'){
            this.filtro = 'fq=dc.title:"'+ termm +'"%26';
            /* this.filtro = 'fq=title:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=title:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            /* this.filtro = '"fq=title:'+this.array_Filtros[i]["term"] +'*"%26'; */
          }
        break;

        case 'autor':
          if(this.array_Filtros[i]['contains']== 'no-contiene'){
            /* this.filtro = 'fq=-dc.contributor.author:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=-dc.contributor.author:'+ term +'*%26'; */
            /* this.filtro = 'fq=-dc.contributor.author:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=-dc.contributor.author:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'contiene'){
            /* this.filtro = 'fq=dc.contributor.author:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=dc.contributor.author:'+ term +'*%26'; */
            /* this.filtro = 'fq=dc.contributor.author:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=dc.contributor.author:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'es'){
            this.filtro = 'fq=bi_2_dis_value_filter:"'+ termm +'"%26';
            /* this.filtro = 'fq=bi_2_dis_value_filter:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=bi_2_dis_value_filter:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
          }
        break;
        
        case 'fecha':
          if(this.array_Filtros[i]['contains']== 'no-contiene'){
            /* this.filtro = 'fq=-dateIssued:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=-dateIssued:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=-dateIssued:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=-dateIssued:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'contiene'){
            /* this.filtro = 'fq=dateIssued:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=dateIssued:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=dateIssued:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=dateIssued:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'es'){
            this.filtro = 'fq=dateIssued:"'+ termm +'"%26';
            /* this.filtro = 'fq=dateIssued:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=dateIssued:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
          }
        break;

        case 'tema':
          if(this.array_Filtros[i]['contains']== 'no-contiene'){
            /* this.filtro = 'fq=-dc.subject.dewey:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=-dc.subject.dewey:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=-dc.subject.dewey:'+ term +'* OR' +' dc.subject.other:'+ term +'*%26';  */ 
            /* this.filtro = 'fq=-dc.subject.dewey:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=-dc.subject.dewey:'+ term + ' OR ' +'dc.subject.other:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'contiene'){
            /* this.filtro = 'fq=dc.subject.dewey:"'+ term +'"%26'; */
            /* this.filtro = 'fq=dc.subject.dewey:'+ term +'*%26'; */
            /* this.filtro = 'fq=dc.subject.dewey:'+ term +'* OR' +' dc.subject.other:'+ term +'*%26'; */
            /* this.filtro = 'fq=dc.subject.dewey:\"'+ term +'\"%26'; */
            this.filtro = 'fq=dc.subject.dewey:'+ term + ' OR ' +'dc.subject.other:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'es'){
            this.filtro = 'fq=bi_4_dis_value_filter:"'+ termm +'"%26';
            
            /* this.filtro = 'fq=bi_4_dis_value_filter:'+ term +'*%26'; */
            /* this.filtro = 'fq=bi_4_dis_value_filter:\"'+ term +'\"%26'; */
          }
        break;

        case 'tipo':
          if(this.array_Filtros[i]['contains']== 'no-contiene'){
            /* this.filtro = 'fq=-dc.type:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=-dc.type:'+ term +'*%26'; */
            /* this.filtro = 'fq=-dc.type:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=-dc.type:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'contiene'){
            /* this.filtro = 'fq=dc.type:"'+this.array_Filtros[i]["term"] +'"%26'; */
            /* this.filtro = 'fq=dc.type:'+ term +'*%26'; */
            /* this.filtro = 'fq=dc.type:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
            this.filtro = 'fq=dc.type:'+ term +'%26';
          }
          if(this.array_Filtros[i]['contains']== 'es'){
            this.filtro = 'fq=dc.type:"'+ termm +'"%26';
            /* this.filtro = 'fq=dc.type:'+this.array_Filtros[i]["term"] +'*%26'; */
            /* this.filtro = 'fq=dc.type:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
          }
        break;


      } 
      str_filtros=str_filtros + this.filtro;
    }
    urlFiltro = this.urlPhp + 'filtro=' + str_filtros +  '&valor=' + localStorage.getItem('search_form') + '&start=' + this.npage + '&orden=' + this.orden + '&email=' + this.correo + '&codpers=' + this.codpers;


    // ++++++++++++++++++ ENVÍO CONSULTA API Y DEVUELVE REGISTROS ++++++++++++++++++++++


    this.queriesService.queryGet( urlFiltro )
      .then((data) => { 
        this.cargando = false; console.log(data)

        if(data['code'] == 404){
          window.location.href=global.php+'/cas-log/cas-login.php';
        }else{
          this.data['search']= data['Doc'];
          
          if(this.data['search'].length>0 ){
            this.array_btn_vincular=[];
            if(localStorage.getItem("vinculaciones")){
              this.bolsaVincula = true;
              this.array_vincular=JSON.parse(localStorage.getItem("vinculaciones"));
              for(let i=0; i<this.data['search'].length; i++){
                for(let a=0; a < this.array_vincular.length; a++){
                  if(this.data['search'][i].handle==this.array_vincular[a].handle){
                    var btn = true
                    break;
                  }else{
                    var btn = false
                  }
                }
                this.array_btn_vincular.push({
                  btn: btn
                });
              }
            }else{
              for(let i=0; i<this.data['search'].length; i++){
                this.array_btn_vincular.push({
                  btn: false
                });
              }
            }

          }else{
            this.msj="No existen registros con ese criterio de búsqueda.";
          }

          // PAGINACIÓN
          this.totalPage = data['RecordsFound'];
          this.cantidadReg = 20;
          this.page  = Math.ceil(this.totalPage / this.cantidadReg); 
          
          
          /* console.log(this.totalPage + " registros");
          console.log(this.page + " páginas" );
          console.log('pagina cada '+page)
          console.log('página atual: '+this.npage);
          console.log(this.nextPage); */
          this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
          for(let i=1; i<=this.page; i++){
            if(i <= 5){
              if(this.npage>5){
                this.paginacion.push(i+(this.npage-5));
              }else{
                this.paginacion.push(i);
              }
            }
          }
          if(this.npage>=2){
            this.prevPage = this.npage-1;
          }else{
            this.prevPage = 1;
          }
          if(this.npage<this.page){
            this.nextPage = this.npage+1;
          }else{
            this.nextPage = this.page;
          }
          // Fin paginación
        }
    }, err => {
      this.cargando = false;
      this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
      console.log(err);
    });


  }
  busquedaAvanzada(){
    this.data['filtroActivo'] = true;
    this.data['title']        = 'Búsqueda avanzada';
    this.addFiltros();
  }
  addFiltros() {  
    this.array_Filtros.push({
      search_by : '',
      contains  : '',
      term      : ''
    });
  }
  removefiltros(i: number) {
    this.array_Filtros.splice(i, 1);
    if(this.array_Filtros.length == 0){
      this.data['filtroActivo'] = false;
      this.data['title']        = 'Búsqueda simple';
    }
  }

  /* Desde menu facetas despliega todas las facetas */
  searchFacetas(param, num){
    this.data['listFacetas'] = true;
    this.data['search'] = false;
    if(param=='tipodoc'){
      this.data['facetas'] = this.tipodoc;
      this.data['tipo'] = 'tipo';
    }
    if(param=='autores'){
      this.data['facetas'] = this.autores;
      this.data['tipo'] = 'autor';
    }
    if(param=='tema'){
      this.data['facetas'] = this.tematicas;
      this.data['tipo'] = param;
    }

                  // PAGINACIÓN
                  this.totalPage = this.data['facetas'].length;
                  this.cantidadReg = 50;
                  this.npage = num;
                  num = (num-1)*20;
                  this.page  = Math.ceil(this.totalPage / this.cantidadReg); 
                  this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
                  this.facetas_desde= (this.npage-1) * this.cantidadReg;
                  this.facetas_hasta = (this.npage) * this.cantidadReg;

                  for(let i=1; i<=this.page; i++){
                    if(i <= 5){
                      if(this.npage>5){
                        this.paginacion.push(i+(this.npage-5));
                      }else{
                        this.paginacion.push(i);
                      }
                    }
                  }
                  if(this.npage>=2){
                    this.prevPage = this.npage-1;
                  }else{
                    this.prevPage = 1;
                  }
                  if(this.npage<this.page){
                    this.nextPage = this.npage+1;
                  }else{
                    this.nextPage = this.page;
                  }
                  // Fin paginación
  }
  /* Desde lista de resutados de facetas despliega lista de resultados de documentos */
  getFacetas(faceta,item){
    this.data['filtroActivo'] = true;
    this.data['title'] = 'Búsqueda avanzada';
    let splits = faceta.split('|||'); // string pasa a array si Separador es |||
    if(splits.length > 1) {
      faceta = splits.pop(); // elimina último elemento de un array
    }
    this.array_Filtros.push({
      search_by: item,
      contains: 'es',
      term: faceta
    });
    this.buscar(1,item, 'asc');
  }

  // GUARDADOS SIN ENVIAR
  mis_guardados(){ 
    this.cargando   = true;
    this.msj        = null;
    var acciones;
    const formData  = new FormData();
    acciones        = {accion: "consultar", id: "", email: this.correo, validaBiblio:"", start: "", handle: ""}
    formData.append('array_acciones', JSON.stringify(acciones));
    
    /* +++++ CARGAR EN BBDD ++++ */
      
    this.http.post(this.urlGuardar, formData)
      .subscribe(response => {
        this.cargando           = false;
        if(response['code'] == 404){ /* Verifica sesion */
          window.location.href  = global.php+'/cas-log/cas-login.php';
        }else{
          if(response){ 
            this.guardados      = response;
            this.guardados      = this.guardados.length;
          }else{
            this.msj            = "No existen documentos guardados"
          }
        }
    }, err => {
      this.cargando             = false;
      this.msj                  = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
      console.log(err);
    })

  }

  /* Solicitar contacto */
  submitContact(form, $event, title, handle) {
    $event.preventDefault();

    if(form.form.value){
      this.response.show = true;
      /* var datos = "<h3>" + this.usuario + " ha enviado una solicitud de corrección para la publicación: <br><a href='" + this.urlHandle + handle + " '>" + title + "</a> <br><br>Handle:<br>" +handle+ "</h3>";
      this.message = datos + "<h3>Mensaje: " + this.message + "</h3>"; */
      console.log(this.urlEmail+'nombreApellido='+this.usuario+'&mail='+this.correo+'&consulta='+this.message+'&handle='+this.urlHandle+handle+'&titulo='+title+'&tipo=correccion')
      this.queriesService.queryGet(this.urlEmail+'nombreApellido='+this.usuario+'&mail='+this.correo+'&consulta='+this.message+'&handle='+this.urlHandle+handle+'&titulo='+title+'&tipo=correccion').then(
      (data) => {
        if(data['respuesta'] == true){
          this.response.show = true;
          this.response.text = 'Mensaje enviado';
          this.response.icono = 'check_circle';
          this.response.color = 'color-verde';
          //form.form.reset();
          this.message = "";
        }else{
          //console.log(data);
          this.response.show = true;
          this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
          this.response.icono = 'highlight_off';
          this.response.color = 'texto-dg-rojo';
        }
      },
      (error) => {
        console.log(error);
        this.response.show = true;
        this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
        this.response.icono = 'highlight_off';
        this.response.color = 'texto-dg-rojo';
      }
    );
    }
  }

  vincular(data,titulo,autores, fecha, tag, revista, capitulo, handle, event, i){

    if(event.target.checked){
      this.array_vincular.push({
        idpost    : data,
        fuente    : "SIPA",
        emailuser : this.correo,
        tag: tag,
        titulo: titulo,
        autores: autores,
        fecha_publicacion: fecha,
        revista: revista,
        capitulo: capitulo,
        handle: handle,
        pag: this.npage,
        indice: i
      });
      localStorage.setItem("vinculaciones",JSON.stringify(this.array_vincular));
      this.bolsaVincula = true;
      
    }else{
      for(var e = 0; this.array_vincular.length>e; e++){  
        if(this.array_vincular[e].handle==event.target.value){
          this.array_vincular.splice(e, 1);
        }  
      }
      if(this.array_vincular.length == 0){
        localStorage.removeItem("vinculaciones");
        this.bolsaVincula = false;
      }else{
        localStorage.setItem("vinculaciones",JSON.stringify(this.array_vincular));
        this.bolsaVincula = true;
      }
    }
    this.num_vinculaciones = this.array_vincular.length;
  }

  eliminaBolsa(i:number, handle){
    this.array_vincular.splice(i, 1);
    this.bolsaVincula = false;

    if(this.array_vincular.length==0){
      localStorage.removeItem("vinculaciones");
      /* this.bolsaVincula = false; */
      /* for(let i=0; i<this.data['search'].length; i++){
        this.array_btn_vincular.push({
          btn: false
        });
      } */
    }else{
      localStorage.setItem("vinculaciones",JSON.stringify(this.array_vincular));
      /* this.bolsaVincula = true; */
    }
    this.num_vinculaciones = this.array_vincular.length;
    this.buscar(this.npage,'', '1');

  }

  bolsaVinculaciones(){
    this.pasos = true
    this.bolsaVincula = false;
    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);
  }
  seguirBuscando(){
    
    this.pasos = false; 
    this.toScroll(); 
    this.bolsaVincula=true
    this.buscar(this.npage,'busqueda','1'); 
  }

  // +++++++++++ ENVIO EMAIL FORM +++++++++++++++
  submitEmailSearch(form){ 
    this.cargando_solicitar = true;
    this.form_help_search = "resultado";
    const formData = new FormData();

        let limpiar_array = this.array_email_search;
        for(let i in limpiar_array){
          if(limpiar_array[i] == null || limpiar_array[i] == ''){
            delete limpiar_array[i];
          }
        }
        console.log(JSON.stringify(limpiar_array))

        /* Entrego archivos */
        for (var i = 0; i < this.myFiles.length; i++) { 
          formData.append("file[]", this.myFiles[i]);
        }
        formData.append('array_datos_email', JSON.stringify(limpiar_array));

        /* +++++ ENVIAR A API ++++ */
        this.http.post(this.urlEmail, formData)
        .subscribe(response => {
          this.cargando_solicitar = false;
          console.log(response['respuesta']);
          if(response['respuesta'] == true){ // response['httpCode'] == "200" || response['httpCode'] == "201"
                  /* form.reset(); */
                  this.msj_solicitar = "success";
                  this.msjSend = "Proceso exitoso"
                  this.array_email_search.archivos = [];
                  console.log("trajo resultados");
                  console.log(response);
          }else{
            this.msj_solicitar = "error";
            console.log("se conectó pero no trajo httpCode 201");
            console.log(response)
          }
        }, err => {
          this.msj_solicitar = "error";
          this.cargando_solicitar = false;
          console.log("¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde");
          console.log(err);
        })
        
      
  }

  submitPublica(form){ 
    this.cargando = true;
    const formData = new FormData();
    this.totalenvios = this.array_vincular.length;
    this.array_resumen = [];
    this.numenvios = 0;

      for(let i=0; i < this.array_vincular.length; i++){
          console.log(this.urlvincular)
          console.log(this.array_vincular[i]);
           

          formData.append('array_vincular', JSON.stringify(this.array_vincular[i]));
          this.http.post(this.urlvincular, formData)
          .subscribe(response => {
            //console.log(response);
            if(response['code'] == 404){ /* Verifica sesion */
              window.location.href=global.php+'/cas-log/cas-login.php';
            }else{
              this.array_resumen.push({
                titulo:         this.array_vincular[i].titulo,
                handle:         this.array_vincular[i].handle,
                httpCode:       response['httpCode'],
                validacion:     response['validacion']
              });
              //console.log(this.array_resumen);

              this.numenvios =  this.numenvios+1; 
              if(this.totalenvios == this.numenvios){
                let array_detalle = JSON.stringify(this.array_resumen);

                /* +++++ ENVIAR EMAIL A API ++++ */
                formData.append('nombreApellido', this.usuario);
                formData.append('mail', this.correo);
                formData.append('consulta', array_detalle);
                formData.append('tipo', "detalle");
                console.log(this.usuario)
                console.log(array_detalle)

                this.http.post(this.urlEmail, formData)
                .subscribe(response => {
                  console.log(response['respuesta']);
                  if(response['respuesta'] == true){
                    this.msjEmail = "Se ha enviado email de confirmación a su cuenta";
                    
                  }else{
                    this.msjEmail = "No hemos podido enviar email de confirmación a su cuenta";
                    console.log("Error al enviar email desde api, no entrega respuesta true")
                    console.log(response)
                  }
                }, err => {
                  console.log("No hubo contacto con api para envío de email")
                  console.log(err);
                })


                this.cargando = false;
                this.msj = "success";
                this.msjSend = "Proceso exitoso"
                this.array_vincular= [];
                localStorage.removeItem("vinculaciones")
                
              }

              
              
            }           
          }, err => {
            this.array_resumen.push({
              titulo:         this.array_vincular[i].titulo,
              handle:         this.array_vincular[i].handle,
              httpCode:       "error"
            });

            this.msj = "error";
            this.cargando = false;
            console.log("¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde");
            console.log(err);
          }) 

          
      }
      
  }

}
