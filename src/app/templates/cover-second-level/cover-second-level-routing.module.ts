import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoverSecondLevelComponent } from './cover-second-level.component';

const routes: Routes = [
  { path: '',                   component: CoverSecondLevelComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoverSecondLevelRoutingModule { }
