import { Component, OnInit }                      from '@angular/core';
import { QueriesService }                         from '../../services/queries.service';
import json                                       from '../../../assets/json/blog/blog.json';
import jsonHome                                      from '../../../assets/json/home.json';
import { ActivatedRoute, Params }                 from '@angular/router';
import { global }                                 from '../../services/global';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {
  data: any = [];
  json: any = json;
  jsonHome: any = jsonHome;
  public urlPhp;

  constructor(
    private activatedRoute: ActivatedRoute,
    private queriesService: QueriesService
    ) { 
      this.urlPhp = global.php; 
    }

  ngOnInit() {

    // refresca página
    this.queriesService.getRefresh();

    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300); 

    // Recibo datos por get
    this.activatedRoute.params.subscribe(params =>{
      this.data['id'] = params['id'];
    });

    // NOVEDADES NEWS MAILCHIMP
    this.queriesService.queryGet(this.urlPhp +'/api-mailchimp/landing-page.php').then((data) => {
      this.data['news'] = data['landing_pages'].reverse();
      //console.log(data);
    });

  }
}



