import { Component, OnInit, ChangeDetectionStrategy, DoCheck, ɵConsole, Output }  from '@angular/core';


import { animate, state, style, transition, trigger }           from '@angular/animations';
import { Router, ActivatedRoute }                               from '@angular/router';
import json                                                     from '../../../assets/json/upload-record/upload-record.json';
import { FileuploadService }                                    from '../../servicio/fileupload.service';
import { QueriesService }                                       from '../../services/queries.service';
import { global }                                               from '../../services/global';
import { HttpClient } from '@angular/common/http';
import { DataService } from 'src/app/services/data.service.js';

import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';


// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;


@Component({
  selector: 'app-upload-record',
  templateUrl: './upload-record.component.html',
  styleUrls: ['./upload-record.component.css'],
  animations: [
    trigger('slide', [
      state('t1', style({ transform: 'translateX(0)' })),
      state('t2', style({ transform: 'translateX(-25%)' })),
      state('t3', style({ transform: 'translateX(-50%)' })),
      state('t4', style({ transform: 'translateX(-75%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class UploadRecordComponent implements OnInit, DoCheck {
  @Output()
  myFiles:string [] = [];
  /* public password; */
  public data: any = [];
  public json: any = json;
  public activePane: any = 't1';
  public vincula = true;
  public bolsaVincula;
  public tipo_pub = "investigacion";
  public tipo_doc = "Artículo";
  public titulopost;
  public array_vincula: any = []
  public archivos:any = [];
  public array_enviar: any = {
    publicacion: null,
    clave: null,
    user: this._queriesService.getToken(),
    titulo: null,
    volumen: null,
    anio_publicacion: null,
    otro_financiamiento: null,
    afiliacion_uc: null,
    url_publicacion: null,
    autores: null,
    archivos: null,
    concedo_licencia: null,
    revistas:null,
    numero: null,
    pagina_inicio: null,
    pagina_termino: null,
    doi: null,
    indexacion: null,
    otra_indexacion: null,
    edicion: null,
    nro_paginas: null,
    editores: null,
    lugar_edicion: null,
    editorial: null,
    isbn: null, 
    titulo_capitulo: null,
    area: null,
    tipo_obra: null,
    fecha_inicio: null,
    fecha_termino: null,
    institucion_auspiciadora: null,
    institucion_patrocinadora: null,
    ubicacion_obra: null,
    pais_obra: [],
    keywords: null,
    tipo_proteccion: null,
    unidad_academica: null,
    titular_solicitante: null,
    territorialidad: null,
    tipo_presentacion: null,
    estado_patentamiento: null,
    fecha_solicitud: null,
    nro_solicitud: null,
    pais_patente:null,
    fecha_concesion: null,
    nro_concesion: null
  };

/*   public array_new_post: any = {
    publicacion: null
  }; */
  public array_new_post: any = [];
  public array_guardados:any = [];

  public otras_autorias = "No";
  public general_colapsable = true;
  public autoria_colapsable = false;
  public archivos_colapsable = false;
  public formFile = false;
  public msj: any;
  public usuario;
  public name_usuario;
  public email = this._queriesService.getToken();
  public datos_academicos = JSON.parse(this._queriesService.getjsonAcademico());
  public login;
  public errLog;
  public urlRegistrar = global.php + "/formulario-sipa/formulario.php"; 
  public urlGuardar = global.php + "/vinculacion/mis_publicaciones.php"; 
  public urlvincular = global.php + "/vinculacion/VincularSipaWos.php"; 
  public urlSql = global.php + "/formulario-sipa/sql.php"; 
  public urlPhp = global.php + "/discovery-sipa.php?";
  public soportadateInput;
  public modal = false;
  public terminodebusqueda;
  public tituloModal;
  public registrosencontrados;
  public numautor;
  public campo1_metadato;
  public campo2_metadato;
  public newIndexacion = false;
  public cargando;
  public cargando_guardados;
  public cargando_autor;
  public btn_aplicar;
  public seleccionables:string;
  public busca_autor = null;
  public selectAutor;
  public msj_autor;
  public pasos = false;
  public filtro_valor = "";
  public msj_guardar;
  public menu;
  public tabla_resumen:any = [];
  public guardados;
  public registros;
  public id_post_draft;
  public nro_borrador;
  public msjEmail;
  public msjSend;
  public msj_error;
  public msj_guardados;
  public numenvios = 0;
  public alertaElimina;
  public idguardar;
  public mostrarForm;
  public max_date_fecha_inscripcion = true;
  public max_date_fecha_solicitud = true;
  public max_date_fecha_concesion = true;
  public minNumber = true;

  today;
  dd;
  mm;
  yyyy;
  anio: number = new Date().getFullYear();
  

  revista             = new FormControl('')
  lugar_edicion       = new FormControl('')
  editorial           = new FormControl('')
  otro_financiamiento = new FormControl('')
  area                = new FormControl('')
  tipo_obra           = new FormControl('')
  otra_indexacion     = new FormControl('')
  pais                = new FormControl('')
  institucion         = new FormControl('')
  unidad_academica    = new FormControl('')

  constructor(
    private uploadService: FileuploadService,
    private _queriesService: QueriesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    public dataService: DataService
    ) 
    {
      /* this.identity = this._queriesService.getIdentity();
      this.token = this._queriesService.getToken();
      this.password = this._queriesService.getPass(); */
    }
    
  ngOnInit() {
    // refresca página
    /* this._queriesService.getRefresh(); */

    /* VALORIZACION */
    var URLactual = window.location.href;
    this.json["rating"]['star'].pagina = URLactual

    /* USUARIOS SIN CODPERS */
    if(localStorage.getItem('estado_user_sipa')=="false"){
      this.router.navigate(['/mispublicaciones/confirmado']);
    }

    this.date();
    this.usuario = this._queriesService.getUser()
    let split = this.usuario.split(',');
    this.name_usuario = split[1];
    
    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);

    this.mis_guardados();
  }

  ngDoCheck(){
    /* this.password = this._queriesService.getPass(); */
  }

  /* +++++++++++++++  */

  date(){
    var pruebaFecha = document.createElement("input");
    //Si tiene soporte: debe aceptar el tipo "date"...
    pruebaFecha.setAttribute("type", "date");
    if ( pruebaFecha.type === "date" ){
      this.soportadateInput = true;
    }else{
      this.soportadateInput = false;
    }
  }
  fechaHoy(valor,desde){
    this.today = new Date();
    this.dd = this.today.getDate();
    this.mm = this.today.getMonth() + 1; //January is 0!
    this.yyyy = this.today.getFullYear();
    if (this.dd < 10) {
      this.dd = '0' + this.dd
    }
    if (this.mm < 10) {
      this.mm = '0' + this.mm
    }

    this.today = this.yyyy + '-' + this.mm + '-' + this.dd;
    if(valor>this.today){
      if(desde == 'fecha_inscripcion'){
        this.max_date_fecha_inscripcion = false
      }
      if(desde == 'fecha_solicitud'){
        this.max_date_fecha_solicitud = false
      }
      if(desde == 'fecha_concesion'){
        this.max_date_fecha_concesion = false
      }
    }else{
      if(desde == 'fecha_inscripcion'){
        this.max_date_fecha_inscripcion = true
      }
      if(desde == 'fecha_solicitud'){
        this.max_date_fecha_solicitud = true
      }
      if(desde == 'fecha_concesion'){
        this.max_date_fecha_concesion = true
      }
    }
  }
  toScroll(){
    document.getElementById('pasos').scrollIntoView({behavior: 'smooth'});
  }
  tipodoc(tipo){
    if(tipo=="Obra creativa"){
      this.array_enviar = [];
      this.array_enviar.publicacion = tipo;
    }
    if(tipo=="Artículo"){
      this.array_enviar = [];
      this.array_enviar.publicacion = tipo;
      this.array_enviar.titulo_capitulo = null;
    }
    if(tipo=="Libro"){
      this.array_enviar = [];
      this.array_enviar.publicacion = tipo;
    }
    if(tipo=="Capítulo de libro"){
      this.array_enviar = [];
      this.array_enviar.publicacion = tipo;
    }
    if(tipo=="Patente"){
      this.array_enviar = [];
      this.array_enviar.publicacion = tipo;
      
    }
    this.tipo_doc = tipo;
  }
  collapse(item){
    $('#'+item).slideToggle(300);
    this.actualizarPost();
  }
  min_number($event, num){
    if($event){
      var str = $event.toString().length;
      var min:Number = num;
      if(str<min){
        this.minNumber = false;
      }else{
        this.minNumber = true;
      }
    }else{
      this.minNumber = true;
    }
    
  }

  /* +++++++++++++++  */

  searchRevista(){
    this.data['revistas'] = null;

    /* Busca al escribir */
    this.revista.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "r";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['revistas']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcono(){
    /* Busca al click */
    var tabla = "r";
    var search = this.revista.value;
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+search)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['revistas']= data['resultado'];
          }
    });
  }
  addRevista(name,nuevo,donde){
      this.array_enviar.revistas = [];
      this.array_enviar.revistas.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['revistas'] = null;
      this.revista.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo=='si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchLugarEdicion(){
    /* Revistas */
    this.data['lugar_edicion'] = null;
    this.lugar_edicion.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "l";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['lugar_edicion']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoLugarEdicion(){
    var tabla = "l";
    var search = this.lugar_edicion.value;
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+search)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['lugar_edicion']= data['resultado'];
          }
    });
  }
  addLugarEdicion(name,nuevo,donde){
    if(!this.array_enviar.lugar_edicion){ /* solo cuando son multi registros */
      this.array_enviar.lugar_edicion = [];
    }

      this.array_enviar.lugar_edicion.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['lugar_edicion'] = null;
      this.lugar_edicion.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        console.log(JSON.stringify(array_newCampo))
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchEditorial(){
    /* Revistas */
    this.data['editorial'] = null;
    this.editorial.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "e";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['editorial']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoEditorial(){
    var tabla = "e";
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.editorial.value)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['editorial']= data['resultado'];
          }
    });
  }
  addEditorial(name,nuevo,donde){
    if(!this.array_enviar.editorial){ /* solo cuando son multi registros */
      this.array_enviar.editorial = [];
    }
    
      this.array_enviar.editorial.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['editorial'] = null;
      this.editorial.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchOtroFinanciamiento(){
    this.data['otro_financiamiento'] = null;
    this.otro_financiamiento.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "of";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['otro_financiamiento']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoOtroFinanciamiento(){
    var tabla = "of";
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.otro_financiamiento.value)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['otro_financiamiento']= data['resultado'];
          }
    });
  }
  addOtroFinanciamiento(name,nuevo,donde){
    if(!this.array_enviar.otro_financiamiento){ /* solo cuando son multi registros */
      this.array_enviar.otro_financiamiento = [];
    }
      this.array_enviar.otro_financiamiento.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['otro_financiamiento'] = null;
      this.otro_financiamiento.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchLugarArea(){
    /* Revistas */
    this.data['area'] = null;
    this.area.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "o";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['area']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoLugarArea(){
    var tabla = "o";
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.area.value)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['area']= data['resultado'];
          }
    });
  }
  addLugarArea(name,nuevo,donde){
    this.array_enviar.area = [];
      this.array_enviar.area.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['area'] = null;
      this.area.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchTipo_obra(){
    /* Revistas */
    this.data['tipo_obra'] = null;
    this.tipo_obra.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "t";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['tipo_obra']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoTipo_obra(){
    var tabla = "t";
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.tipo_obra.value)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['tipo_obra']= data['resultado'];
          }
    });
  }
  addTipo_obra(name,nuevo,donde){
    this.array_enviar.tipo_obra = [];
      this.array_enviar.tipo_obra.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['tipo_obra'] = null;
      this.tipo_obra.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchOtra_indexacion(){
    /* Revistas */
    this.data['otra_indexacion'] = null;
    this.otra_indexacion.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "ox";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['otra_indexacion']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoOtra_indexacion(){
    var tabla = "ox";
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.otra_indexacion.value)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['otra_indexacion']= data['resultado'];
          }
    });
  }
  addOtra_indexacion(name,nuevo,donde){
    if(!this.array_enviar.otra_indexacion){ /* solo cuando son multi registros */
      this.array_enviar.otra_indexacion = [];
    }
    
      this.array_enviar.otra_indexacion.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['otra_indexacion'] = null;
      this.otra_indexacion.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchPais(i){
    this.data['pais'] = null;
    this.selectAutor = i;
    this.pais.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "p";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['pais']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIconoPais(){
    /* Busca al click */
    var tabla = "p";
    var search = this.pais.value;
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+search)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['pais']= data['resultado'];
          }
    });
  }
  addPais(name,i,nuevo,donde){
    console.log(name)
    console.log(i);
    console.log(nuevo);
    console.log(donde)
    if(!this.array_enviar.pais_patente){ /* solo cuando son multi registros */
      this.array_enviar.pais_patente = [];
    }
    
      this.array_enviar.pais_patente.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['pais'] = null;
      this.pais.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        console.log(JSON.stringify(array_newCampo))
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  searchInstitucion(i){
    this.data['institucion'] = null;
    this.selectAutor = i;
    this.institucion.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "i";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['institucion']= data['resultado'];
          }
        });
      }
    });
    
  }
  addInstitucion(name,i,nuevo,donde){
    if(!this.array_enviar.autores[i].institucion){ /* solo cuando son multi registros */
      this.array_enviar.autores[i].institucion = [];
    }
    
      this.array_enviar.autores[i].institucion.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['institucion'] = null;
      this.institucion.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        console.log(JSON.stringify(array_newCampo))
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
      console.log(this.array_enviar.autores)
  }

  searchUnidad_academica(){
    this.data['unidad_academica'] = null;
    this.unidad_academica.valueChanges
    .pipe(debounceTime(300))
    .subscribe(value => {
      this.filtro_valor = value;

      if(this.filtro_valor){
        var tabla = "u";
        this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.filtro_valor)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['unidad_academica']= data['resultado'];
          }
        });
      }
    });
    
  }
  searchIcoUnidad_academica(){
    var tabla = "u";
    this._queriesService.queryGet( this.urlSql+ '?campo='+tabla+'&busqueda='+this.unidad_academica.value)
        .then((data) => {  
          if(data['code'] == 404){ /* Verifica sesion */
            console.log(data['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else{
          this.data['unidad_academica']= data['resultado'];
          }
    });
  }
  addUnidad_academica(name,nuevo,donde){
    if(!this.array_enviar.unidad_academica){ /* solo cuando son multi registros */
      this.array_enviar.unidad_academica = [];
    }
    
      this.array_enviar.unidad_academica.push({
        nombre  : name,
        nuevo   : nuevo
      });
      event.preventDefault()
      this.data['unidad_academica'] = null;
      this.unidad_academica.setValue('');

      /* Insert Nuevo Campo */
      if(nuevo == 'si'){
        let array_newCampo = {
          tipo    : donde,
          campo   : name
        };
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        
        this.http.post(this.urlSql, formData)
        .subscribe(response => {
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == "200"){
            console.log("insertó nuevo campo");   
          }else{
            console.log("se conectó pero no trajo resultado 200");
            console.log(response)
          }
        })
      }
  }

  quitaAplicaSeleccion(titulomodal, i: number, i_pais){
    if(titulomodal == "Revista"){
      this.array_enviar.revistas.splice(i, 1);
      this.array_enviar.revistas = null;
    }
    if(titulomodal == "Otro financiamiento"){
      this.array_enviar.otro_financiamiento.splice(i, 1);
      if(this.array_enviar.otro_financiamiento==0){
        this.array_enviar.otro_financiamiento = null;
      }
    }
    if(titulomodal == "Indexación"){
      this.array_enviar.otra_indexacion.splice(i, 1);
    }
    if(titulomodal == "Lugar edición"){
      this.array_enviar.lugar_edicion.splice(i, 1);
      if(this.array_enviar.lugar_edicion==0){
        this.array_enviar.lugar_edicion = null;
      }
    }
    if(titulomodal == "Editorial"){
      this.array_enviar.editorial.splice(i, 1);
      if(this.array_enviar.editorial==0){
        this.array_enviar.editorial = null;
      }
    }
    if(titulomodal == "Area"){
      this.array_enviar.area.splice(i, 1);
      this.array_enviar.area = null;
    }
    if(titulomodal == "Tipo de obra"){
      this.array_enviar.tipo_obra.splice(i, 1);
      this.array_enviar.tipo_obra = null;
    }
    if(titulomodal == "Pais"){
      this.array_enviar.pais_patente.splice(i, 1);
      this.array_enviar.pais_patente = null;
      /* this.array_enviar.autores[i].pais.splice(i_pais, 1); */
    }
    if(titulomodal == "Institución"){
      this.array_enviar.autores[i].institucion.splice(i_pais, 1);
    }
    if(titulomodal == "Unidad académica"){
      this.array_enviar.unidad_academica.splice(i, 1);
      this.array_enviar.unidad_academica = null;
    }
    
  }
  
  /* +++++++++++++++  */
     
  seleccionarArchivo(event) {
    this.formFile = true;
    /* Cargo archivos en array myFiles */
    for (var i = 0; i < event.target.files.length; i++) {    
        this.myFiles.push(event.target.files[i]);
    }
    /* Escribo archivo en el array con descricion y fecha */
    if(!this.array_enviar.archivos){
      this.array_enviar.archivos = [];
    }
    
    for (var i = 0; i < event.target.files.length; i++) { 
        this.array_enviar.archivos.push({
          archivo: event.target.files[i].name,
          fecha_embargo: '0'
        });
    }
    console.log(this.myFiles);
    console.log(this.array_enviar.archivos)
  }
  removeFile(i: number) {
    this.array_enviar.archivos.splice(i, 1);
    this.myFiles.splice(i, 1);
    if(this.array_enviar.archivos.length == 0){
      this.array_enviar.archivos = null;
    }
    console.log(this.myFiles);
    console.log(this.array_enviar.archivos)
  }
  resetFile(){
    for (var i = 0; i < this.array_enviar.archivos.length; i++) { 
      this.array_enviar.archivos[i].archivo = null;
    }
  }

  addBuscaAutores(){
    this.cargando_autor = true;
    this.msj_autor = null;
    
    for(let i=0; this.array_enviar.autores.length>i; i++){
        if(this.array_enviar.autores[i].email == this.busca_autor){
          this.busca_autor = null;
          this.cargando_autor = false;
          this.msj_autor = "El autor ya existe en su lista";
        }
    }

    if(this.busca_autor){
      // Busca autores
      let busca_autor = {
        tipo    : "buscarAutor",
        campo   :  this.busca_autor //  "riolmos@uc.cl"
      };
      const formData = new FormData();
      formData.append('array_insert', JSON.stringify(busca_autor));

      this.http.post(this.urlSql, formData)
        .subscribe(response => {
          this.cargando_autor = false;
          if(response['code'] == 404){ /* Verifica sesion */
            console.log(response['msg']);
            window.location.href=global.php+'/cas-log/cas-login.php';
          }else if(response == 400){
            this.msj_autor = "No existe ese usuario UC";
            console.log(response)
          }else{
            
            this.msj_autor = null; 
            if(response[0].nacionalidad==' '){var pais="Chile"}else{pais=response[0].nacionalidad};
            this.array_enviar.autores.push({
              email: response[0].correo,
              rol: null,
              nombres: null,
              apellidos: null,
              nombre_completo: response[0].nombre_completo,
              ORCID: null,
              pais: [{
                nuevo: "no",
                nombre: pais
              }],
              institucion: [{
                nuevo: "no",
                nombre: "UC"
              }]
            });
          }
          console.log(this.array_enviar.autores)
        }, err => {
          this.msj_autor = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
          this.cargando_autor = false;
          console.log(err);
        })
    }
  }
  addAutores() {  
    this.array_enviar.autores = this.array_enviar.autores;
    this.array_enviar.autores.push({
      email: null,
      rol: null,
      nombres: null,
      apellidos: null,
      nombre_completo:null,
      ORCID: null,
      pais: null,
      institucion: []
    });
  }
  removeAutores(i: number) {
    this.array_enviar.autores.splice(i, 1); // elimina 1 indice a partir del indice i
    /* if(this.array_enviar.autores==0){
      this.array_enviar.autores = [];
    } */

    /* if(this.array_enviar.autores.length==1){
      this.array_enviar.autores = null;
    }else{
      this.array_enviar.autores.splice(i, 1); // elimina 1 indice a partir del indice i
    } */
    console.log(this.array_enviar.autores)
  }

  cheq(event){
    if(!this.array_enviar.indexacion){
      this.array_enviar.indexacion = [];
    }
    if(event.target.checked){
      this.array_enviar.indexacion.push(
        event.target.value
      );
    }else{
      for(var i = 0; this.array_enviar.indexacion.length>i; i++){    
        if(this.array_enviar.indexacion[i]==event.target.value){
          this.array_enviar.indexacion.splice(i, 1);
        }
      }
    }

  }

  addUrl() {  
    if(!this.array_enviar.url_publicacion){
      this.array_enviar.url_publicacion = [];
    }
    this.array_enviar.url_publicacion.push({
      url: null
    });

      //document.getElementById('input_13_0').focus();

    
  }
  removeUrl(i: number) {
    this.array_enviar.url_publicacion.splice(i, 1); // elimina 1 indice a partir del indice i
    if(this.array_enviar.url_publicacion==0){
      this.array_enviar.url_publicacion = null;
    }
  }

  addEditores() {  
    if(!this.array_enviar.editores){
      this.array_enviar.editores = [];
    }
    this.array_enviar.editores.push({
      editor: null
    });
  }
  removeEditores(i: number) {
    this.array_enviar.editores.splice(i, 1); // elimina 1 indice a partir del indice i
    if(this.array_enviar.editores==0){
      this.array_enviar.editores = null;
    }
    
  }

  addAuspiciadora() {  
    if(!this.array_enviar.institucion_auspiciadora){
      this.array_enviar.institucion_auspiciadora = [];
    }
    this.array_enviar.institucion_auspiciadora.push({
      nombre: null
    });
  }
  removeAuspiciadora(i: number) {
    this.array_enviar.institucion_auspiciadora.splice(i, 1); // elimina 1 indice a partir del indice i
    if(this.array_enviar.institucion_auspiciadora==0){
      this.array_enviar.institucion_auspiciadora = null;
    }
  }

  addPatrocinadora() {  
    if(!this.array_enviar.institucion_patrocinadora){
      this.array_enviar.institucion_patrocinadora = [];
    }
    this.array_enviar.institucion_patrocinadora.push({
      nombre: null
    });
  }
  removePatrocinadora(i: number) {
    this.array_enviar.institucion_patrocinadora.splice(i, 1); // elimina 1 indice a partir del indice i
    if(this.array_enviar.institucion_patrocinadora==0){
      this.array_enviar.institucion_patrocinadora = null;
    }
  }

  addTitularSolicitante(opcion){
    console.log(opcion);
    /* if(!this.array_enviar.titular_solicitante){
      this.array_enviar.titular_solicitante = [];
    } */
    this.array_enviar.titular_solicitante.push({
      nombre: null
    });
    
    /* if(opcion == "Otro"){
      this.array_enviar.titular_solicitante = null;
    }else{
      this.array_enviar.titular_solicitante = opcion;
    } */
  }
  removeTitularSolicitante(i: number) {
    this.array_enviar.titular_solicitante.splice(i, 1); // elimina 1 indice a partir del indice i
  }

  territ(opcion){
    console.log(opcion);
    this.array_enviar.territorialidad = opcion;
    this.array_enviar.tipo_presentacion = null;
    if(opcion == "Extranjera"){
      this.array_enviar.tipo_presentacion = opcion;
    }
  }
  tipo_presentacion(opcion){
    console.log(opcion);
    this.array_enviar.tipo_presentacion = opcion;
    
  }
  


/* /////////////// APIS //////////////////  */


  /* +++ INSERTAR NEW BORRADOR +++ */
  insertBorrador(titulo){
    this.array_new_post={
      publicacion: titulo,
      user: this._queriesService.getToken(),
      titular_solicitante:[{
        nombre: "Pontificia Universidad Católica de Chile"
      }]
    }
    if(this.datos_academicos){
      this.array_new_post.autores = [];
      this.array_new_post.autores.push({
        email: this.email,
        nombre_completo: this.usuario,
        ORCID: this.datos_academicos.ORCID,
        pais: [{
          nuevo: "no",
          nombre: this.datos_academicos.PAIS
          }],
        institucion: [{
          nuevo: "no",
          nombre: this.datos_academicos.INSTITUCION
          }]
      });
    }

    var acciones;
    acciones = {accion: "guardar", id: "", email: this.email, validaBiblio: ""}
    const formData = new FormData();
    formData.append('array_acciones', JSON.stringify(acciones));
    formData.append('array_borrador', JSON.stringify(this.array_new_post));

    /* +++++ CARGAR EN BBDD ++++ */
    
    this.http.post(this.urlGuardar, formData)
    .subscribe(response => {
      this.cargando = false;
      console.log(response);
      if(response['code'] == 404){ /* Verifica sesion */
        window.location.href=global.php+'/cas-log/cas-login.php';
      }else if(response['httpCode'] == "200"){
        this.idguardar = response['mensaje'];
        this.editPost(this.array_new_post, this.idguardar);
        this.mis_guardados();

        $('#general').show();
        /* document.getElementById('formu').scrollIntoView({behavior: 'smooth'}); */
        this.msj_guardar = "Se guardó borrador en Mi producción académica.";
               
      }else{
              this.msj_guardar = "Error de conexión, no pudo ser guardado";
              console.log("se conectó pero no trajo httpCode 201");
              console.log(response)
      }
    }, err => {
      this.msj_guardar = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
      this.cargando = false;
      console.log("error en api")
      console.log(err);
    })
  }

  /* +++ EDITAR BORRADOR FORM +++ */
  actualizarPost(){

    let limpiar_array = this.array_enviar;
    for(let i in limpiar_array){
      if(limpiar_array[i] == null || limpiar_array[i] == ''){
        delete limpiar_array[i];
      }
    }

    /* Armo acciones */
    var acciones;
    acciones = {accion: "actualizar", id: this.idguardar, email: this.email, validaBiblio: ""}

    const formData = new FormData();
    /* Entrego archivos */
    for (var i = 0; i < this.myFiles.length; i++) { 
      formData.append("file[]", this.myFiles[i]);
    }
    /* Entrgo array */
    formData.append('array_acciones', JSON.stringify(acciones));
    formData.append('array_borrador', JSON.stringify(limpiar_array));

    console.log(acciones)
    console.log(limpiar_array);

    /* +++++ CARGAR EN BBDD ++++ */
    
    this.http.post(this.urlGuardar, formData)
    .subscribe(response => {
      this.cargando = false;
      if(response['code'] == 404){ /* Verifica sesion */
        window.location.href=global.php+'/cas-log/cas-login.php';
      }else if(response['httpCode'] == "200"){
        this.mis_guardados();
              this.msj_guardar = "Se actualizó borrador en Mi producción académica.";
              console.log(response);
              
      }else{
              this.msj_guardar = "Error de conexión, no pudo ser guardado";
              console.log("se conectó pero no trajo httpCode 201");
              console.log(response)
      }
    }, err => {
      this.msj_guardar = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
      this.cargando = false;
      console.log("error en api")
      console.log(err);
    })

  }
  // GUARDADOS SIN ENVIAR
  mis_guardados(){ 
    this.cargando_guardados = true;
    this.msj_guardados = null;
    var acciones;
    const formData = new FormData();
    acciones = {accion: "consultar", id: "", email: this.email, validaBiblio:"", start: "", handle: ""}
    formData.append('array_acciones', JSON.stringify(acciones));
     
    /* +++++ CARGAR EN BBDD ++++ */
      
    this.http.post(this.urlGuardar, formData)
      .subscribe(response => {
        this.cargando_guardados = false;
        this.array_guardados = response;

        if(response['code'] == 404){ /* Verifica sesion */
          window.location.href=global.php+'/cas-log/cas-login.php';
        }else{
          if(response && this.array_guardados.length != 0){ 
            for( let i=0; this.array_guardados.length>i; i++){
              this.array_guardados[i].json_temporal = JSON.parse(this.array_guardados[i].json_temporal)
            }
          }else{
            this.array_guardados = [];
            this.msj_guardados = "No existen documentos guardados"
          }
        }
    }, err => {
      this.cargando_guardados = false;
      this.msj_guardados = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
      console.log(err);
    })
  
  }
  alertDelete(i){
    this.alertaElimina = i;
    console.log(i)
  }
  /* ELIMINO GUARDADO */
  deletePost(idpost, i){
    this.cargando = true;
    this.msj = null;
    this.alertaElimina = null;
    
    const formData = new FormData();

      /* Armo acciones */
      var acciones;
      acciones = {accion: "eliminar", id: idpost, email: this.email, validaBiblio:"", start: "", handle: ""}
      console.log(acciones);
      /* Entrgo array */
      formData.append('array_acciones', JSON.stringify(acciones));
    
      /* +++++ ENVIO ++++ */
      this.http.post(this.urlGuardar, formData)
      .subscribe(response => {

        this.cargando = false;
        if(response['code'] == 404){ /* Verifica sesion */
          window.location.href=global.php+'/cas-log/cas-login.php';
        }else if(response['httpCode'] == "201" || response['httpCode'] == "200"){ 
          this.array_guardados.splice(i, 1);   
          this.mostrarForm = false;     
        }else{
          this.msj = "Tuvimos problema, no pudimos eliminar el registro";
          console.log("conecto pero no trajo httpCode = 201 o 200");
          console.log(response)
        }
      }, err => {
        this.msj= "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde"
        this.cargando = false;
        console.log(err);
      })
  }
  /* SEGUIR EDITANDO */
  editPost(obj,id_post){
    /* console.log(obj) */
    this.registros = false;
    this.mostrarForm = true;
    this.activePane = 't1';
    $('#general').show();
    this.idguardar = id_post;
    this.array_enviar = obj;
    this.tipo_doc = this.array_enviar.publicacion;
    document.getElementById('formu').scrollIntoView({behavior: 'smooth'});
  }

  resumenEnvio(){
    let limpiar_array = this.array_enviar;
    let array_tabla:any = [];
    for(let i in limpiar_array){
      if(limpiar_array[i] == null || limpiar_array[i] == ''){
        delete limpiar_array[i];
      }
    }
    let list = limpiar_array;
    for(let ind in list){
      array_tabla.push( { "cod":ind, "valor": list[ind] });
    }
    console.log(array_tabla)
  }

  // +++++++++++ ENVIO FORM +++++++++++++++
  submitPublica(form){ 
    this.cargando     = true;
    const formData    = new FormData();
    let limpiar_array = this.array_enviar;

        for(let i in limpiar_array){
          if(limpiar_array[i] == null || limpiar_array[i] == ''){
            delete limpiar_array[i];
          }
        }

        /* Entrego archivos */
        for (var i = 0; i < this.myFiles.length; i++) { 
          formData.append("file[]", this.myFiles[i]);
        }
        /* Desde guardados */
        if(this.idguardar){
          formData.append('id_eliminar', this.idguardar);
        }else{
          formData.append('id_eliminar', 'no');
        }

        formData.append('array_datos', JSON.stringify(limpiar_array));

        /* +++++ ENVIAR A API ++++ */
        this.http.post(this.urlRegistrar, formData)
        .subscribe(response => {
          
          if(response['code'] == 404){ /* Verifica sesion */
            window.location.href = global.php+'/cas-log/cas-login.php';
          }else if(response['httpCode'] == "200" || response['httpCode'] == "201"){
            localStorage.removeItem('idguardar');
            form.reset();
            this.cargando = false;
            this.msj      = "success";
            this.msjSend  = "Proceso exitoso"
            this.mis_guardados();
            console.log(response);
            if(response['httpCodeCorreo'] == "200" || response['httpCodeCorreo'] == "201"){
              this.msjEmail = "Se ha enviado email de confirmación a su cuenta";
            }else{
              this.msjEmail = "No hemos podido enviar email de confirmación a su cuenta";
            }
          }else{
            this.cargando   = false;
            this.msj        = "error";
            this.msjSend    = "Envío ha fallado."
            this.msj_error  = "¡Disculpa! Hemos enviado sus datos pero el servicio no logra recibirlos, inténtalo nuevamente.";
            console.log(response)
    }
        }, err => {
          this.msj        = "error";
          this.cargando   = false;
          this.msjSend    = "Envío ha fallado."
          this.msj_error  = "¡Disculpa! Hemos perdido la conexión con el servicio, inténtalo mas tarde";
          console.log(err);
        })
        
      
  }

}
