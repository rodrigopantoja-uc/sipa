import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadRecordRoutingModule } from './upload-record-routing.module';
import { UploadRecordComponent} from './upload-record.component';
/* import { BreadcrumbsComponent } from '../../partials/commons/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from '../../partials/commons/title/title.component'; */

@NgModule({
  declarations: [
    UploadRecordComponent
/*     BreadcrumbsComponent,
    TitleComponent */
  ],
  imports: [
    CommonModule,
    UploadRecordRoutingModule
  ]
})
export class UploadRecordModule { }
