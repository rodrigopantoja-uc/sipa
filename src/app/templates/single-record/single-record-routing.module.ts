import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SingleRecordComponent} from './single-record.component';

const routes: Routes = [
  { path: '',                   component: SingleRecordComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SingleRecordRoutingModule { }
