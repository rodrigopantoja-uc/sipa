import { Component, OnInit }                      from '@angular/core';
import { QueriesService }                         from '../../services/queries.service';
import json                                       from '../../../assets/json/blog/blog.json';
import { ActivatedRoute, Params }                 from '@angular/router';
import { global }                                 from '../../services/global';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  data: any = [];
  json: any = json;
  public urlPhp;

  constructor(
    private activatedRoute: ActivatedRoute,
    private queriesService: QueriesService
    ) { 
      this.urlPhp = global.php; 
    }

  ngOnInit() {
    // refresca página
    this.queriesService.getRefresh();

    // JQuery ir arriba
      $('body, html').animate({
        scrollTop: '0px'
      }, 300); 

    // Recibo datos por get
    this.activatedRoute.params.subscribe(params =>{
      this.data['id'] = params['id'];
    });

    this.queriesService.queryGet(this.urlPhp+'/api-mailchimp/landing.php?page='+this.data['id']).then((data) => {
      this.data['blog'] = data['landing'];
      $("#blog").html(this.data['blog']);
    });

    

  }


}



