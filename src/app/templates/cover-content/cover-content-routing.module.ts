import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoverContentComponent } from './cover-content.component';

const routes: Routes = [
  { path: '',                   component: CoverContentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoverContentRoutingModule { }
