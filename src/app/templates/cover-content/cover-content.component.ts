import { Component, OnInit, DoCheck }  from '@angular/core';
import { Router, ActivatedRoute, Params }     from '@angular/router';
import json                   from '../../../assets/json/cover-content/enlaces.json';
import { QueriesService }                         from '../../services/queries.service';

@Component({
  selector: 'app-cover-content',
  templateUrl: './cover-content.component.html',
  styleUrls: ['./cover-content.component.css']
})
export class CoverContentComponent implements OnInit, DoCheck {

  data: any = [];
  main: any = json;

  public pagina: string;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    private queriesService: QueriesService
  ) { }

  ngOnInit() {

    // refresca página
    /* this.queriesService.getRefresh(); */

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
      // JQuery ir arriba
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);
    });
  }
  ngDoCheck() {
    this.main = json;
    this.paginas();
  }

  paginas(){
      // MENU ENLACES Y RECURSOS
      if(this.pagina == "otros-repositorios"){
        this.main = this.main[this.pagina ];

      }
      if(this.pagina == "recursos-uc"){
        this.main = this.main[this.pagina ];
      }
      if(this.pagina == "patentes-uc"){
        this.main = this.main[this.pagina ];
      }
  }

}
