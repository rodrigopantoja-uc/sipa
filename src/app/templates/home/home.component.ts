import { Component, OnInit,DoCheck, ɵConsole }              from '@angular/core';
import { Observable }                             from 'rxjs';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { QueriesService }                         from '../../services/queries.service';
import json                                       from '../../../assets/json/home.json';
import AreasTematicas                             from '../../../assets/json/areas-tematicas.json';
import CajonBusqueda                              from '../../../assets/json/cajon-busqueda.json';

import { FormGroup,  FormBuilder,  Validators }   from '@angular/forms';
import { Router }                                 from '@angular/router';

import { NgForm }                                 from '@angular/forms';
import { SubscribeService }                       from '../../services/subscribe.service';
import { global }                                 from '../../services/global';
import { DataService }                                 from '../../services/data.service';

import { environment }                            from "../../../environments/environment";
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
  
})
export class HomeComponent implements OnInit, DoCheck {
  public urlPhp;
  json:     any = json;
  posts:    any = AreasTematicas['areas-tematicas'];
  CajonBusqueda: any = CajonBusqueda;
  data:     any = [];
  loggedIn: any = false;
  public usuario = this.queriesService.getUser();
  public correo = this.queriesService.getToken();
  /* public unidad = localStorage.getItem('unidad'); */
  public datos_academicos = JSON.parse(this.queriesService.getjsonAcademico());
  /* usuario;
  correo;
  unidad; 
  datos_academicos;*/
  tabla_datos_academicos: any=[];
  editing:  any = false;
  userData: any = [];
  loginFailed: any = "";
  angForm: FormGroup;
  editForm: FormGroup;
  categorias: any = [];
  status;
  public buscalibro = { name: ''};
  subscribeData: any = <any>{};
  modal = false;
  tituloModal;
  condicion_publicar = false;

  /* expirar localsession */

  public now;
  public hours = 1;

  d1;
  d2;

  constructor(
    private subscribeService: SubscribeService,
    private router: Router,
    private queriesService: QueriesService, private _sanitizer: DomSanitizer, private formBuilder: FormBuilder,
    private dataService: DataService
    ) 
    {
    this.urlPhp = global.php; 
    }

  ngOnInit() {
    
    
    //document.getElementById('top').scrollIntoView({behavior: 'smooth'}); // Scroll hacia buscador
    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);
    
    // TEXTOS PÁGINA
    this.json = json;
    
    // NUESTROS NUMEROS
    /* this.queriesService.queryGet('https://sipa-des.cloud.uc.cl/assets/php/cantidad-otros.php').then((data) => { */
    /* this.queriesService.queryGet(this.urlPhp +'/cantidad-tesis.php').then((data) => { */
    this.queriesService.queryGet(this.urlPhp +'/cantidad-articulos.php').then((data) => {
      this.data['statistics-articulos'] = data['response']['numFound'];
    });

    this.queriesService.queryGet(this.urlPhp +'/cantidad-capituloDeLibros.php').then((data) => {
      this.data['statistics-capitulo'] = data['response']['numFound'];
      
    });

    this.queriesService.queryGet(this.urlPhp +'/cantidad-libros.php').then((data) => {
      this.data['statistics-libros'] = data['response']['numFound']; console.log(data)
    });

    this.queriesService.queryGet(this.urlPhp +'/cantidad-patentes.php').then((data) => {
      this.data['statistics-patentes'] = data['response']['numFound'];
    });

    this.queriesService.queryGet(this.urlPhp +'/cantidad-obras.php').then((data) => {
      this.data['statistics-obras'] = data['response']['numFound'];
    });
    
  }

  ngDoCheck(){

    // COMPUEBO LOGIN
    if(this.queriesService.getToken()){
      this.loggedIn = true;
      this.router.navigate(['/mispublicaciones/confirmado']); // /upload/subir-publicacion
    }else{
      this.loggedIn = false;
    }
  }

  scrollToElement($element): void {
    console.log($element);
    document.getElementById('suscripcion').scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
  abreModal(){
    this.modal = true;
    this.tituloModal = "Mensaje";
  }
  cerrarModal(){
    this.modal = false;
  }

  loginCas(){
    /* window.location.href='assets/php/cas-log/cas-login.php'; */

    localStorage.setItem('usuario', 'Pantoja Flores, Rodrigo Alejandro');
    localStorage.setItem('correo', 'rodrigo.pantoja@uc.cl');
    localStorage.setItem('estado_user_sipa', 'true');
    localStorage.setItem('jsonAcademico', 
    '{"NOMBRE_COMPLETO": "PANTOJA FLORES, RODRIGO ALEJANDRO","CATEGORIA_ACADEMICA": "PROFESOR ASISTENTE","COD_PERS": "1105052","CORREO": "rodrigo.pantoja@uc.cl","DV": "K","FACULTAD": "FACULTAD DE MEDICINA","RUT":"11.111.111-1","GRADO_PERFECCIONAMIENTO":"ESPECIALIDAD MÉDICA/ ODONTOLÓGICA","PAIS":"Chile","ORCID":"123456789","INSTITUCION":"PUC","UNIDAD_ACADEMICA":"DISEÑO","NOMBRE_PERFIL":["jefaturas"],"DESCRIPCION_PERFIL":["acceso a gestion de academicos"]}');

    this.d1 = new Date().getTime();
    localStorage.setItem("sessionsipa", this.d1);
  } 
  logout(){
    localStorage.removeItem('usuario');
    localStorage.removeItem('correo');
    localStorage.removeItem('estado_user_sipa');
    /* localStorage.removeItem('unidad'); */
    localStorage.removeItem('jsonAcademico');
    window.location.href='assets/php/cas-log/logout-cas.php';
  }

  getStatistics(num){
    localStorage.setItem('search_form','');
    var array_Filtros: any[] = [
      {
        search_by: 'tipo',
        contains: 'contiene',
        term: num
      }
    ];
    localStorage.setItem('json_filtros',JSON.stringify(array_Filtros));
    localStorage.setItem('searchAdvanced','true');

    this.router.navigate(['/busqueda']);
  }

  cheq(event){
    if(event.target.checked){
      /* this.array_form.indexacion.push(
        event.target.value
      ); */
      this.condicion_publicar = true;
    }else{
      /* for(var i = 0; this.array_form.indexacion.length>i; i++){    
        if(this.array_form.indexacion[i]==event.target.value){
          this.array_form.indexacion.splice(i, 1);
        }
      } */
      this.condicion_publicar = false;
    }

  }
  

}
