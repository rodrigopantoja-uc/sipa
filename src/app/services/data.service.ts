import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  postId: string = null;
  entrada: boolean = null;
  activarSidebar: string = 'false';
  editar: string = "aqui";

  constructor() { }
}
