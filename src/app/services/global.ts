
export var global = {
    // DES
    /* dominio: window.location.protocol +'//'+window.location.hostname,
    php: window.location.protocol +'//'+window.location.hostname +'/assets/php', 
    php_img: window.location.protocol +'//'+window.location.hostname+':8080',
    php_download: window.location.protocol +'//'+window.location.hostname +':8080' */

    // LIB
    /* dominio: window.location.protocol +'//'+window.location.hostname,
    php: window.location.protocol +'//'+window.location.hostname +'/assets/php', 
    php_img: window.location.protocol +'//'+window.location.hostname+'/xmlui',
    php_download: window.location.protocol +'//'+window.location.hostname +'/xmlui' */
    
    // LOCAL
    dominio: 'https://sipa-des.cloud.uc.cl',
    php:'https://sipa-des.cloud.uc.cl/assets/php',
    php_img:'https://sipa-des.cloud.uc.cl:8080', 
    php_download:'https://sipa-des.cloud.uc.cl:8080'

    /* dominio: 'http://sipa-des.bibliotecas.uc.cl',
    php:'http://sipa-des.bibliotecas.uc.cl/assets/php',
    php_img:'http://sipa-des.bibliotecas.uc.cl:8080', 
    php_download:'http://sipa-des.bibliotecas.uc.cl:8080' */
    
};

