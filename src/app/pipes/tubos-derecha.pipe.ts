import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name:"tubos_derecha"
})
export class TubosDerechaPipe implements PipeTransform{

  /* transform(value:string, [separator]):string {
    let splits = value.split(separator);
    if(splits.length > 1) {
      return splits.pop();
    } else {
      return '';
    }
  } */

  transform(value:string):string {

    let splits = value.split('|||');
    if(splits.length > 1) {
      return splits.pop();
    } else {
      return value;
    }
  }

}