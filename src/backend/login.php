<?php

require_once 'init.inc.php';

if (isset($_GET['service'])) {
    // TODO whitelist service allowed
    phpCAS::setFixedServiceURL($_GET['service']);
    phpCAS::setNoClearTicketsFromUrl(); // removing ticket from url uses redirect which we can't do
}
if (!phpCAS::isAuthenticated()) {
    header('HTTP/1.0 401 Unauthorized');
    echo 'HTTP/1.0 401 Unauthorized';    
    exit();
}

  header('Content-type: application/json; charset=UTF-8');
  echo json_encode([ "bearer" => session_id() ]);

