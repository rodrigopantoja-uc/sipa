function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["templates-cover-content-cover-content-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/cover-content/cover-content.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/templates/cover-content/cover-content.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppTemplatesCoverContentCoverContentComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"uc-section uc-section__header mb-60\" *ngIf=\"main['horizon_header']['active']\">\n  <div class=\"container\">\n    <!-- <app-breadcrumbs [breadcrumbs]=\"main['horizon_header']['section_breadcrumbs']['breadcrumbs']\" *ngIf=\"main['horizon_header']['section_breadcrumbs']['active']\"></app-breadcrumbs> -->\n    \n    <ul class=\"uc-breadcrumb mb-24\" *ngIf=\"main['horizon_header']['section_breadcrumbs']['active']\">\n      <li *ngFor=\"let breadcrumb of main['horizon_header']['section_breadcrumbs']['breadcrumbs']; last as l\" class=\"uc-breadcrumb_item{{(l) ? ' current' : '' }}\">\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"!breadcrumb['dynamic']\">{{ breadcrumb.text }}</a>\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"breadcrumb['dynamic']\">{{ particular }}</a>\n        <i class=\"uc-icon\" *ngIf=\"!l\">keyboard_arrow_right</i>\n      </li>\n    </ul>\n\n    <div class=\"row\">\n      <div class=\"col-md-8 col-12\">\n        <h1>{{ main['horizon_header']['title'] }}</h1>\n        <div class=\"uc-excerpt\" *ngIf=\"main['horizon_header']['section_excerpt']['active']\">\n          {{main['horizon_header']['section_excerpt']['excerpt']}}\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n\n<section class=\"uc-section uc-section__page\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <ng-container *ngFor=\"let page of main['childs']\">\n      <div class=\"col-lg-6 mb-32\" >\n        <!-- <app-card-horizontal [data]=\"page\" [dataOrientation]=\"'image-left'\" [dataDivider]=\"true\"></app-card-horizontal> -->\n        <article class=\"uc-card card-type--horizontal card-height--same\">\n          <figure class=\"uc-card_img\" [style.background-image]=\"'url(' + page.image + ')'\">\n\n            <div class=\"uc-veil uc-veil--blue\"></div>\n          </figure>\n          <div class=\"uc-card_body\">\n            <ng-container *ngIf=\"page['tag']\">\n              <a title=\"{{ page['tag']['title'] }}\" target=\"_blank\"  href=\"{{ page['tag']['url'] }}\" class=\"uc-tag mb-12\">{{ page['tag']['text'] }}</a>\n            </ng-container>\n            <h3 class=\"uc-card__title mb-16\">\n              <a title=\"{{ page['link']['title'] }}\" target=\"_blank\" href=\"{{ page['link']['url'] }}\">{{ page['title'] }}</a>\n            </h3>\n            <div class=\"uc-text-divider divider-primary\"></div>\n            <div class=\"uc-excerpt mb-24\" *ngIf=\"excerpt\"><p>{{ page['excerpt'] }}</p></div>\n            <a title=\"{{ page['link']['title'] }}\" target=\"_blank\"  href=\"{{ page['link']['url'] }}\" class=\"uc-link no-decoration text-align--right mt-auto\">\n              <strong>{{ page['link']['text'] }}<i class=\"uc-icon\">{{ page['link']['icon'] }}</i></strong>\n            </a>\n          </div>\n        </article>\n\n      </div>\n    </ng-container>\n    </div>\n  </div>\n</section>\n";
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: CoverContentRoutingModule */

  /***/
  function srcAppTemplatesCoverContentCoverContentRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoverContentRoutingModule", function () {
      return CoverContentRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _cover_content_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./cover-content.component */
    "./src/app/templates/cover-content/cover-content.component.ts");

    var routes = [{
      path: '',
      component: _cover_content_component__WEBPACK_IMPORTED_MODULE_3__["CoverContentComponent"]
    }];

    var CoverContentRoutingModule = function CoverContentRoutingModule() {
      _classCallCheck(this, CoverContentRoutingModule);
    };

    CoverContentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CoverContentRoutingModule);
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content.component.css":
  /*!*********************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content.component.css ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppTemplatesCoverContentCoverContentComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlcy9jb3Zlci1jb250ZW50L2NvdmVyLWNvbnRlbnQuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content.component.ts ***!
    \********************************************************************/

  /*! exports provided: CoverContentComponent */

  /***/
  function srcAppTemplatesCoverContentCoverContentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoverContentComponent", function () {
      return CoverContentComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../assets/json/cover-content/enlaces.json */
    "./src/assets/json/cover-content/enlaces.json");

    var _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/cover-content/enlaces.json */
    "./src/assets/json/cover-content/enlaces.json", 1);
    /* harmony import */


    var _services_queries_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/queries.service */
    "./src/app/services/queries.service.ts");

    var CoverContentComponent =
    /*#__PURE__*/
    function () {
      function CoverContentComponent(activatedRoute, router, queriesService) {
        _classCallCheck(this, CoverContentComponent);

        this.activatedRoute = activatedRoute;
        this.router = router;
        this.queriesService = queriesService;
        this.data = [];
        this.main = _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3__;
      }

      _createClass(CoverContentComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          // refresca página

          /* this.queriesService.getRefresh(); */
          this.activatedRoute.params.subscribe(function (params) {
            _this.pagina = params['pag']; // JQuery ir arriba

            $('body, html').animate({
              scrollTop: '0px'
            }, 300);
          });
        }
      }, {
        key: "ngDoCheck",
        value: function ngDoCheck() {
          this.main = _assets_json_cover_content_enlaces_json__WEBPACK_IMPORTED_MODULE_3__;
          this.paginas();
        }
      }, {
        key: "paginas",
        value: function paginas() {
          // MENU ENLACES Y RECURSOS
          if (this.pagina == "otros-repositorios") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "recursos-uc") {
            this.main = this.main[this.pagina];
          }

          if (this.pagina == "patentes-uc") {
            this.main = this.main[this.pagina];
          }
        }
      }]);

      return CoverContentComponent;
    }();

    CoverContentComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_queries_service__WEBPACK_IMPORTED_MODULE_4__["QueriesService"]
      }];
    };

    CoverContentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cover-content',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./cover-content.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/cover-content/cover-content.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./cover-content.component.css */
      "./src/app/templates/cover-content/cover-content.component.css")).default]
    })], CoverContentComponent);
    /***/
  },

  /***/
  "./src/app/templates/cover-content/cover-content.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/templates/cover-content/cover-content.module.ts ***!
    \*****************************************************************/

  /*! exports provided: CoverContentModule */

  /***/
  function srcAppTemplatesCoverContentCoverContentModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoverContentModule", function () {
      return CoverContentModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _cover_content_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./cover-content-routing.module */
    "./src/app/templates/cover-content/cover-content-routing.module.ts");
    /* harmony import */


    var _cover_content_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./cover-content.component */
    "./src/app/templates/cover-content/cover-content.component.ts");

    var CoverContentModule = function CoverContentModule() {
      _classCallCheck(this, CoverContentModule);
    };

    CoverContentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_cover_content_component__WEBPACK_IMPORTED_MODULE_4__["CoverContentComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _cover_content_routing_module__WEBPACK_IMPORTED_MODULE_3__["CoverContentRoutingModule"]]
    })], CoverContentModule);
    /***/
  },

  /***/
  "./src/assets/json/cover-content/enlaces.json":
  /*!****************************************************!*\
    !*** ./src/assets/json/cover-content/enlaces.json ***!
    \****************************************************/

  /*! exports provided: otros-repositorios, recursos-uc, patentes-uc, default */

  /***/
  function srcAssetsJsonCoverContentEnlacesJson(module) {
    module.exports = JSON.parse("{\"otros-repositorios\":{\"horizon_header\":{\"active\":true,\"title\":\"Otros Repositorios\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Otros Repositorios\",\"title\":\"Te encuentras en la página Otros Repositorios de la sección Enlaces y Recursos\",\"url\":\"/enlaces/otros-repositorios\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Creemos que la colaboración e intercambio de información entre las distintas instituciones que estén generando investigación es fundamental para el nacimiento de nuevas ideas y soluciones. Compartimos a continuación una selección de directorios de repositorios nacionales e internacionales que disponen de valiosa información en acceso abierto.\"}},\"childs\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/apollo_logo.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Apollo University of Cambridge\",\"url\":\"https://www.repository.cam.ac.uk/\"},\"title\":\"Apollo - University of Cambridge\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/respositorio-academico-uchile.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio Académico Universidad de Chile\",\"url\":\"http://repositorio.uchile.cl/\"},\"title\":\"Repositorio Académico - Universidad de Chile\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/digital-csic.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio institucional del Consejo Superior de Investigaciones Científicas España.\",\"url\":\"https://digital.csic.es/\"},\"title\":\"Repositorio institucional del Consejo Superior de Investigaciones Científicas España.\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/upc.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio Universidad Politécnica de Cataluña, Barcelona, España\",\"url\":\"https://upcommons.upc.edu/\"},\"title\":\"Repositorio Universidad Politécnica de Cataluña, Barcelona, España\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/rua_medi.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio institucional de la Universidad de Alicante, España\",\"url\":\"http://rua.ua.es/dspace/\"},\"title\":\"Repositorio institucional de la Universidad de Alicante, España\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/universidad-complutense-madrid.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a E-prints Universidad Complutense\",\"url\":\"http://eprints.ucm.es/\"},\"title\":\"E-prints Universidad Complutense\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/repec.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a RePEc  Research Papers in Economic\",\"url\":\"http://repec.org/\"},\"title\":\"RePEc  Research Papers in Economic\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/philpapers.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a PhilPapers\",\"url\":\"https://philpapers.org/\"},\"title\":\"PhilPapers\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/lume.jpeg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio Digital LUME, Brasil\",\"url\":\"https://www.lume.ufrgs.br/\"},\"title\":\"Repositorio Digital LUME, Brasil\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/gredos.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a GREDOS Universidad de Salamanca\",\"url\":\"https://gredos.usal.es/\"},\"title\":\"GREDOS Universidad de Salamanca\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/dspace.gif\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a DSpace MIT\",\"url\":\"https://dspace.mit.edu/\"},\"title\":\"DSpace MIT\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/EuropePMC1.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Europe PMC\",\"url\":\"https://europepmc.org/\"},\"title\":\"Europe PMC\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/stacked-logo.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a eScholarship Universidad de California\",\"url\":\"https://escholarship.org/repository\"},\"title\":\"eScholarship Universidad de California\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"assets/img/eur_signature.png\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link a Repositorio de Tesis de la Universidad de Erasmus, Países Bajos\",\"url\":\"https://thesis.eur.nl/\"},\"title\":\"Repositorio de Tesis de la Universidad de Erasmus, Países Bajos\"}]},\"recursos-uc\":{\"horizon_header\":{\"active\":true,\"title\":\"Recursos UC\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Inicio\",\"title\":\"Ir a página de inicio del Repositorio\",\"url\":\"/\"},{\"text\":\"Recursos UC\",\"title\":\"Te encuentras en la página Recursos UC de la sección Enlaces y Recursos\",\"url\":\"/enlaces/recursos-uc\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Con el objetivo de potenciar su labor de investigación, preservación y difusión del conocimiento, la  Pontificia Universidad Católica de Chile ha puesto a disposición de toda la comunidad una serie de recursos en acceso abierto en distintas disciplinas.\"}},\"childs\":[{\"excerpt\":\"Archivo Histórico UC\",\"image\":\"http://archivohistorico.uc.cl/images/archivohistorico.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"Archivo Histórico UC\",\"url\":\"http://archivohistorico.uc.cl/\"},\"title\":\"Archivo Histórico UC\"},{\"excerpt\":\"Catálogo patrimonial UC\",\"image\":\"https://archivospatrimoniales.uc.cl/themes/Mirage2/images/slider-patrimonio05.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"Catálogo patrimonial UC\",\"url\":\"https://archivospatrimoniales.uc.cl/\"},\"title\":\"Catálogo patrimonial UC\"},{\"excerpt\":\"Revistas UC\",\"image\":\"assets/img/revistas.jpg\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"Revistas UC\",\"url\":\"http://guiastematicas.bibliotecas.uc.cl/editoresUC/editoresuc/listadorevistas\"},\"title\":\"Revistas UC\"}]},\"patentes-uc\":{\"horizon_header\":{\"active\":true,\"title\":\"Patentes UC\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Repositorio\",\"title\":\"Repositorio\",\"url\":\"/\"},{\"text\":\"Patentes UC\",\"title\":\"Patentes UC\",\"url\":\"/enlaces/patentes-uc\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium.\"}},\"childs\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit etiam urna est facilisis\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit etiam urna est facilisis\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua\",\"image\":\"https://picsum.photos/1000/1000\",\"link\":{\"icon\":\"open_in_new\",\"text\":\"Ir al sitio\",\"title\":\"link\",\"url\":\"#\"},\"title\":\"Lorem ipsum dolor sit amet\"}]}}");
    /***/
  }
}]);
//# sourceMappingURL=templates-cover-content-cover-content-module-es5.js.map