function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["templates-bibliometrics-bibliometrics-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.html":
  /*!***************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.html ***!
    \***************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPartialsCardsCardBibliometricsCardBibliometricsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"uc-card uc-card--gray card-height--same\">\n  <div class=\"uc-card_body p-32\">\n    <h3>{{ cardData.title }}</h3>\n    <div class=\"uc-text-divider divider-primary my-16\"></div>\n    <div class=\"uc-card_excerpt mb-24\">\n      <p>{{ cardData.excerpt }}</p>\n    </div>\n    <a title=\"{{ cardData.link.title }}\" href=\"{{ cardData.link.url }}\" class=\"uc-btn btn-primary mt-auto\" download>{{ cardData.link.text }}<i class=\"uc-icon icon-shape--rounded\">{{ cardData.link.icon }}</i></a>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/bibliometrics/bibliometrics.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/templates/bibliometrics/bibliometrics.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppTemplatesBibliometricsBibliometricsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"uc-section uc-section__header mb-60\" *ngIf=\"mainBibliometrics.horizon_header.active\">\n  <div class=\"container\">\n    <!-- <app-breadcrumbs [breadcrumbs]=\"mainBibliometrics['horizon_header']['section_breadcrumbs']['breadcrumbs']\" *ngIf=\"mainBibliometrics['horizon_header']['section_breadcrumbs']['active']\"></app-breadcrumbs> -->\n    <ul class=\"uc-breadcrumb mb-24\" *ngIf=\"mainBibliometrics['horizon_header']['section_breadcrumbs']['active']\">\n      <li *ngFor=\"let breadcrumb of mainBibliometrics['horizon_header']['section_breadcrumbs']['breadcrumbs']; last as l\" class=\"uc-breadcrumb_item{{(l) ? ' current' : '' }}\">\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"!breadcrumb['dynamic']\">{{ breadcrumb.text }}</a>\n        <a title=\"{{breadcrumb.title}}\" [routerLink]=\"[breadcrumb.url]\" *ngIf=\"breadcrumb['dynamic']\">{{ particular }}</a>\n        <i class=\"uc-icon\" *ngIf=\"!l\">keyboard_arrow_right</i>\n      </li>\n    </ul>\n\n    <div class=\"row\">\n      <div class=\"col-lg-8 col-12\">\n        <h1>{{ mainBibliometrics.horizon_header.title }}</h1>\n        <div class=\"uc-excerpt\" *ngIf=\"mainBibliometrics.horizon_header.section_excerpt.active\">\n          {{mainBibliometrics.horizon_header.section_excerpt.excerpt}}\n        </div>\n      </div>\n      <div class=\"offset-lg-1 col-lg-3 offset-0 col-12\">\n        <ng-container *ngIf=\"mainBibliometrics.horizon_header.section_sidebar.active && data['studies']\">\n          <span class=\"uc-subtitle d-block mb-16\">\n            {{ mainBibliometrics.horizon_header.section_sidebar.title }}\n          </span>\n          <ng-container *ngFor=\"let studies of data['studies']; let i = index\">\n            <a title=\"Sección\" href=\"#\" class=\"text-weight--500 d-block mt-8\" *ngIf=\"i != 0\" (click)=\"scrollTo('list_'+i, $event)\">Sección {{ i }} - {{ studies['ano'] }}</a>\n          </ng-container>\n        </ng-container>\n      </div>\n    </div>\n  </div>\n</section>\n\n<section class=\"uc-section uc-section__page pt-32\" *ngIf=\"data['studies']\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-lg-4 col mb-32\" *ngFor=\"let study of data['studies'][0]['studies']; let i = index\">\n        <app-card-bibliometrics [cardData]=\"study\" *ngIf=\"i < 4\"></app-card-bibliometrics>\n      </div>\n    </div>\n  </div>\n</section>\n\n<section class=\"uc-section uc-section__page\">\n  <div class=\"container\">\n    <hr class=\"uc-hr\">\n  </div>\n</section>";
    /***/
  },

  /***/
  "./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.css":
  /*!************************************************************************************!*\
    !*** ./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.css ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPartialsCardsCardBibliometricsCardBibliometricsComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhcnRpYWxzL2NhcmRzL2NhcmQtYmlibGlvbWV0cmljcy9jYXJkLWJpYmxpb21ldHJpY3MuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: CardBibliometricsComponent */

  /***/
  function srcAppPartialsCardsCardBibliometricsCardBibliometricsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CardBibliometricsComponent", function () {
      return CardBibliometricsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var CardBibliometricsComponent =
    /*#__PURE__*/
    function () {
      function CardBibliometricsComponent() {
        _classCallCheck(this, CardBibliometricsComponent);
      }

      _createClass(CardBibliometricsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CardBibliometricsComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], CardBibliometricsComponent.prototype, "cardData", void 0);
    CardBibliometricsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-card-bibliometrics',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./card-bibliometrics.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./card-bibliometrics.component.css */
      "./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.css")).default]
    })], CardBibliometricsComponent);
    /***/
  },

  /***/
  "./src/app/templates/bibliometrics/bibliometrics-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/templates/bibliometrics/bibliometrics-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: BibliometricsRoutingModule */

  /***/
  function srcAppTemplatesBibliometricsBibliometricsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BibliometricsRoutingModule", function () {
      return BibliometricsRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _bibliometrics_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./bibliometrics.component */
    "./src/app/templates/bibliometrics/bibliometrics.component.ts");

    var routes = [{
      path: '',
      component: _bibliometrics_component__WEBPACK_IMPORTED_MODULE_3__["BibliometricsComponent"]
    }];

    var BibliometricsRoutingModule = function BibliometricsRoutingModule() {
      _classCallCheck(this, BibliometricsRoutingModule);
    };

    BibliometricsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], BibliometricsRoutingModule);
    /***/
  },

  /***/
  "./src/app/templates/bibliometrics/bibliometrics.component.css":
  /*!*********************************************************************!*\
    !*** ./src/app/templates/bibliometrics/bibliometrics.component.css ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppTemplatesBibliometricsBibliometricsComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlcy9iaWJsaW9tZXRyaWNzL2JpYmxpb21ldHJpY3MuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/templates/bibliometrics/bibliometrics.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/templates/bibliometrics/bibliometrics.component.ts ***!
    \********************************************************************/

  /*! exports provided: BibliometricsComponent */

  /***/
  function srcAppTemplatesBibliometricsBibliometricsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BibliometricsComponent", function () {
      return BibliometricsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _assets_json_bibliometrics_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../assets/json/bibliometrics.json */
    "./src/assets/json/bibliometrics.json");

    var _assets_json_bibliometrics_json__WEBPACK_IMPORTED_MODULE_2___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/bibliometrics.json */
    "./src/assets/json/bibliometrics.json", 1);
    /* harmony import */


    var _assets_json_studies_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../assets/json/studies.json */
    "./src/assets/json/studies.json");

    var _assets_json_studies_json__WEBPACK_IMPORTED_MODULE_3___namespace =
    /*#__PURE__*/
    __webpack_require__.t(
    /*! ../../../assets/json/studies.json */
    "./src/assets/json/studies.json", 1);
    /* harmony import */


    var _services_queries_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/queries.service */
    "./src/app/services/queries.service.ts");

    var BibliometricsComponent =
    /*#__PURE__*/
    function () {
      function BibliometricsComponent(queriesService) {
        _classCallCheck(this, BibliometricsComponent);

        this.queriesService = queriesService;
        this.data = [];
        this.mainBibliometrics = {};
      }

      _createClass(BibliometricsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          // refresca página
          this.queriesService.getRefresh();
          this.mainBibliometrics = _assets_json_bibliometrics_json__WEBPACK_IMPORTED_MODULE_2__;
          this.data['studies'] = _assets_json_studies_json__WEBPACK_IMPORTED_MODULE_3__; // JQuery ir arriba

          $('body, html').animate({
            scrollTop: '0px'
          }, 300);
        }
      }, {
        key: "scrollTo",
        value: function scrollTo(element, $event) {
          $event.preventDefault();
          document.getElementById(element).scrollIntoView({
            behavior: 'smooth'
          });
        }
      }]);

      return BibliometricsComponent;
    }();

    BibliometricsComponent.ctorParameters = function () {
      return [{
        type: _services_queries_service__WEBPACK_IMPORTED_MODULE_4__["QueriesService"]
      }];
    };

    BibliometricsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-bibliometrics',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./bibliometrics.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/templates/bibliometrics/bibliometrics.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./bibliometrics.component.css */
      "./src/app/templates/bibliometrics/bibliometrics.component.css")).default]
    })], BibliometricsComponent);
    /***/
  },

  /***/
  "./src/app/templates/bibliometrics/bibliometrics.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/templates/bibliometrics/bibliometrics.module.ts ***!
    \*****************************************************************/

  /*! exports provided: BibliometricsModule */

  /***/
  function srcAppTemplatesBibliometricsBibliometricsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BibliometricsModule", function () {
      return BibliometricsModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _bibliometrics_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./bibliometrics-routing.module */
    "./src/app/templates/bibliometrics/bibliometrics-routing.module.ts");
    /* harmony import */


    var _bibliometrics_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./bibliometrics.component */
    "./src/app/templates/bibliometrics/bibliometrics.component.ts");
    /* harmony import */


    var _partials_cards_card_bibliometrics_card_bibliometrics_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../partials/cards/card-bibliometrics/card-bibliometrics.component */
    "./src/app/partials/cards/card-bibliometrics/card-bibliometrics.component.ts");

    var BibliometricsModule = function BibliometricsModule() {
      _classCallCheck(this, BibliometricsModule);
    };

    BibliometricsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_bibliometrics_component__WEBPACK_IMPORTED_MODULE_4__["BibliometricsComponent"], _partials_cards_card_bibliometrics_card_bibliometrics_component__WEBPACK_IMPORTED_MODULE_5__["CardBibliometricsComponent"]],
      exports: [],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _bibliometrics_routing_module__WEBPACK_IMPORTED_MODULE_3__["BibliometricsRoutingModule"]]
    })], BibliometricsModule);
    /***/
  },

  /***/
  "./src/assets/json/bibliometrics.json":
  /*!********************************************!*\
    !*** ./src/assets/json/bibliometrics.json ***!
    \********************************************/

  /*! exports provided: horizon_header, default */

  /***/
  function srcAssetsJsonBibliometricsJson(module) {
    module.exports = JSON.parse("{\"horizon_header\":{\"active\":true,\"title\":\"Informes bibliométricos\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Repositorio\",\"title\":\"Repositorio\",\"url\":\"/\"},{\"text\":\"UC Institucional\",\"title\":\"UC Institucional\",\"url\":\"/\"},{\"text\":\"Informes bibliométricos\",\"title\":\"Informes bibliométricos\",\"url\":\"/bibliometrics\"}]},\"section_excerpt\":{\"active\":true,\"excerpt\":\"Bibliotecas UC ofrece este servicio que permite conocer el impacto académico en el plano internacional utilizando como fuente de información los artículos científicos publicados en revistas indizadas en la bases de datos Web of Science, Scopus y SciELO WoS durante los últimos años. El análisis de la productividad y el rendimiento de la investigación es realizado con InCites®, herramienta bibliométrica de evaluación de la investigación. Este software permite acceder a la información bibliográfica de la base de datos Web of Science y obtener directamente los indicadores de productividad, impacto, colaboración y métricas comparativas. Así mismo, el análisis de Scopus se realiza a través de Scimago Journal & Country Rank y la propia herramienta bibliométrica de Scopus: CiteScore.\"},\"section_sidebar\":{\"active\":false,\"title\":\"En esta sección\"}}}");
    /***/
  },

  /***/
  "./src/assets/json/studies.json":
  /*!**************************************!*\
    !*** ./src/assets/json/studies.json ***!
    \**************************************/

  /*! exports provided: 0, 1, 2, 3, 4, default */

  /***/
  function srcAssetsJsonStudiesJson(module) {
    module.exports = JSON.parse("[{\"ano\":2019,\"studies\":[{\"excerpt\":\"Informe que declara las publicaciones citables de la UC -entre 2013-2017- extraídas en junio del 2019 de las bases de datos Web of Science y SciELO (WoS). Se consideran publicaciones citables aquellas de tipo article, review y proceedings paper, por tanto, contribuyen y tienen gran impacto en las disciplinas del conocimiento.\",\"link\":{\"icon\":\"get_app\",\"title\":\"Descargar publicaciones citables UC 2013-2017\",\"text\":\"Descargar\",\"url\":\"/assets/download/estudios/Copia de IB_Datos_Publicaciones citables UC 2013-2017.xlsx\"},\"title\":\"Publicaciones citables UC 2013-2017\"},{\"excerpt\":\"Informe que da cuenta de la colaboración entre la UC y University of Edinburgh -en el período 2013-2017- extraída en 2018 y analizada con la data de publicaciones de Scopus. Se identificaron indicadores de impacto de autores que presentan mayor productividad en colaboración, así también, aquellos que potencialmente pueden colaborar.\",\"link\":{\"icon\":\"get_app\",\"title\":\"Descargar Colaboración UC-Edinburg\",\"text\":\"Descargar\",\"url\":\"assets/download/estudios/Copia de IB_Datos_Publicaciones citables UC 2013-2017.xlsx\"},\"title\":\"Colaboración UC-Edinburg\"},{\"excerpt\":\"Informe que da cuenta del impacto y colaboración nacional e internacional de las publicaciones de la UC -entre 2011-2017- extraídas a fines del 2017 desde las bases de datos Web of Science (ISI) y la herramienta bibliométrica InCites.\",\"link\":{\"icon\":\"get_app\",\"title\":\"Descargar Publicaciones UC 2011-2017\",\"text\":\"Descargar\",\"url\":\"assets/download/estudios/Copia de IB_Datos_Publicaciones citables UC 2013-2017.xlsx\"},\"title\":\"Publicaciones UC 2011-2017\"}]},{\"ano\":2018,\"studies\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"}]},{\"ano\":2017,\"studies\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"}]},{\"ano\":2016,\"studies\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"}]},{\"ano\":2015,\"studies\":[{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"},{\"excerpt\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa\",\"link\":{\"icon\":\"get_app\",\"title\":\"File\",\"text\":\"Descargar\",\"url\":\"#\"},\"title\":\"Nombre del archivo\"}]}]");
    /***/
  }
}]);
//# sourceMappingURL=templates-bibliometrics-bibliometrics-module-es5.js.map