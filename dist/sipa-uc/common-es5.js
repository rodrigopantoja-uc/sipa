(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
  /***/
  "./src/assets/json/blog/blog.json":
  /*!****************************************!*\
    !*** ./src/assets/json/blog/blog.json ***!
    \****************************************/

  /*! exports provided: horizon_header, horizon_tags, horizon_content, horizon_relacionados, horizon_social, horizon_formulario_comentario, horizon_comentarios, default */

  /***/
  function srcAssetsJsonBlogBlogJson(module) {
    module.exports = JSON.parse("{\"horizon_header\":{\"active\":true,\"title\":\"Blog\",\"section_breadcrumbs\":{\"active\":true,\"breadcrumbs\":[{\"text\":\"Repositorio\",\"title\":\"Repositorio\",\"url\":\"/\"},{\"text\":\"Noticias\",\"title\":\"Noticias\",\"url\":\"/blog-list\"},{\"text\":\"Noticia\",\"title\":\"Noticia\",\"url\":\"/blog-list\"}]},\"section_slider\":{\"active\":true,\"title\":\"\"}},\"horizon_tags\":{\"active\":true,\"title\":\"Información\"},\"horizon_content\":{\"active\":true,\"title\":\"\",\"excerpt\":\"\"},\"horizon_relacionados\":{\"active\":true,\"title\":\"Investigaciones relacionadas\",\"excerpt\":\"\",\"link\":{\"text\":\"Ver otras investigaciones\",\"title\":\"Investigacion\",\"url\":\"#\",\"icon\":\"arrow_forward\"}},\"horizon_social\":{\"active\":true,\"title\":\"\",\"excerpt\":\"\",\"sections\":{\"like\":{\"active\":true,\"title\":\"¿Te gusta esta publicación?\"},\"share\":{\"active\":true,\"title\":\"Comparte esta publicación\"}}},\"horizon_formulario_comentario\":{\"active\":true,\"title\":\"Deja tu comentario\",\"excerpt\":\"\",\"forms\":{\"textarea\":{\"name\":\"comment_textarea\",\"placeholder\":\"Escribe acá tu comentario\"},\"button\":{\"name\":\"comment_button\",\"text\":\"Comentar\"},\"reset\":{\"text\":\"Comentar de nuevo\"}}},\"horizon_comentarios\":{\"active\":true,\"title\":\"Comentarios\",\"excerpt\":\"\"}}");
    /***/
  }
}]);
//# sourceMappingURL=common-es5.js.map