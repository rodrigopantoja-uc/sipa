<?php
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");
    header('Content-Type: text/html; charset=utf-8');
    
   
    
    $json = '{"entorno":{
          "php"                     :"http://repositorio.lib.uc.cl/assets/php",
          "php_img"                 :"http://repositorio.lib.uc.cl:8080", 
          
          "php_upload"              :"https://repositorio.uc.cl",
          "php_ficha"               :"http://repositorio.lib.uc.cl/assets/php/ficha.php?",
          "php_ultimos_agregados"   : "http://repositorio.lib.uc.cl/assets/php/ultimos-agregados.php"
        }}';
    
    echo $json;

?>
