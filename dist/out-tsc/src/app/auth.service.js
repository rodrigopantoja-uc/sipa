import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpRequest, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/mergeMap';
let AuthService = class AuthService {
    constructor(http) {
        this.http = http;
        const m = window.location.href.match(/(.*)[&?]ticket=([^&?]*)$/);
        console.log(m);
        if (m) {
            const [_, ourUrl, ticket] = m;
            console.log("got ticket from url " + ticket);
            this.ticket2bearer(ticket, ourUrl).finally(() => {
                //remove from url:
                history.replaceState({}, null, ourUrl);
            }).subscribe(bearer => {
                this.bearer = bearer;
                console.log("got bearer from login " + this.bearer);
                sessionStorage["bearer"] = this.bearer;
            });
        }
        else if (sessionStorage["bearer"]) {
            this.bearer = sessionStorage["bearer"];
            console.log("got bearer from sessionStorage " + this.bearer);
        }
        else {
            this.login();
        }
    }
    ticket2bearer(ticket, service) {
        console.log(ticket);
        const url = `/backend/login.php?service=${encodeURIComponent(service)}&ticket=${encodeURIComponent(ticket)}`;
        return this.http.handle(new HttpRequest('GET', url)).mergeMap(resp => (resp instanceof HttpResponse && resp.body && [resp.body['bearer']] || []));
    }
    login() {
        // console.log('Soy tu padre');
        window.location.href = `${environment.cas_login_url}?service=${encodeURIComponent(window.location.href)}`;
    }
    requestAddBearer(req) {
        return req.clone({ setHeaders: { 'X-Authorization': `Bearer ${this.bearer}` } });
    }
};
AuthService = tslib_1.__decorate([
    Injectable()
], AuthService);
export { AuthService };
//# sourceMappingURL=auth.service.js.map