import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let SliderComponent = class SliderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input()
], SliderComponent.prototype, "sliderData", void 0);
SliderComponent = tslib_1.__decorate([
    Component({
        selector: 'app-slider',
        templateUrl: './slider.component.html',
        styleUrls: ['./slider.component.css']
    })
], SliderComponent);
export { SliderComponent };
//# sourceMappingURL=slider.component.js.map