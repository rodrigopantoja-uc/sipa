import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardNormalComponent = class CardNormalComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input()
], CardNormalComponent.prototype, "data", void 0);
tslib_1.__decorate([
    Input()
], CardNormalComponent.prototype, "divider", void 0);
tslib_1.__decorate([
    Input()
], CardNormalComponent.prototype, "excerpt", void 0);
CardNormalComponent = tslib_1.__decorate([
    Component({
        selector: 'app-card-normal',
        templateUrl: './card-normal.component.html',
        styleUrls: ['./card-normal.component.css']
    })
], CardNormalComponent);
export { CardNormalComponent };
//# sourceMappingURL=card-normal.component.js.map