import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardPublicationsComponent = class CardPublicationsComponent {
    constructor() {
        this.vincularme = [];
    }
    ngOnInit() {
    }
    cheq(event) {
        if (event.target.checked) {
            this.vincularme.push(event.target.value);
        }
        else {
            for (var i = 0; this.vincularme.length > i; i++) {
                if (this.vincularme[i] == event.target.value) {
                    this.vincularme.splice(i, 1);
                }
            }
        }
        console.log(this.vincularme);
    }
};
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "data", void 0);
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "heightSame", void 0);
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "urlImg", void 0);
tslib_1.__decorate([
    Input()
], CardPublicationsComponent.prototype, "vinculado", void 0);
CardPublicationsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-card-publications',
        templateUrl: './card-publications.component.html',
        styleUrls: ['./card-publications.component.css']
    })
], CardPublicationsComponent);
export { CardPublicationsComponent };
//# sourceMappingURL=card-publications.component.js.map