import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardTopicComponent = class CardTopicComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    fotoerror() {
        alert("no hay");
    }
    getSearch(topic) {
        var array_Filtros = [
            {
                search_by: 'tema',
                contains: 'es',
                term: topic
            }
        ];
        localStorage.setItem('json_filtros', JSON.stringify(array_Filtros));
        localStorage.setItem('searchAdvanced', 'true');
        localStorage.setItem('search_form', '');
        this.router.navigate(['/busqueda']);
    }
};
tslib_1.__decorate([
    Input()
], CardTopicComponent.prototype, "cardTopicData", void 0);
tslib_1.__decorate([
    Input()
], CardTopicComponent.prototype, "cardTopicIndex", void 0);
tslib_1.__decorate([
    Input()
], CardTopicComponent.prototype, "cardTopicImage", void 0);
CardTopicComponent = tslib_1.__decorate([
    Component({
        selector: 'app-card-topic',
        templateUrl: './card-topic.component.html',
        styleUrls: ['./card-topic.component.css']
    })
], CardTopicComponent);
export { CardTopicComponent };
//# sourceMappingURL=card-topic.component.js.map