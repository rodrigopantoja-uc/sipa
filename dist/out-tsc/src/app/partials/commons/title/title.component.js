import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let TitleComponent = class TitleComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input()
], TitleComponent.prototype, "title", void 0);
tslib_1.__decorate([
    Input()
], TitleComponent.prototype, "type", void 0);
TitleComponent = tslib_1.__decorate([
    Component({
        selector: 'app-title',
        templateUrl: './title.component.html',
        styleUrls: ['./title.component.css']
    })
], TitleComponent);
export { TitleComponent };
//# sourceMappingURL=title.component.js.map