import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let BreadcrumbsComponent = class BreadcrumbsComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib_1.__decorate([
    Input()
], BreadcrumbsComponent.prototype, "breadcrumbs", void 0);
tslib_1.__decorate([
    Input()
], BreadcrumbsComponent.prototype, "particular", void 0);
BreadcrumbsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-breadcrumbs',
        templateUrl: './breadcrumbs.component.html',
        styleUrls: ['./breadcrumbs.component.css']
    })
], BreadcrumbsComponent);
export { BreadcrumbsComponent };
//# sourceMappingURL=breadcrumbs.component.js.map