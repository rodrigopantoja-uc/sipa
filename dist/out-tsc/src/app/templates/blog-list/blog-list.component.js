import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from '../../../assets/json/blog/blog.json';
import jsonHome from '../../../assets/json/home.json';
import { global } from '../../services/global';
let BlogListComponent = class BlogListComponent {
    constructor(activatedRoute, queriesService) {
        this.activatedRoute = activatedRoute;
        this.queriesService = queriesService;
        this.data = [];
        this.json = json;
        this.jsonHome = jsonHome;
        this.urlPhp = global.php;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        // Recibo datos por get
        this.activatedRoute.params.subscribe(params => {
            this.data['id'] = params['id'];
        });
        // NOVEDADES NEWS MAILCHIMP
        this.queriesService.queryGet(this.urlPhp + '/api-mailchimp/landing-page.php').then((data) => {
            this.data['news'] = data['landing_pages'].reverse();
            //console.log(data);
        });
    }
};
BlogListComponent = tslib_1.__decorate([
    Component({
        selector: 'app-blog-list',
        templateUrl: './blog-list.component.html',
        styleUrls: ['./blog-list.component.css']
    })
], BlogListComponent);
export { BlogListComponent };
//# sourceMappingURL=blog-list.component.js.map