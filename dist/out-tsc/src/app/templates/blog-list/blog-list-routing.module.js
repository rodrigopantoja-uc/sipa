import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BlogListComponent } from './blog-list.component';
const routes = [
    { path: '', component: BlogListComponent }
];
let BlogListRoutingModule = class BlogListRoutingModule {
};
BlogListRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], BlogListRoutingModule);
export { BlogListRoutingModule };
//# sourceMappingURL=blog-list-routing.module.js.map