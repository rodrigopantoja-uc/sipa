import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BibliometricsComponent } from './bibliometrics.component';
const routes = [
    { path: '', component: BibliometricsComponent }
];
let BibliometricsRoutingModule = class BibliometricsRoutingModule {
};
BibliometricsRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], BibliometricsRoutingModule);
export { BibliometricsRoutingModule };
//# sourceMappingURL=bibliometrics-routing.module.js.map