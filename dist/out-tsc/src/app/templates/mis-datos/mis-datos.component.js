import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let MisDatosComponent = class MisDatosComponent {
    constructor() {
        this.loggedIn = false;
        this.datos_academicos = [];
    }
    ngOnInit() {
        //document.getElementById('top').scrollIntoView({behavior: 'smooth'}); // Scroll hacia buscador
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    }
    ngDoCheck() {
        // COMPUEBO LOGIN
        if (localStorage.getItem('correo')) {
            this.loggedIn = true;
            this.usuario = localStorage.getItem('usuario');
            this.correo = localStorage.getItem('correo');
            this.unidad = localStorage.getItem('unidad');
            if (localStorage.getItem('jsonAcademico') == "") {
                this.datos_academicos = false;
            }
            else {
                this.datos_academicos = JSON.parse(localStorage.getItem('jsonAcademico'));
            }
        }
        else {
            this.loggedIn = false;
        }
    }
};
MisDatosComponent = tslib_1.__decorate([
    Component({
        selector: 'app-mis-datos',
        templateUrl: './mis-datos.component.html',
        styleUrls: ['./mis-datos.component.css']
    })
], MisDatosComponent);
export { MisDatosComponent };
//# sourceMappingURL=mis-datos.component.js.map