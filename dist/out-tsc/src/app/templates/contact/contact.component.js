import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './contact.json';
import Faqs from '../../../assets/json/faq/preguntas-frecuentes.json';
import { global } from '../../services/global';
let ContactComponent = class ContactComponent {
    constructor(queriesService) {
        this.queriesService = queriesService;
        this.data = [];
        this.main = {};
        this.response = {
            'show': false,
            'data': ''
        };
        this.name = localStorage.getItem('usuario');
        this.email = localStorage.getItem('correo');
        this.urlPhp = global.php;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        this.main = json;
        this.data['faqs'] = Faqs['preguntas-frecuente'];
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    }
    resetForm() {
        this.response.show = false;
        this.response.text = '';
    }
    submitContact(form, $event) {
        $event.preventDefault();
        if (form.form.value)
            this.response.show = true;
        this.queriesService.queryGet(this.urlPhp + '/mail-contacto/envio.php?nombreApellido=' + this.name + '&mail=' + this.email + '&consulta=' + this.message).then((data) => {
            if (data['respuesta'] == true) {
                this.response.show = true;
                this.response.text = 'Su mensaje ha sido enviado con éxito';
                this.response.icono = 'check_circle';
                this.response.color = 'color-verde';
                //form.form.reset();
                /* this.name = "";
                this.email = ""; */
                this.message = "";
            }
            else {
                //console.log(data);
                this.response.show = true;
                this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
                this.response.icono = 'highlight_off';
                this.response.color = 'texto-dg-rojo';
            }
        }, (error) => {
            console.log(error);
            this.response.show = true;
            this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
            this.response.icono = 'highlight_off';
            this.response.color = 'texto-dg-rojo';
        });
    }
};
ContactComponent = tslib_1.__decorate([
    Component({
        selector: 'app-contact',
        templateUrl: './contact.component.html',
        styleUrls: ['./contact.component.css']
    })
], ContactComponent);
export { ContactComponent };
//# sourceMappingURL=contact.component.js.map