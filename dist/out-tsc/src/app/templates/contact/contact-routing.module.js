import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
const routes = [
    { path: '', component: ContactComponent }
];
let ContactRoutingModule = class ContactRoutingModule {
};
ContactRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], ContactRoutingModule);
export { ContactRoutingModule };
//# sourceMappingURL=contact-routing.module.js.map