import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './cover-novelty.json';
let CoverNoveltyComponent = class CoverNoveltyComponent {
    constructor(queriesService, _sanitizer) {
        this.queriesService = queriesService;
        this._sanitizer = _sanitizer;
        this.data = [];
        this.json = json;
    }
    ngOnInit() {
        this.queriesService.queryGet('http://localhost:3000/news').then((data) => { this.data['news'] = data; });
    }
    getBackgroundImage(image) {
        return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
    }
};
CoverNoveltyComponent = tslib_1.__decorate([
    Component({
        selector: 'app-cover-novelty',
        templateUrl: './cover-novelty.component.html',
        styleUrls: ['./cover-novelty.component.css']
    })
], CoverNoveltyComponent);
export { CoverNoveltyComponent };
//# sourceMappingURL=cover-novelty.component.js.map