import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let DescargaGaComponent = class DescargaGaComponent {
    constructor(activatedRoute, meta, title, _location, queriesService) {
        this.activatedRoute = activatedRoute;
        this.meta = meta;
        this.title = title;
        this._location = _location;
        this.queriesService = queriesService;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        // Recibo nombre de la página por get (descarga-ficha/nombrepagina)
        this.activatedRoute.params.subscribe(params => {
            // Pone nombre de la página en title
            this.title.setTitle(params['titulo']);
        });
        this._location.back();
    }
};
DescargaGaComponent = tslib_1.__decorate([
    Component({
        selector: 'app-descarga-ga',
        templateUrl: './descarga-ga.component.html',
        styleUrls: ['./descarga-ga.component.css']
    })
], DescargaGaComponent);
export { DescargaGaComponent };
//# sourceMappingURL=descarga-ga.component.js.map