import * as tslib_1 from "tslib";
import { Component, EventEmitter, Output } from '@angular/core';
import json from '../../../assets/json/search-advanced/search-advanced.json';
import { global } from '../../services/global';
let BusquedaComponent = class BusquedaComponent {
    constructor(queriesService, activatedRoute, dataService) {
        this.queriesService = queriesService;
        this.activatedRoute = activatedRoute;
        this.dataService = dataService;
        this.propagar = new EventEmitter();
        this.json = json;
        this.data = [];
        this.valorSearch = "";
        this.array_Filtros = [];
        this.npage = 1;
        this.paginacion = [];
        this.tipodoc = [];
        this.autores = [];
        this.tematicas = [];
        this.materias = [];
        this.condicion_publicar = null;
        this.modal = false;
        this.modal_ficha = false;
        this.ver_mas = false;
        this.array_vincular = [];
        this.emailSolicitar = null;
        this.metadatos_simple = [];
        this.error = false;
        this.tipodata = "sencillo";
        this.response = {
            'show': false,
            'data': ''
        };
        this.name = localStorage.getItem('usuario');
        this.email = localStorage.getItem('correo');
        this.urlImg = global.php_img + "/bitstream/handle";
        this.urlPhp = global.php + "/discovery-sipa.php?"; //  discovery_sipa     des-discovery_sipa.php
        this.urlEmail = global.php + "/mail-contacto/envio.php?";
        this.urlFicha = global.php + "/ficha.php?";
        this.urlHandle = global.dominio + "/handle/";
        this.urldownload = global.php_download + "/bitstream/handle";
    }
    ngOnInit() {
        this.dataService.entrada = false;
        this.tituloModal = "Términos y condiciones del servicio";
        if (localStorage.getItem("declainer")) {
            this.condicion_publicar = localStorage.getItem("declainer");
            this.modal = false;
        }
        else {
            this.condicion_publicar = null;
            this.modal = true;
        }
        this.data['filtroActivo'] = false; // oculta div de filtros
        this.npage = 1;
        /* this.buscar(this.npage,"", ""); */
        this.entrada = false;
        //document.getElementById('buscador').scrollIntoView({behavior: 'smooth'}); // Scroll hacia buscador
    }
    ngDoCheck() {
    }
    condiciones(event) {
        if (event.target.checked) {
            this.condicion_publicar = event.target.value;
            localStorage.setItem("declainer", this.condicion_publicar);
        }
        else {
            this.condicion_publicar = null;
            localStorage.removeItem("declainer");
            this.modal = true;
        }
    }
    abreModal() {
        this.modal = true;
    }
    cerrarModal() {
        this.condicion_publicar = "condiciones";
        localStorage.setItem("declainer", 'condiciones');
        this.modal = false;
    }
    abreModal_ficha(i, handle) {
        this.modal_ficha = true;
        this.index_ficha = i;
        this.tituloModal = "";
        this.queriesService.queryGet(this.urlFicha + 'handle=' + handle).then((data) => {
            console.log(data);
            if (data['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                if (data['response']['docs'].length > 0) {
                    this.error = false;
                    this.data['publics'] = data['response']['docs'][0];
                    //data['miniatura'];
                    if (data['miniatura']) {
                        this.foto = this.urlImg + "/" + data['response']['docs'][0]['handle'] + "/" + data['miniatura'];
                    }
                    else {
                        /* this.foto = "assets/img/logo_uc_linea.png"; */
                        this.foto = null;
                    }
                    // Archivo en embargo
                    if (data['embargo'] == 200 || data['embargo'] == 201) {
                        this.embargo = false;
                    }
                    else {
                        this.embargo = true;
                    }
                    // Construye tabla metadatos
                    let list = this.data['publics'];
                    for (let i in list) {
                        if (i.substr(0, 3) == 'dc.' && i.indexOf('_') == -1) {
                            /* cambiar nombres */
                            var alias = i;
                            var link = false;
                            var mostrar = true;
                            if (i == "dc.concurso") {
                                alias = "Concurso Arte y Cultura";
                                link = false;
                            }
                            if (i == "dc.contributor.advisor") {
                                alias = "Profesor guía";
                                link = false;
                            }
                            if (i == "dc.contributor.author") {
                                alias = "Autor";
                                link = false;
                            }
                            if (i == "dc.contributor.other") {
                                alias = "Otro autor";
                                link = false;
                            }
                            if (i == "dc.contributor.editor") {
                                alias = "Editor";
                                link = false;
                            }
                            if (i == "dc.contributor.illustrator") {
                                alias = "Ilustrador";
                                link = false;
                            }
                            if (i == "dc.date" || i == "dc.date.accessioned" || i == "dc.date.available") { /* eliminar ??? */
                                alias = "nulo";
                                link = false;
                                mostrar = false;
                            }
                            if (i == "dc.date.created") {
                                alias = "Fecha de creación";
                                link = false;
                            }
                            if (i == "dc.date.issued") {
                                alias = "Fecha de publicación";
                                link = false;
                            }
                            if (i == "dc.date.updated") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.description") {
                                alias = "Nota";
                                link = false;
                            }
                            if (i == "dc.description.abstract") {
                                alias = "Resumen";
                                link = false;
                            }
                            if (i == "dc.description.tableofcontents") {
                                alias = "Tabla de contenido";
                                link = false;
                            }
                            if (i == "dc.description.uri") {
                                alias = "Enlace";
                                link = false;
                            }
                            if (i == "dc.description.version") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.estamento.1" || i == "dc.estamento.2" || i == "dc.estamento.3" || i == "dc.estamento.4") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.format.extent") {
                                alias = "Paginación";
                                link = false;
                            }
                            if (i == "dc.fuente.origen") { /* eliminar?? */
                                alias = "nulo";
                                link = false;
                                mostrar = false;
                            }
                            if (i == "dc.identifier.codUA" || i == "dc.identifier.codUA1" || i == "dc.identifier.codUA2" || i == "dc.identifier.codUA3" || i == "dc.identifier.codUA4" || i == "dc.identifier.codUA5" || i == "dc.identifier.codUA6" || i == "dc.identifier.codUA7" || i == "dc.identifier.codUA8" || i == "dc.identifier.codUA9" || i == "dc.identifier.codUA10") {
                                alias = "Unidad académica";
                                link = false;
                            }
                            if (i == "dc.identifier.citation") {
                                alias = "Cómo citar este documento";
                                link = false;
                            }
                            if (i == "dc.identifier.converisid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.identifier.dialnetid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.identifier.doi") {
                                alias = "DOI";
                                link = true;
                            }
                            if (i == "dc.identifier.eisbn") {
                                alias = "ISBN electrónico";
                                link = false;
                            }
                            if (i == "dc.identifier.eissn") {
                                alias = "ISSN electrónico";
                                link = false;
                            }
                            if (i == "dc.identifier.isbn") {
                                alias = "ISBN";
                                link = false;
                            }
                            if (i == "dc.identifier.issn") {
                                alias = "ISSN";
                                link = false;
                            }
                            if (i == "dc.identifier.pubmedid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.identifier.scopusid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.issue.numero") {
                                alias = "Número de publicación";
                                link = false;
                            }
                            if (i == "dc.identifier.orcid" || i == "dc.identifier.orcid1" || i == "dc.identifier.orcid2" || i == "dc.identifier.orcid3" || i == "dc.identifier.orcid4" || i == "dc.identifier.orcid5" || i == "dc.identifier.orcid6" || i == "dc.identifier.orcid7" || i == "dc.identifier.orcid8" || i == "dc.identifier.orcid9" || i == "dc.identifier.orcid10") {
                                alias = "Número ORCID";
                                link = false;
                            }
                            if (i == "dc.identifier.uri") {
                                alias = "Enlace";
                                link = true;
                            }
                            if (i == "dc.identifier.wosid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.informacion.autoruc") {
                                alias = "Información del autor UC";
                                link = false;
                            }
                            if (i == "dc.language.iso") {
                                alias = "nulo"; /* Idioma */
                                link = false;
                            }
                            if (i == "dc.pagina.final") {
                                alias = "Página final";
                                link = false;
                            }
                            if (i == "dc.pagina.inicio") {
                                alias = "Página inicio";
                                link = false;
                            }
                            if (i == "dc.publisher") {
                                alias = "Editorial";
                                link = false;
                            }
                            if (i == "dc.relation") {
                                alias = "Publicación relacionada";
                                link = false;
                            }
                            if (i == "dc.relation.isformatof") {
                                alias = "Se encuentra en";
                                link = false;
                            }
                            if (i == "dc.relation.ispartofseries") {
                                alias = "Serie";
                                link = false;
                            }
                            if (i == "dc.relation.ispartof") {
                                alias = "Publicado en";
                                link = false;
                            }
                            if (i == "dc.revista") {
                                alias = "Revista";
                                link = false;
                            }
                            if (i == "dc.rights") {
                                alias = "Derechos";
                                link = false;
                            }
                            if (i == "dc.rights.holder") {
                                alias = "Titular de los derechos";
                                link = false;
                            }
                            if (i == "dc.relation.uri") {
                                alias = "Enlace relacionada";
                                link = true;
                            }
                            if (i == "dc.subject.ddc") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.subject.dewey") {
                                alias = "Temática";
                                link = false;
                            }
                            if (i == "dc.subject.other") {
                                alias = "Materia";
                                link = false;
                            }
                            if (i == "dc.subject") {
                                alias = "Palabra clave";
                                link = false;
                            }
                            if (i == "dc.title.alternative") {
                                alias = "Otro título";
                                link = false;
                            }
                            if (i == "dc.title") {
                                alias = "Título";
                                link = false;
                            }
                            if (i == "dc.type") {
                                alias = "Tipo de documento";
                                link = false;
                            }
                            if (i == "dc.volumen") {
                                alias = "Volumen";
                                link = false;
                            }
                            if (i == "dc.zcode" || i == "dc.zcode.1" || i == "dc.zcode.2" || i == "dc.zcode.3" || i == "dc.zcode.4" || i == "dc.zcode.5" || i == "dc.zcode.6" || i == "dc.zcode.7" || i == "dc.zcode.8" || i == "dc.zcode.9" || i == "dc.zcode.10") {
                                alias = "nulo";
                                link = false;
                                mostrar = false;
                            }
                            /* if(mostrar){
                              this.metadatos_simple.push( { "metadato": alias, "cod":i, "valor": list[i], "link":link });
                            } */
                            this.metadatos_simple.push({ "metadato": alias, "cod": i, "valor": list[i], "link": link, "mostrar": mostrar });
                        }
                    }
                    this.metadatos_simple.sort((a, b) => (a.metadato > b.metadato) ? 1 : -1);
                    console.log(this.metadatos_simple);
                }
                else {
                    this.error = true;
                }
            }
        });
    }
    cerrarModal_ficha() {
        this.metadatos_simple = [];
        this.modal_ficha = false;
    }
    cerrarModal_declainer() {
        this.modal = false;
    }
    metadatas(reg) {
        this.tipodata = reg;
    }
    vincular(data, titulo, fuente, autores, fecha, tag, revista, capitulo, handle, event) {
        /*   this.array_vincular.push({
            tag: tag,
            titulo: titulo,
            autores: autores,
            fecha_publicacion: fecha,
            revista: revista,
            capitulo: capitulo,
            handle: handle,
            boton: true
          })
          this.dataService.postId = this.array_vincular; */
        if (!this.array_vincular) {
            this.array_vincular = [];
        }
        if (event.target.checked) {
            this.array_vincular.push({
                handle: event.target.value
            });
            this.propagar.emit({ data, titulo, fuente, autores, fecha, tag, revista, capitulo, handle });
        }
        else {
            for (var i = 0; this.array_vincular.length > i; i++) {
                if (this.array_vincular[i] == event.target.value) {
                    this.array_vincular.splice(i, 1);
                }
            }
        }
    }
    registra() {
        this.propagar.emit("registrar");
    }
    buscar(page, filtro, orden) {
        document.getElementById('buscador').scrollIntoView({ behavior: 'smooth' }); //Scroll hacia div resultados
        this.cargando = true;
        this.msj = null;
        this.dataService.entrada = true;
        let str_filtros = '';
        let urlFiltro;
        this.npage = page;
        localStorage.setItem('page', page);
        page = (page - 1) * 20;
        this.orden = orden;
        // SI VIENE DE ESTRA PÁGINA
        if (filtro == "busqueda") {
            localStorage.setItem('search_form', this.valorSearch);
        }
        // SI VIENE DE HOME AREAS TEMATICAS Y PAGINAS TESIS Y PUBLICACIONES
        if (localStorage.getItem('searchAdvanced') == 'true') {
            this.data['title'] = 'Búsqueda avanzada';
            this.data['filtroActivo'] = true;
            this.array_Filtros = JSON.parse(localStorage.getItem('json_filtros'));
            localStorage.removeItem('searchAdvanced');
        }
        this.valorSearch = localStorage.getItem('search_form');
        localStorage.setItem('json_filtros', JSON.stringify(this.array_Filtros));
        // RECORRO ARRAY array_Filtros DE FILTROS
        for (let i = 0; i < this.array_Filtros.length; i++) {
            this.array_Filtros[i]['term'] = this.array_Filtros[i]['term'];
            let termm = this.array_Filtros[i]['term'];
            let term = termm.replace(".", "").replace(":", "").replace("-", " ");
            // Construyo cadena de filtros
            switch (this.array_Filtros[i]['search_by']) {
                case 'titulo':
                    if (this.array_Filtros[i]['contains'] == 'no-contiene') {
                        /* this.filtro = 'fq=-title:"'+ term +'"%26'; */
                        /* this.filtro = 'fq=-dc.title:'+ term +'*%26'; */
                        /* this.filtro = 'fq=-title:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=-dc.title:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'contiene') {
                        /* this.filtro = 'fq=title:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        this.filtro = 'fq=dc.title:' + term + '%26';
                        /* this.filtro = 'fq=title:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                    }
                    if (this.array_Filtros[i]['contains'] == 'es') {
                        this.filtro = 'fq=dc.title:"' + termm + '"%26';
                        /* this.filtro = 'fq=title:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=title:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        /* this.filtro = '"fq=title:'+this.array_Filtros[i]["term"] +'*"%26'; */
                    }
                    break;
                case 'autor':
                    if (this.array_Filtros[i]['contains'] == 'no-contiene') {
                        /* this.filtro = 'fq=-dc.contributor.author:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=-dc.contributor.author:'+ term +'*%26'; */
                        /* this.filtro = 'fq=-dc.contributor.author:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=-dc.contributor.author:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'contiene') {
                        /* this.filtro = 'fq=dc.contributor.author:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=dc.contributor.author:'+ term +'*%26'; */
                        /* this.filtro = 'fq=dc.contributor.author:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=dc.contributor.author:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'es') {
                        this.filtro = 'fq=bi_2_dis_value_filter:"' + termm + '"%26';
                        /* this.filtro = 'fq=bi_2_dis_value_filter:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=bi_2_dis_value_filter:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                    }
                    break;
                case 'fecha':
                    if (this.array_Filtros[i]['contains'] == 'no-contiene') {
                        /* this.filtro = 'fq=-dateIssued:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=-dateIssued:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=-dateIssued:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=-dateIssued:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'contiene') {
                        /* this.filtro = 'fq=dateIssued:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=dateIssued:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=dateIssued:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=dateIssued:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'es') {
                        this.filtro = 'fq=dateIssued:"' + termm + '"%26';
                        /* this.filtro = 'fq=dateIssued:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=dateIssued:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                    }
                    break;
                case 'tema':
                    if (this.array_Filtros[i]['contains'] == 'no-contiene') {
                        /* this.filtro = 'fq=-dc.subject.dewey:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=-dc.subject.dewey:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=-dc.subject.dewey:'+ term +'* OR' +' dc.subject.other:'+ term +'*%26';  */
                        /* this.filtro = 'fq=-dc.subject.dewey:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=-dc.subject.dewey:' + term + ' OR ' + 'dc.subject.other:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'contiene') {
                        /* this.filtro = 'fq=dc.subject.dewey:"'+ term +'"%26'; */
                        /* this.filtro = 'fq=dc.subject.dewey:'+ term +'*%26'; */
                        /* this.filtro = 'fq=dc.subject.dewey:'+ term +'* OR' +' dc.subject.other:'+ term +'*%26'; */
                        /* this.filtro = 'fq=dc.subject.dewey:\"'+ term +'\"%26'; */
                        this.filtro = 'fq=dc.subject.dewey:' + term + ' OR ' + 'dc.subject.other:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'es') {
                        this.filtro = 'fq=bi_4_dis_value_filter:"' + termm + '"%26';
                        /* this.filtro = 'fq=bi_4_dis_value_filter:'+ term +'*%26'; */
                        /* this.filtro = 'fq=bi_4_dis_value_filter:\"'+ term +'\"%26'; */
                    }
                    break;
                case 'tipo':
                    if (this.array_Filtros[i]['contains'] == 'no-contiene') {
                        /* this.filtro = 'fq=-dc.type:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=-dc.type:'+ term +'*%26'; */
                        /* this.filtro = 'fq=-dc.type:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=-dc.type:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'contiene') {
                        /* this.filtro = 'fq=dc.type:"'+this.array_Filtros[i]["term"] +'"%26'; */
                        /* this.filtro = 'fq=dc.type:'+ term +'*%26'; */
                        /* this.filtro = 'fq=dc.type:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                        this.filtro = 'fq=dc.type:' + term + '%26';
                    }
                    if (this.array_Filtros[i]['contains'] == 'es') {
                        this.filtro = 'fq=dc.type:"' + termm + '"%26';
                        /* this.filtro = 'fq=dc.type:'+this.array_Filtros[i]["term"] +'*%26'; */
                        /* this.filtro = 'fq=dc.type:\"'+this.array_Filtros[i]["term"] +'\"%26'; */
                    }
                    break;
            }
            str_filtros = str_filtros + this.filtro;
        }
        urlFiltro = this.urlPhp + 'filtro=' + str_filtros + '&valor=' + localStorage.getItem('search_form') + '&start=' + this.npage + '&orden=' + this.orden;
        console.log(urlFiltro);
        // ++++++++++++++++++ ENVÍO CONSULTA API Y DEVUELVE REGISTROS ++++++++++++++++++++++
        this.queriesService.queryGet(urlFiltro)
            .then((data) => {
            console.log(data);
            this.cargando = false;
            if (data['code'] == 404) {
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['search'] = data['Doc'];
                // MUESTRA/OCULTA SECCIÓN REGISTROS
                if (this.data['search'].length > 0) {
                    console.log(this.data['search']);
                }
                else {
                    this.msj = "No existen registros con ese criterio de búsqueda.";
                }
                // PAGINACIÓN
                this.totalPage = data['RecordsFound'];
                this.cantidadReg = 20;
                this.page = Math.ceil(this.totalPage / this.cantidadReg);
                console.log(this.totalPage + " registros");
                console.log(this.page + " páginas");
                console.log('pagina cada ' + page);
                console.log('página atual: ' + this.npage);
                console.log(this.nextPage);
                this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
                for (let i = 1; i <= this.page; i++) {
                    if (i <= 5) {
                        if (this.npage > 5) {
                            this.paginacion.push(i + (this.npage - 5));
                        }
                        else {
                            this.paginacion.push(i);
                        }
                    }
                }
                if (this.npage >= 2) {
                    this.prevPage = this.npage - 1;
                }
                else {
                    this.prevPage = 1;
                }
                if (this.npage < this.page) {
                    this.nextPage = this.npage + 1;
                }
                else {
                    this.nextPage = this.page;
                }
                // Fin paginación
            }
        }, err => {
            this.cargando = false;
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            console.log(err);
        });
    }
    busquedaAvanzada() {
        this.data['filtroActivo'] = true;
        this.data['title'] = 'Búsqueda avanzada';
        this.addFiltros();
    }
    addFiltros() {
        this.array_Filtros.push({
            search_by: '',
            contains: '',
            term: ''
        });
    }
    removefiltros(i) {
        this.array_Filtros.splice(i, 1); // elimina 1 indice a partir del indice i
        if (this.array_Filtros.length == 0) { // si no existen filtros
            this.data['filtroActivo'] = false; // oculta div de filtros
            this.data['title'] = 'Búsqueda simple';
        }
    }
    /* Desde menu facetas despliega todas las facetas */
    searchFacetas(param, num) {
        this.data['listFacetas'] = true;
        this.data['search'] = false;
        if (param == 'tipodoc') {
            this.data['facetas'] = this.tipodoc;
            this.data['tipo'] = 'tipo';
        }
        if (param == 'autores') {
            this.data['facetas'] = this.autores;
            this.data['tipo'] = 'autor';
        }
        if (param == 'tema') {
            this.data['facetas'] = this.tematicas;
            this.data['tipo'] = param;
        }
        // PAGINACIÓN
        this.totalPage = this.data['facetas'].length;
        this.cantidadReg = 50;
        this.npage = num;
        num = (num - 1) * 20;
        this.page = Math.ceil(this.totalPage / this.cantidadReg);
        this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
        this.facetas_desde = (this.npage - 1) * this.cantidadReg;
        this.facetas_hasta = (this.npage) * this.cantidadReg;
        for (let i = 1; i <= this.page; i++) {
            if (i <= 5) {
                if (this.npage > 5) {
                    this.paginacion.push(i + (this.npage - 5));
                }
                else {
                    this.paginacion.push(i);
                }
            }
        }
        if (this.npage >= 2) {
            this.prevPage = this.npage - 1;
        }
        else {
            this.prevPage = 1;
        }
        if (this.npage < this.page) {
            this.nextPage = this.npage + 1;
        }
        else {
            this.nextPage = this.page;
        }
        // Fin paginación
    }
    /* Desde lista de resutados de facetas despliega lista de resultados de documentos */
    getFacetas(faceta, item) {
        this.data['filtroActivo'] = true;
        this.data['title'] = 'Búsqueda avanzada';
        let splits = faceta.split('|||'); // string pasa a array si Separador es |||
        if (splits.length > 1) {
            faceta = splits.pop(); // elimina último elemento de un array
        }
        this.array_Filtros.push({
            search_by: item,
            contains: 'es',
            term: faceta
        });
        this.buscar(1, item, 'asc');
    }
    /* Solicitar contacto */
    submitContact(form, $event, title, handle) {
        $event.preventDefault();
        if (form.form.value)
            this.response.show = true;
        var datos = "<h3>Enviado desde publicación: <br><a href='" + this.urlHandle + handle + " '>" + title + "</a> <br><br>Handle:<br>" + handle + "</h3>";
        this.message = datos + "<h3>Mensaje: " + this.message + "</h3>";
        console.log(this.message);
        this.queriesService.queryGet(this.urlEmail + 'nombreApellido=' + this.name + '&mail=' + this.email + '&consulta=' + this.message).then((data) => {
            if (data['respuesta'] == true) {
                this.response.show = true;
                this.response.text = 'Mensaje enviado';
                this.response.icono = 'check_circle';
                this.response.color = 'color-verde';
                //form.form.reset();
                /* this.name = "";
                this.email = ""; */
                this.message = "";
            }
            else {
                //console.log(data);
                this.response.show = true;
                this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
                this.response.icono = 'highlight_off';
                this.response.color = 'texto-dg-rojo';
            }
        }, (error) => {
            console.log(error);
            this.response.show = true;
            this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
            this.response.icono = 'highlight_off';
            this.response.color = 'texto-dg-rojo';
        });
    }
};
tslib_1.__decorate([
    Output()
], BusquedaComponent.prototype, "propagar", void 0);
BusquedaComponent = tslib_1.__decorate([
    Component({
        selector: 'app-busqueda',
        templateUrl: './busqueda.component.html',
        styleUrls: ['./busqueda.component.css']
    })
], BusquedaComponent);
export { BusquedaComponent };
//# sourceMappingURL=busqueda.component copy.js.map