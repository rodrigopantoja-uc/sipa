import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { global } from '../../services/global';
import json from '../../../assets/json/mis-publicaciones/mis-publicaciones.json';
let MisPublicacionesComponent = class MisPublicacionesComponent {
    constructor(queriesService, http, router, activatedRoute, dataService) {
        this.queriesService = queriesService;
        this.http = http;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.dataService = dataService;
        this.json = json;
        this.array_guardados = [];
        this.array_en_espera = [];
        this.array_confirmados = [];
        this.estados = "confirmado";
        this.data = [];
        this.perfil = [];
        this.loggedIn = false;
        this.modal_ficha = false;
        this.metadatos_simple = [];
        this.error = false;
        this.tipodata = "sencillo";
        this.npage = 1;
        this.paginacion = [];
        this.urlImg = global.php_img + "/bitstream/handle";
        this.urlPhp = global.php + "/discovery-sipa.php?";
        this.urlGuardar = global.php + "/vinculacion/mis_publicaciones.php";
        this.urlDesvincular = global.php + '/vinculacion/VincularSipaWos.php';
        this.urlFicha = global.php + "/ficha.php?";
    }
    ngOnInit() {
        // Recibo datos por get
        this.activatedRoute.params.subscribe(params => {
            this.estado(params['pag']);
        });
        /* Datos del usuario */
        //document.getElementById('top').scrollIntoView({behavior: 'smooth'}); // Scroll hacia buscador
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        this.confirmados(1, '', '');
        this.mis_guardados(false);
    }
    ngDoCheck() {
        // COMPUEBO LOGIN
        if (localStorage.getItem('correo')) {
            this.loggedIn = true;
            //this.usuario = localStorage.getItem('usuario');
            let split = localStorage.getItem('usuario').split(',');
            this.usuario = split[1];
            this.correo = localStorage.getItem('correo');
            this.unidad = localStorage.getItem('unidad');
            if (localStorage.getItem('jsonAcademico') == "") {
                this.perfil = false;
            }
            else {
                this.perfil = JSON.parse(localStorage.getItem('jsonAcademico'));
            }
        }
        else {
            this.loggedIn = false;
        }
    }
    estado(param) {
        this.estados = param;
        if (this.estados == "sinenviar") {
            this.mis_guardados(true);
        }
        if (this.estados == "enespera") {
            this.enEspera();
        }
        if (this.estados == "confirmado") {
            this.confirmados(1, '', '');
        }
    }
    confirmados(page, filtro, orden) {
        this.cargando = true;
        this.msj = null;
        var acciones;
        this.npage = page;
        localStorage.setItem('page', page);
        page = (page - 1) * 20;
        this.orden = orden;
        const formData = new FormData();
        /* Armo acciones */
        acciones = { accion: "buscar", id: "", email: localStorage.getItem('correo'), validaBiblio: "Validado", start: this.npage, orden: this.orden, handle: "" };
        console.log(acciones);
        /* Entrgo array */
        formData.append('array_acciones', JSON.stringify(acciones));
        /* +++++ CARGAR EN BBDD ++++ */
        this.http.post(this.urlGuardar, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                if (response) {
                    this.array_confirmados = response['Doc'];
                    console.log(response);
                    // PAGINACIÓN
                    this.totalPage = response['RecordsFound'];
                    this.cantidadReg = 20;
                    this.page = Math.ceil(this.totalPage / this.cantidadReg);
                    console.log(this.totalPage + " registros");
                    console.log(this.page + " páginas");
                    console.log('pagina cada ' + page);
                    console.log('página atual: ' + this.npage);
                    console.log(this.nextPage);
                    this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
                    for (let i = 1; i <= this.page; i++) {
                        if (i <= 5) {
                            if (this.npage > 5) {
                                this.paginacion.push(i + (this.npage - 5));
                            }
                            else {
                                this.paginacion.push(i);
                            }
                        }
                    }
                    if (this.npage >= 2) {
                        this.prevPage = this.npage - 1;
                    }
                    else {
                        this.prevPage = 1;
                    }
                    if (this.npage < this.page) {
                        this.nextPage = this.npage + 1;
                    }
                    else {
                        this.nextPage = this.page;
                    }
                    // Fin paginación
                }
                else {
                    this.array_confirmados = [];
                    this.msj = "No existen documentos en espera";
                }
            }
        }, err => {
            this.cargando = false;
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            console.log(err);
        });
    }
    desvincular(id_post, handle) {
        this.cargando = true;
        this.msj = null;
        var acciones;
        const formData = new FormData();
        /* Armo acciones */
        acciones = { idpost: id_post, fuente: "DESVINCULAR", emailuser: localStorage.getItem('correo'), autor: localStorage.getItem('usuario'), rol: "" };
        console.log(acciones);
        /* Entrgo array */
        formData.append('array_vincular', JSON.stringify(acciones));
        /* +++++ CARGAR EN BBDD ++++ */
        this.http.post(this.urlDesvincular, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                console.log(response['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else if (response['httpCode'] == 201) {
                this.confirmados(1, '', '');
                console.log(response);
            }
            else {
                //this.array_en_espera = [];
                this.msj = "algo";
                console.log(response);
            }
        }, err => {
            this.cargando = false;
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            console.log(err);
        });
    }
    // GUARDADOS SIN ENVIAR
    mis_guardados(param) {
        if (param) {
            this.cargando = true;
            this.msj = null;
        }
        var acciones;
        const formData = new FormData();
        /* Armo acciones */
        acciones = { accion: "consultar", id: "", email: localStorage.getItem('correo'), validaBiblio: "", start: "", handle: "" };
        console.log(acciones);
        /* Entrgo array */
        formData.append('array_acciones', JSON.stringify(acciones));
        /* +++++ CARGAR EN BBDD ++++ */
        this.http.post(this.urlGuardar, formData)
            .subscribe(response => {
            this.cargando = false;
            this.array_guardados = response;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                if (response && this.array_guardados.length != 0) {
                    console.log(response);
                    for (let i = 0; this.array_guardados.length > i; i++) {
                        this.array_guardados[i].json_temporal = JSON.parse(this.array_guardados[i].json_temporal);
                    }
                }
                else {
                    this.array_guardados = [];
                    this.msj = "No existen documentos guardados";
                }
            }
        }, err => {
            this.cargando = false;
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            console.log(err);
        });
    }
    alertDelete(i) {
        this.alertaElimina = i;
    }
    /* ELIMINO GUARDADO */
    deletePost(idpost, i) {
        this.cargando = true;
        this.msj = null;
        this.alertaElimina = null;
        const formData = new FormData();
        /* Armo acciones */
        var acciones;
        acciones = { accion: "eliminar", id: idpost, email: localStorage.getItem('correo'), validaBiblio: "", start: "", handle: "" };
        console.log(acciones);
        /* Entrgo array */
        formData.append('array_acciones', JSON.stringify(acciones));
        /* +++++ ENVIO ++++ */
        this.http.post(this.urlGuardar, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else if (response['httpCode'] == "201" || response['httpCode'] == "200") {
                this.array_guardados.splice(i, 1);
            }
            else {
                this.msj = "Tuvimos problema, no pudimos eliminar el registro";
                console.log("conecto pero no trajo httpCode = 201 o 200");
                console.log(response);
            }
        }, err => {
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            this.cargando = false;
            console.log(err);
        });
    }
    /* SEGUIR EDITANDO */
    editPost(obj, id_post) {
        let json = JSON.stringify(obj);
        localStorage.setItem('editar', json);
        localStorage.setItem('idguardar', id_post);
        this.router.navigate(['/upload/editar']);
    }
    enEspera() {
        this.cargando = true;
        this.msj = null;
        var acciones;
        const formData = new FormData();
        /* Armo acciones */
        acciones = { accion: "buscar", id: "", email: localStorage.getItem('correo'), validaBiblio: "Sin Validar", start: "", handle: "" };
        console.log(acciones);
        /* Entrgo array */
        formData.append('array_acciones', JSON.stringify(acciones));
        /* +++++ CARGAR EN BBDD ++++ */
        this.http.post(this.urlGuardar, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                if (response) {
                    this.array_en_espera = response['Doc'];
                    console.log(this.array_en_espera);
                    /* for( let i=0; this.array_en_espera.length>i; i++){
                      this.array_en_espera[i].json_temporal = JSON.parse(this.array_en_espera[i].json_temporal)
                    } */
                }
                else {
                    this.array_en_espera = [];
                    this.msj = "No existen documentos en espera";
                }
            }
        }, err => {
            this.cargando = false;
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            console.log(err);
        });
    }
    abreModal_ficha(i, handle) {
        this.modal_ficha = true;
        this.index_ficha = i;
        this.tituloModal = "";
        console.log(this.urlFicha + 'handle=' + handle);
        this.queriesService.queryGet(this.urlFicha + 'handle=' + handle).then((data) => {
            console.log("desde ficha");
            console.log(data);
            if (data['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                if (data['response']['docs'].length > 0) {
                    this.error = false;
                    this.data['publics'] = data['response']['docs'][0];
                    //data['miniatura'];
                    if (data['miniatura']) {
                        this.foto = this.urlImg + "/" + data['response']['docs'][0]['handle'] + "/" + data['miniatura'];
                    }
                    else {
                        /* this.foto = "assets/img/logo_uc_linea.png"; */
                        this.foto = null;
                    }
                    // Archivo en embargo
                    if (data['embargo'] == 200 || data['embargo'] == 201) {
                        this.embargo = false;
                    }
                    else {
                        this.embargo = true;
                    }
                    // Construye tabla metadatos
                    let list = this.data['publics'];
                    for (let i in list) {
                        if (i.substr(0, 3) == 'dc.' && i.indexOf('_') == -1) {
                            /* cambiar nombres */
                            var alias = i;
                            var link = false;
                            var mostrar = true;
                            if (i == "dc.concurso") {
                                alias = "Concurso Arte y Cultura";
                                link = false;
                            }
                            if (i == "dc.contributor.advisor") {
                                alias = "Profesor guía";
                                link = false;
                            }
                            if (i == "dc.contributor.author") {
                                alias = "Autor";
                                link = false;
                            }
                            if (i == "dc.contributor.other") {
                                alias = "Otro autor";
                                link = false;
                            }
                            if (i == "dc.contributor.editor") {
                                alias = "Editor";
                                link = false;
                            }
                            if (i == "dc.contributor.illustrator") {
                                alias = "Ilustrador";
                                link = false;
                            }
                            if (i == "dc.date" || i == "dc.date.accessioned" || i == "dc.date.available") { /* eliminar ??? */
                                alias = "nulo";
                                link = false;
                                mostrar = false;
                            }
                            if (i == "dc.date.created") {
                                alias = "Fecha de creación";
                                link = false;
                            }
                            if (i == "dc.date.issued") {
                                alias = "Fecha de publicación";
                                link = false;
                            }
                            if (i == "dc.date.updated") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.description") {
                                alias = "Nota";
                                link = false;
                            }
                            if (i == "dc.description.abstract") {
                                alias = "Resumen";
                                link = false;
                            }
                            if (i == "dc.description.tableofcontents") {
                                alias = "Tabla de contenido";
                                link = false;
                            }
                            if (i == "dc.description.uri") {
                                alias = "Enlace";
                                link = false;
                            }
                            if (i == "dc.description.version") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.estamento.1" || i == "dc.estamento.2" || i == "dc.estamento.3" || i == "dc.estamento.4") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.format.extent") {
                                alias = "Paginación";
                                link = false;
                            }
                            if (i == "dc.fuente.origen") { /* eliminar?? */
                                alias = "nulo";
                                link = false;
                                mostrar = false;
                            }
                            if (i == "dc.identifier.codUA" || i == "dc.identifier.codUA1" || i == "dc.identifier.codUA2" || i == "dc.identifier.codUA3" || i == "dc.identifier.codUA4" || i == "dc.identifier.codUA5" || i == "dc.identifier.codUA6" || i == "dc.identifier.codUA7" || i == "dc.identifier.codUA8" || i == "dc.identifier.codUA9" || i == "dc.identifier.codUA10") {
                                alias = "Unidad académica";
                                link = false;
                            }
                            if (i == "dc.identifier.citation") {
                                alias = "Cómo citar este documento";
                                link = false;
                            }
                            if (i == "dc.identifier.converisid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.identifier.dialnetid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.identifier.doi") {
                                alias = "DOI";
                                link = true;
                            }
                            if (i == "dc.identifier.eisbn") {
                                alias = "ISBN electrónico";
                                link = false;
                            }
                            if (i == "dc.identifier.eissn") {
                                alias = "ISSN electrónico";
                                link = false;
                            }
                            if (i == "dc.identifier.isbn") {
                                alias = "ISBN";
                                link = false;
                            }
                            if (i == "dc.identifier.issn") {
                                alias = "ISSN";
                                link = false;
                            }
                            if (i == "dc.identifier.pubmedid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.identifier.scopusid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.issue.numero") {
                                alias = "Número de publicación";
                                link = false;
                            }
                            if (i == "dc.identifier.orcid" || i == "dc.identifier.orcid1" || i == "dc.identifier.orcid2" || i == "dc.identifier.orcid3" || i == "dc.identifier.orcid4" || i == "dc.identifier.orcid5" || i == "dc.identifier.orcid6" || i == "dc.identifier.orcid7" || i == "dc.identifier.orcid8" || i == "dc.identifier.orcid9" || i == "dc.identifier.orcid10") {
                                alias = "Número ORCID";
                                link = false;
                            }
                            if (i == "dc.identifier.uri") {
                                alias = "Enlace";
                                link = true;
                            }
                            if (i == "dc.identifier.wosid") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.informacion.autoruc") {
                                alias = "Información del autor UC";
                                link = false;
                            }
                            if (i == "dc.language.iso") {
                                alias = "nulo"; /* Idioma */
                                link = false;
                            }
                            if (i == "dc.pagina.final") {
                                alias = "Página final";
                                link = false;
                            }
                            if (i == "dc.pagina.inicio") {
                                alias = "Página inicio";
                                link = false;
                            }
                            if (i == "dc.publisher") {
                                alias = "Editorial";
                                link = false;
                            }
                            if (i == "dc.relation") {
                                alias = "Publicación relacionada";
                                link = false;
                            }
                            if (i == "dc.relation.isformatof") {
                                alias = "Se encuentra en";
                                link = false;
                            }
                            if (i == "dc.relation.ispartofseries") {
                                alias = "Serie";
                                link = false;
                            }
                            if (i == "dc.relation.ispartof") {
                                alias = "Publicado en";
                                link = false;
                            }
                            if (i == "dc.revista") {
                                alias = "Revista";
                                link = false;
                            }
                            if (i == "dc.rights") {
                                alias = "Derechos";
                                link = false;
                            }
                            if (i == "dc.rights.holder") {
                                alias = "Titular de los derechos";
                                link = false;
                            }
                            if (i == "dc.relation.uri") {
                                alias = "Enlace relacionada";
                                link = true;
                            }
                            if (i == "dc.subject.ddc") {
                                alias = "nulo";
                                link = false;
                            }
                            if (i == "dc.subject.dewey") {
                                alias = "Temática";
                                link = false;
                            }
                            if (i == "dc.subject.other") {
                                alias = "Materia";
                                link = false;
                            }
                            if (i == "dc.subject") {
                                alias = "Palabra clave";
                                link = false;
                            }
                            if (i == "dc.title.alternative") {
                                alias = "Otro título";
                                link = false;
                            }
                            if (i == "dc.title") {
                                alias = "Título";
                                link = false;
                            }
                            if (i == "dc.type") {
                                alias = "Tipo de documento";
                                link = false;
                            }
                            if (i == "dc.volumen") {
                                alias = "Volumen";
                                link = false;
                            }
                            if (i == "dc.zcode" || i == "dc.zcode.1" || i == "dc.zcode.2" || i == "dc.zcode.3" || i == "dc.zcode.4" || i == "dc.zcode.5" || i == "dc.zcode.6" || i == "dc.zcode.7" || i == "dc.zcode.8" || i == "dc.zcode.9" || i == "dc.zcode.10") {
                                alias = "nulo";
                                link = false;
                                mostrar = false;
                            }
                            /* if(mostrar){
                              this.metadatos_simple.push( { "metadato": alias, "cod":i, "valor": list[i], "link":link });
                            } */
                            this.metadatos_simple.push({ "metadato": alias, "cod": i, "valor": list[i], "link": link, "mostrar": mostrar });
                        }
                    }
                    this.metadatos_simple.sort((a, b) => (a.metadato > b.metadato) ? 1 : -1);
                    console.log(this.metadatos_simple);
                }
                else {
                    this.error = true;
                }
            }
        });
    }
    cerrarModal_ficha() {
        this.metadatos_simple = [];
        this.modal_ficha = false;
    }
    metadatas(reg) {
        this.tipodata = reg;
    }
};
MisPublicacionesComponent = tslib_1.__decorate([
    Component({
        selector: 'app-mis-publicaciones',
        templateUrl: './mis-publicaciones.component.html',
        styleUrls: ['./mis-publicaciones.component.css']
    })
], MisPublicacionesComponent);
export { MisPublicacionesComponent };
//# sourceMappingURL=mis-publicaciones.component.js.map