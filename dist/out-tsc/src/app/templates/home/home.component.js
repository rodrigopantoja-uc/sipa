import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from '../../../assets/json/home.json';
import AreasTematicas from '../../../assets/json/areas-tematicas.json';
import CajonBusqueda from '../../../assets/json/cajon-busqueda.json';
import { global } from '../../services/global';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/mergeMap';
let HomeComponent = class HomeComponent {
    constructor(subscribeService, router, queriesService, _sanitizer, formBuilder, dataService) {
        this.subscribeService = subscribeService;
        this.router = router;
        this.queriesService = queriesService;
        this._sanitizer = _sanitizer;
        this.formBuilder = formBuilder;
        this.dataService = dataService;
        this.json = json;
        this.posts = AreasTematicas['areas-tematicas'];
        this.CajonBusqueda = CajonBusqueda;
        this.data = [];
        this.loggedIn = false;
        this.tabla_datos_academicos = [];
        this.editing = false;
        this.userData = [];
        this.loginFailed = "";
        this.categorias = [];
        this.buscalibro = { name: '' };
        this.subscribeData = {};
        this.modal = false;
        this.condicion_publicar = false;
        this.urlPhp = global.php;
    }
    ngOnInit() {
        //document.getElementById('top').scrollIntoView({behavior: 'smooth'}); // Scroll hacia buscador
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        // TEXTOS PÁGINA
        this.json = json;
        // NUESTROS NUMEROS
        /* this.queriesService.queryGet('https://sipa-des.cloud.uc.cl/assets/php/cantidad-otros.php').then((data) => { */
        /* this.queriesService.queryGet(this.urlPhp +'/cantidad-tesis.php').then((data) => { */
        this.queriesService.queryGet(this.urlPhp + '/cantidad-otros.php').then((data) => {
            this.data['statistics-tesis'] = data['response']['numFound'];
        });
        this.queriesService.queryGet(this.urlPhp + '/cantidad-articulos.php').then((data) => {
            this.data['statistics-articulos'] = data['response']['numFound'];
        });
        this.queriesService.queryGet(this.urlPhp + '/cantidad-libros.php').then((data) => {
            this.data['statistics-libros'] = data['response']['numFound'];
        });
    }
    ngDoCheck() {
        // COMPUEBO LOGIN
        if (localStorage.getItem('correo')) {
            this.loggedIn = true;
            this.usuario = localStorage.getItem('usuario');
            this.correo = localStorage.getItem('correo');
            this.unidad = localStorage.getItem('unidad');
            if (localStorage.getItem('jsonAcademico') == "") {
                this.datos_academicos = false;
            }
            else {
                this.datos_academicos = JSON.parse(localStorage.getItem('jsonAcademico'));
            }
            this.router.navigate(['/mispublicaciones/confirmado']); // /upload/subir-publicacion
        }
        else {
            this.loggedIn = false;
        }
    }
    scrollToElement($element) {
        console.log($element);
        document.getElementById('suscripcion').scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    }
    abreModal() {
        this.modal = true;
        this.tituloModal = "Mensaje";
    }
    cerrarModal() {
        this.modal = false;
    }
    loginCas() {
        /* window.location.href='assets/php/cas-log/cas-login.php'; */
        localStorage.setItem('usuario', 'PANTOJA FLORES, RODRIGO ALEJANDRO');
        localStorage.setItem('correo', 'rodrigo.pantoja@uc.cl');
        localStorage.setItem('unidad', 'STIDD');
        localStorage.setItem('jsonAcademico', '{"NOMBRE_COMPLETO": "PANTOJA FLORES, RODRIGO ALEJANDRO","CATEGORIA_ACADEMICA": "PROFESOR ASISTENTE","COD_PERS": "codPrueba1234","CORREO": "rodrigo.pantoja@uc.cl","DV": "K","FACULTAD": "FACULTAD DE MEDICINA","RUT":"11.111.111-1","GRADO_PERFECCIONAMIENTO":"ESPECIALIDAD MÉDICA/ ODONTOLÓGICA","PAIS":"Chile","ORCID":"123456789","INSTITUCION":"PUC","UNIDAD_ACADEMICA":"DISEÑO"}');
    }
    logout() {
        localStorage.removeItem('usuario');
        localStorage.removeItem('correo');
        localStorage.removeItem('unidad');
        localStorage.removeItem('jsonAcademico');
        window.location.href = 'assets/php/cas-log/logout-cas.php';
    }
    getStatistics(num) {
        localStorage.setItem('search_form', '');
        var array_Filtros = [
            {
                search_by: 'tipo',
                contains: 'contiene',
                term: num
            }
        ];
        localStorage.setItem('json_filtros', JSON.stringify(array_Filtros));
        localStorage.setItem('searchAdvanced', 'true');
        this.router.navigate(['/busqueda']);
    }
    cheq(event) {
        if (event.target.checked) {
            /* this.array_form.indexacion.push(
              event.target.value
            ); */
            this.condicion_publicar = true;
        }
        else {
            /* for(var i = 0; this.array_form.indexacion.length>i; i++){
              if(this.array_form.indexacion[i]==event.target.value){
                this.array_form.indexacion.splice(i, 1);
              }
            } */
            this.condicion_publicar = false;
        }
    }
};
HomeComponent = tslib_1.__decorate([
    Component({
        selector: 'app-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.css']
    })
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map