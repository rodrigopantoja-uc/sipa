import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import json from '../../../assets/json/upload-record/upload-record.json';
import { global } from '../../services/global';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
let UploadRecordComponent = class UploadRecordComponent {
    constructor(uploadService, _queriesService, router, activatedRoute, http, dataService) {
        this.uploadService = uploadService;
        this._queriesService = _queriesService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.dataService = dataService;
        this.myFiles = [];
        this.data = [];
        this.json = json;
        this.activePane = 't1';
        this.vincula = true;
        this.tipo_pub = "investigacion";
        this.tipo_doc = "Artículo";
        this.array_vincula = [];
        this.array_comunes = {
            publicacion: "Artículo",
            clave: null,
            user: localStorage.getItem("correo"),
            titulo: null,
            volumen: null,
            anio_publicacion: null,
            financiamiento: "Seleccionar",
            otro_financiamiento: [],
            afiliacion_uc: "No",
            url_publicacion: [],
            autores: [],
            archivos: [],
            concedo_licencia: null
        };
        this.array_form = {
            revistas: [],
            numero: null,
            pagina_inicio: null,
            pagina_termino: null,
            doi: null,
            indexacion: [],
            otra_indexacion: []
        };
        this.array_libro = {
            edicion: null,
            nro_paginas: null,
            pagina_inicio: null,
            pagina_termino: null,
            editores: [{
                    editor: null
                }],
            lugar_edicion: [],
            editorial: [],
            isbn: null,
        };
        this.array_capitulo_libro = {
            titulo_capitulo: null
        };
        this.array_obras = {
            publicacion: null,
            clave: this._queriesService.getPass(),
            user: localStorage.getItem("correo"),
            titulo: null,
            area: [],
            tipo_obra: [],
            fecha_inscripcion: null,
            institucion_auspiciadora: [{
                    nombre: null
                }],
            institucion_patrocinadora: [{
                    nombre: null
                }],
            ubicacion_obra: null,
            pais_obra: [],
            afiliacion_uc: "No",
            concedo_licencia: null,
            autores: [],
            archivos: []
        };
        this.autores = [{
                email: null,
                rol: null,
                nombres: null,
                apellidos: null,
                nombre_completo: null,
                ORCID: null,
                pais: [{
                        nombre: null,
                        nuevo: null
                    }],
                institucion: [{
                        nombre: null,
                        nuevo: null
                    }]
            }];
        this.archivos = [];
        this.array_enviar = {
            publicacion: "Artículo",
            clave: null,
            user: localStorage.getItem("correo"),
            titulo: null,
            volumen: null,
            anio_publicacion: null,
            financiamiento: "Seleccionar",
            otro_financiamiento: null,
            afiliacion_uc: "No",
            url_publicacion: null,
            autores: null,
            archivos: null,
            concedo_licencia: null,
            revistas: null,
            numero: null,
            pagina_inicio: null,
            pagina_termino: null,
            doi: null,
            indexacion: null,
            otra_indexacion: null,
            edicion: null,
            nro_paginas: null,
            editores: null,
            lugar_edicion: null,
            editorial: null,
            isbn: null,
            titulo_capitulo: null,
            area: null,
            tipo_obra: null,
            fecha_inscripcion: null,
            institucion_auspiciadora: null,
            institucion_patrocinadora: null,
            ubicacion_obra: null,
            pais_obra: []
        };
        this.array_seleccionados = [];
        this.array_agregados = [];
        this.otras_autorias = "No";
        this.general_colapsable = true;
        this.autoria_colapsable = false;
        this.archivos_colapsable = false;
        this.formFile = false;
        this.urlRegistrar = global.php + "/formulario-sipa/formulario.php";
        this.urlGuardar = global.php + "/vinculacion/mis_publicaciones.php";
        this.urlvincular = global.php + "/vinculacion/VincularSipaWos.php";
        this.urlSql = global.php + "/formulario-sipa/sql.php";
        this.urlPhp = global.php + "/discovery-sipa.php?";
        this.modal = false;
        this.newIndexacion = false;
        this.busca_autor = null;
        this.pasos = false;
        this.filtro_valor = "";
        this.tabla_resumen = [];
        this.numenvios = 0;
        this.anio = new Date().getFullYear();
        this.revista = new FormControl('');
        this.lugar_edicion = new FormControl('');
        this.editorial = new FormControl('');
        this.otro_financiamiento = new FormControl('');
        this.area = new FormControl('');
        this.tipo_obra = new FormControl('');
        this.otra_indexacion = new FormControl('');
        this.pais = new FormControl('');
        this.institucion = new FormControl('');
        this.identity = this._queriesService.getIdentity();
        this.token = this._queriesService.getToken();
        this.password = this._queriesService.getPass();
    }
    ngOnInit() {
        // refresca página
        //this._queriesService.getRefresh();
        this.date();
        this.usuario = localStorage.getItem('usuario');
        let split = localStorage.getItem('usuario').split(',');
        this.name_usuario = split[1];
        //document.getElementById('top').scrollIntoView({behavior: 'smooth'}); // Scroll hacia buscador
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        var altura = 300;
        $(window).on('scroll', function () {
            /* var altura = $('.mainBolsa').offset().top; */
            if ($(window).scrollTop() > altura) {
                $('.mainBolsa').addClass('mainBolsa-fixed');
                $('.headerBolsa').addClass('headerBolsa-fixed');
                $('.footerBolsa').addClass('footerBolsa-fixed');
            }
            else {
                $('.mainBolsa').removeClass('mainBolsa-fixed');
                $('.headerBolsa').removeClass('headerBolsa-fixed');
                $('.footerBolsa').removeClass('footerBolsa-fixed');
            }
        });
        // Recibo datos por get
        this.activatedRoute.params.subscribe(params => {
            if (params['pag'] == "editar") {
                this.pasos = true;
                this.vincula = false;
                let obj = JSON.parse(localStorage.getItem("editar"));
                this.array_enviar = Object.assign(obj);
                this.tipo_doc = this.array_enviar.publicacion;
            }
            else {
                /* Autores - Datos del usuario */
                if (localStorage.getItem('jsonAcademico')) {
                    let perfil = JSON.parse(localStorage.getItem('jsonAcademico'));
                    this.array_enviar.autores = [];
                    this.array_enviar.autores.push({
                        email: localStorage.getItem('correo'),
                        rol: null,
                        nombres: null,
                        apellidos: null,
                        nombre_completo: perfil.NOMBRE_COMPLETO,
                        ORCID: perfil.ORCID,
                        pais: [{
                                nuevo: "no",
                                nombre: perfil.PAIS
                            }],
                        institucion: [{
                                nuevo: "no",
                                nombre: perfil.INSTITUCION
                            }]
                    });
                }
            }
            console.log(this.array_enviar.autores);
        });
        this.mis_guardados();
    }
    ngDoCheck() {
        this.password = this._queriesService.getPass();
    }
    /* +++++++++++++++  */
    date() {
        var pruebaFecha = document.createElement("input");
        //Si tiene soporte: debe aceptar el tipo "date"...
        pruebaFecha.setAttribute("type", "date");
        if (pruebaFecha.type === "date") {
            this.soportadateInput = true;
        }
        else {
            this.soportadateInput = false;
        }
    }
    loguearse() {
        this.login = true;
        this.errLog = false;
    }
    closeLogin() {
        this.login = false;
    }
    toScroll() {
        document.getElementById('pasos').scrollIntoView({ behavior: 'smooth' });
    }
    tipo_publicacion(event) {
        if (event.target.checked) {
            this.tipo_pub = event.target.value;
            if (event.target.value == "Obra creativa") {
                this.array_enviar.publicacion = event.target.value;
                this.tipo_doc = event.target.value;
            }
            if (event.target.value == "investigacion") {
                this.tipo_doc = "Artículo";
            }
        }
    }
    tipodoc(tipo) {
        if (tipo == "Obra creativa") {
            this.array_enviar.publicacion = tipo;
        }
        if (tipo == "Artículo") {
            this.array_enviar.publicacion = tipo;
            this.array_enviar.titulo_capitulo = null;
        }
        if (tipo == "Libro") {
            this.array_enviar.publicacion = tipo;
        }
        if (tipo == "Capítulo de libro") {
            this.array_enviar.publicacion = tipo;
        }
        this.tipo_doc = tipo;
    }
    collapse(item) {
        $('#' + item).slideToggle(300);
        this.actualizarPost();
    }
    vincular(valor) {
        if (valor == "true") {
            this.vincula = true;
            this.bolsaVincula = true;
        }
        else {
            this.vincula = false;
        }
        //this.toScroll();
    }
    /* +++++++++++++++  */
    searchRevista() {
        this.data['revistas'] = null;
        /* Busca al escribir */
        this.revista.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "r";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['revistas'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcono() {
        /* Busca al click */
        var tabla = "r";
        var search = this.revista.value;
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + search)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['revistas'] = data['resultado'];
            }
        });
    }
    addRevista(name, nuevo, donde) {
        this.array_enviar.revistas = [];
        this.array_enviar.revistas.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['revistas'] = null;
        this.revista.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchLugarEdicion() {
        /* Revistas */
        this.data['lugar_edicion'] = null;
        this.lugar_edicion.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "l";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['lugar_edicion'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcoLugarEdicion() {
        var tabla = "l";
        var search = this.lugar_edicion.value;
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + search)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['lugar_edicion'] = data['resultado'];
            }
        });
    }
    addLugarEdicion(name, nuevo, donde) {
        if (!this.array_enviar.lugar_edicion) { /* solo cuando son multi registros */
            this.array_enviar.lugar_edicion = [];
        }
        this.array_enviar.lugar_edicion.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['lugar_edicion'] = null;
        this.lugar_edicion.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            console.log(JSON.stringify(array_newCampo));
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchEditorial() {
        /* Revistas */
        this.data['editorial'] = null;
        this.editorial.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "e";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['editorial'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcoEditorial() {
        var tabla = "e";
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.editorial.value)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['editorial'] = data['resultado'];
            }
        });
    }
    addEditorial(name, nuevo, donde) {
        if (!this.array_enviar.editorial) { /* solo cuando son multi registros */
            this.array_enviar.editorial = [];
        }
        this.array_enviar.editorial.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['editorial'] = null;
        this.editorial.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchOtroFinanciamiento() {
        this.data['otro_financiamiento'] = null;
        this.otro_financiamiento.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "of";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['otro_financiamiento'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcoOtroFinanciamiento() {
        var tabla = "of";
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.otro_financiamiento.value)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['otro_financiamiento'] = data['resultado'];
            }
        });
    }
    addOtroFinanciamiento(name, nuevo, donde) {
        if (!this.array_enviar.otro_financiamiento) { /* solo cuando son multi registros */
            this.array_enviar.otro_financiamiento = [];
        }
        this.array_enviar.otro_financiamiento.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['otro_financiamiento'] = null;
        this.otro_financiamiento.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchLugarArea() {
        /* Revistas */
        this.data['area'] = null;
        this.area.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "o";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['area'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcoLugarArea() {
        var tabla = "o";
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.area.value)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['area'] = data['resultado'];
            }
        });
    }
    addLugarArea(name, nuevo, donde) {
        this.array_enviar.area = [];
        this.array_enviar.area.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['area'] = null;
        this.area.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchTipo_obra() {
        /* Revistas */
        this.data['tipo_obra'] = null;
        this.tipo_obra.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "t";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['tipo_obra'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcoTipo_obra() {
        var tabla = "t";
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.tipo_obra.value)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['tipo_obra'] = data['resultado'];
            }
        });
    }
    addTipo_obra(name, nuevo, donde) {
        this.array_enviar.tipo_obra = [];
        this.array_enviar.tipo_obra.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['tipo_obra'] = null;
        this.tipo_obra.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchOtra_indexacion() {
        /* Revistas */
        this.data['otra_indexacion'] = null;
        this.otra_indexacion.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "ox";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['otra_indexacion'] = data['resultado'];
                    }
                });
            }
        });
    }
    searchIcoOtra_indexacion() {
        var tabla = "ox";
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.otra_indexacion.value)
            .then((data) => {
            if (data['code'] == 404) { /* Verifica sesion */
                console.log(data['msg']);
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                this.data['otra_indexacion'] = data['resultado'];
            }
        });
    }
    addOtra_indexacion(name, nuevo, donde) {
        if (!this.array_enviar.otra_indexacion) { /* solo cuando son multi registros */
            this.array_enviar.otra_indexacion = [];
        }
        this.array_enviar.otra_indexacion.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['otra_indexacion'] = null;
        this.otra_indexacion.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
    }
    searchPais(i) {
        this.data['pais'] = null;
        this.selectAutor = i;
        this.pais.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "p";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['pais'] = data['resultado'];
                    }
                });
            }
        });
    }
    addPais(name, i, nuevo, donde) {
        console.log(name);
        console.log(i);
        console.log(nuevo);
        console.log(donde);
        if (!this.array_enviar.autores[i].pais) { /* solo cuando son multi registros */
            this.array_enviar.autores[i].pais = [];
        }
        this.array_enviar.autores[i].pais.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['pais'] = null;
        this.pais.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            console.log(JSON.stringify(array_newCampo));
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
        console.log(this.array_enviar.autores);
    }
    searchInstitucion(i) {
        this.data['institucion'] = null;
        this.selectAutor = i;
        this.institucion.valueChanges
            .pipe(debounceTime(300))
            .subscribe(value => {
            this.filtro_valor = value;
            if (this.filtro_valor) {
                var tabla = "i";
                this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.filtro_valor)
                    .then((data) => {
                    if (data['code'] == 404) { /* Verifica sesion */
                        console.log(data['msg']);
                        window.location.href = global.php + '/cas-log/cas-login.php';
                    }
                    else {
                        this.data['institucion'] = data['resultado'];
                    }
                });
            }
        });
    }
    addInstitucion(name, i, nuevo, donde) {
        if (!this.array_enviar.autores[i].institucion) { /* solo cuando son multi registros */
            this.array_enviar.autores[i].institucion = [];
        }
        this.array_enviar.autores[i].institucion.push({
            nombre: name,
            nuevo: nuevo
        });
        event.preventDefault();
        this.data['institucion'] = null;
        this.institucion.setValue('');
        /* Insert Nuevo Campo */
        if (nuevo == 'si') {
            let array_newCampo = {
                tipo: donde,
                campo: name
            };
            console.log(JSON.stringify(array_newCampo));
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(array_newCampo));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == "200") {
                    console.log("insertó nuevo campo");
                }
                else {
                    console.log("se conectó pero no trajo resultado 200");
                    console.log(response);
                }
            });
        }
        console.log(this.array_enviar.autores);
    }
    quitaAplicaSeleccion(titulomodal, i, i_pais) {
        if (titulomodal == "Revista") {
            this.array_enviar.revistas.splice(i, 1);
            this.array_enviar.revistas = null;
        }
        if (titulomodal == "Otro financiamiento") {
            this.array_enviar.otro_financiamiento.splice(i, 1);
        }
        if (titulomodal == "Indexación") {
            this.array_enviar.otra_indexacion.splice(i, 1);
        }
        if (titulomodal == "Lugar edición") {
            this.array_enviar.lugar_edicion.splice(i, 1);
        }
        if (titulomodal == "Editorial") {
            this.array_enviar.editorial.splice(i, 1);
        }
        if (titulomodal == "Area") {
            this.array_enviar.area.splice(i, 1);
            this.array_enviar.area = null;
        }
        if (titulomodal == "Tipo de obra") {
            this.array_enviar.tipo_obra.splice(i, 1);
            this.array_enviar.tipo_obra = null;
        }
        if (titulomodal == "Pais") {
            this.array_enviar.autores[i].pais.splice(i_pais, 1);
        }
        if (titulomodal == "Institución") {
            this.array_enviar.autores[i].institucion.splice(i_pais, 1);
        }
    }
    /* +++++++++++++++  */
    seleccionarArchivo(event) {
        this.formFile = true;
        /* Cargo archivos en array myFiles */
        for (var i = 0; i < event.target.files.length; i++) {
            this.myFiles.push(event.target.files[i]);
        }
        /* Escribo archivo en el array con descricion y fecha */
        if (!this.array_enviar.archivos) {
            this.array_enviar.archivos = [];
        }
        for (var i = 0; i < event.target.files.length; i++) {
            this.array_enviar.archivos.push({
                archivo: event.target.files[i].name,
                fecha_embargo: '0'
            });
        }
        console.log(this.myFiles);
        console.log(this.array_enviar.archivos);
    }
    removeFile(i) {
        this.array_enviar.archivos.splice(i, 1);
        this.myFiles.splice(i, 1);
        if (this.array_enviar.archivos.length == 0) {
            this.array_enviar.archivos = null;
        }
        console.log(this.myFiles);
        console.log(this.array_enviar.archivos);
    }
    resetFile() {
        for (var i = 0; i < this.array_enviar.archivos.length; i++) {
            this.array_enviar.archivos[i].archivo = null;
        }
    }
    addBuscaAutores() {
        this.cargando_autor = true;
        this.msj_autor = null;
        for (let i = 0; this.array_enviar.autores.length > i; i++) {
            if (this.array_enviar.autores[i].email == this.busca_autor) {
                this.busca_autor = null;
                this.cargando_autor = false;
                this.msj_autor = "El autor ya existe en su lista";
            }
        }
        if (this.busca_autor) {
            // Busca autores
            let busca_autor = {
                tipo: "buscarAutor",
                campo: this.busca_autor //  "riolmos@uc.cl"
            };
            const formData = new FormData();
            formData.append('array_insert', JSON.stringify(busca_autor));
            this.http.post(this.urlSql, formData)
                .subscribe(response => {
                this.cargando_autor = false;
                if (response['code'] == 404) { /* Verifica sesion */
                    console.log(response['msg']);
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response == 400) {
                    this.msj_autor = "No existe ese usuario UC";
                    console.log(response);
                }
                else {
                    this.msj_autor = null;
                    if (response[0].nacionalidad == ' ') {
                        var pais = "Chile";
                    }
                    else {
                        pais = response[0].nacionalidad;
                    }
                    ;
                    this.array_enviar.autores.push({
                        email: response[0].correo,
                        rol: null,
                        nombres: null,
                        apellidos: null,
                        nombre_completo: response[0].nombre_completo,
                        ORCID: null,
                        pais: [{
                                nuevo: "no",
                                nombre: pais
                            }],
                        institucion: [{
                                nuevo: "no",
                                nombre: "UC"
                            }]
                    });
                }
                console.log(response);
            }, err => {
                this.msj_autor = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
                this.cargando_autor = false;
                console.log(err);
            });
        }
    }
    addAutores() {
        this.array_enviar.autores.push({
            email: null,
            rol: null,
            nombres: null,
            apellidos: null,
            nombre_completo: null,
            ORCID: null,
            pais: null,
            institucion: []
        });
    }
    removeAutores(i) {
        this.array_enviar.autores.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    cheq(event) {
        if (!this.array_enviar.indexacion) {
            this.array_enviar.indexacion = [];
        }
        if (event.target.checked) {
            this.array_enviar.indexacion.push(event.target.value);
        }
        else {
            for (var i = 0; this.array_enviar.indexacion.length > i; i++) {
                if (this.array_enviar.indexacion[i] == event.target.value) {
                    this.array_enviar.indexacion.splice(i, 1);
                }
            }
        }
    }
    SelectIndexacion(event) {
        if (event.target.value == "Otro") {
            this.array_form.otra_indexacion[0].nombre = null;
            this.newIndexacion = true;
            //document.getElementById('newindexa').focus();
            /* document.getElementById('indexa').style.display = "none"; */
        }
        else {
            this.newIndexacion = false;
        }
    }
    addUrl() {
        if (!this.array_enviar.url_publicacion) {
            this.array_enviar.url_publicacion = [];
        }
        this.array_enviar.url_publicacion.push({
            url: null
        });
        //document.getElementById('input_13_0').focus();
    }
    removeUrl(i) {
        this.array_enviar.url_publicacion.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addEditores() {
        if (!this.array_enviar.editores) {
            this.array_enviar.editores = [];
        }
        this.array_enviar.editores.push({
            editor: null
        });
    }
    removeEditores(i) {
        this.array_enviar.editores.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addAuspiciadora() {
        if (!this.array_enviar.institucion_auspiciadora) {
            this.array_enviar.institucion_auspiciadora = [];
        }
        this.array_enviar.institucion_auspiciadora.push({
            nombre: null
        });
    }
    removeAuspiciadora(i) {
        this.array_enviar.institucion_auspiciadora.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addPatrocinadora() {
        if (!this.array_enviar.institucion_patrocinadora) {
            this.array_enviar.institucion_patrocinadora = [];
        }
        this.array_enviar.institucion_patrocinadora.push({
            nombre: null
        });
    }
    removePatrocinadora(i) {
        this.array_enviar.institucion_patrocinadora.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    /* /////////////// APIS //////////////////  */
    // GUARDADOS SIN ENVIAR
    mis_guardados() {
        this.cargando = true;
        this.msj = null;
        var acciones;
        const formData = new FormData();
        /* Armo acciones */
        acciones = { accion: "consultar", id: "", email: localStorage.getItem('correo'), validaBiblio: "", start: "", handle: "" };
        console.log(acciones);
        /* Entrgo array */
        formData.append('array_acciones', JSON.stringify(acciones));
        /* +++++ CARGAR EN BBDD ++++ */
        this.http.post(this.urlGuardar, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else {
                if (response) {
                    console.log(response);
                    this.guardados = response;
                    this.guardados = this.guardados.length;
                }
                else {
                    this.msj = "No existen documentos guardados";
                }
            }
        }, err => {
            this.cargando = false;
            this.msj = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            console.log(err);
        });
    }
    /* +++ INSERTAR GUARDAR FORM */
    vinculaPost(id) {
        //this.pasos = true
        localStorage.removeItem("idguardar");
        //document.getElementById('top').scrollIntoView({behavior: 'smooth'});
        if (id == "registrar") {
            this.pasos = true;
            document.getElementById('top').scrollIntoView({ behavior: 'smooth' });
            /* Insert nuevo borrador */
            let limpiar_array = this.array_enviar;
            for (let i in limpiar_array) {
                if (limpiar_array[i] == null || limpiar_array[i] == '') {
                    delete limpiar_array[i];
                }
            }
            console.log(limpiar_array);
            /* Armo acciones */
            var acciones;
            acciones = { accion: "guardar", id: "", email: localStorage.getItem('correo'), validaBiblio: "" };
            const formData = new FormData();
            /* Entrgo array */
            formData.append('array_acciones', JSON.stringify(acciones));
            formData.append('array_borrador', JSON.stringify(limpiar_array));
            /* +++++ CARGAR EN BBDD ++++ */
            this.http.post(this.urlGuardar, formData)
                .subscribe(response => {
                this.cargando = false;
                if (response['code'] == 404) { /* Verifica sesion */
                    window.location.href = global.php + '/cas-log/cas-login.php';
                }
                else if (response['httpCode'] == "200") {
                    /* this.id_post_draft = response['mensaje']; */
                    localStorage.setItem('idguardar', response['mensaje']);
                    this.msj_guardar = "Se guardó borrador en Mi producción académica.";
                    console.log(response);
                    console.log(this.id_post_draft);
                    //this.router.navigate(['/mispublicaciones']);
                }
                else {
                    this.msj_guardar = "Error de conexión, no pudo ser guardado";
                    console.log("se conectó pero no trajo httpCode 201");
                    console.log(response);
                }
            }, err => {
                this.msj_guardar = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
                this.cargando = false;
                console.log("error en api");
                console.log(err);
            });
            this.vincula = false;
        }
        else {
            /* this.vincula = true;
            this.array_vincula.idpost = id.data;
            this.array_vincula.fuente = id.fuente; */
            this.bolsaVincula = true;
            this.array_vincula.push({
                idpost: id.data,
                fuente: id.fuente,
                emailuser: localStorage.getItem("correo"),
                rol: "Serás vinculado como autor",
                titulo: id.titulo,
                autores: id.autores,
                fecha: id.fecha,
                tag: id.tag,
                revista: id.revista,
                capitulo: id.capitulo,
                handle: id.handle
            });
            console.log(this.array_vincula);
        }
    }
    eliminaBolsa(i) {
        this.array_vincula.splice(i, 1);
    }
    bolsaVinculaciones() {
        this.pasos = true;
        document.getElementById('top').scrollIntoView({ behavior: 'smooth' });
        this.vincula = true;
        this.bolsaVincula = false;
    }
    /* +++ EDITAR GUARDAR FORM +++ */
    actualizarPost() {
        let limpiar_array = this.array_enviar;
        for (let i in limpiar_array) {
            if (limpiar_array[i] == null || limpiar_array[i] == '') {
                delete limpiar_array[i];
            }
        }
        /* Armo acciones */
        var acciones;
        acciones = { accion: "actualizar", id: localStorage.getItem('idguardar'), email: localStorage.getItem('correo'), validaBiblio: "" };
        const formData = new FormData();
        /* Entrego archivos */
        for (var i = 0; i < this.myFiles.length; i++) {
            formData.append("file[]", this.myFiles[i]);
        }
        /* Entrgo array */
        formData.append('array_acciones', JSON.stringify(acciones));
        formData.append('array_borrador', JSON.stringify(limpiar_array));
        console.log(acciones);
        console.log(limpiar_array);
        /* +++++ CARGAR EN BBDD ++++ */
        this.http.post(this.urlGuardar, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else if (response['httpCode'] == "200") {
                this.msj_guardar = "Se actualizó borrador en Mi producción académica.";
                console.log(response);
            }
            else {
                this.msj_guardar = "Error de conexión, no pudo ser guardado";
                console.log("se conectó pero no trajo httpCode 201");
                console.log(response);
            }
        }, err => {
            this.msj_guardar = "¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde";
            this.cargando = false;
            console.log("error en api");
            console.log(err);
        });
    }
    // +++++++++++ ENVIO FORM +++++++++++++++
    submitPublica(form) {
        this.cargando = true;
        const formData = new FormData();
        this.urlfile = this.urlRegistrar;
        /* Quito Seleccionar si no se ingresó nada */
        if (this.array_comunes.financiamiento == "Seleccionar") {
            this.array_comunes.financiamiento = null;
        }
        let limpiar_array = this.array_enviar;
        for (let i in limpiar_array) {
            if (limpiar_array[i] == null || limpiar_array[i] == '') {
                delete limpiar_array[i];
            }
        }
        console.log(JSON.stringify(limpiar_array));
        console.log(limpiar_array);
        /* Entrego archivos */
        for (var i = 0; i < this.myFiles.length; i++) {
            formData.append("file[]", this.myFiles[i]);
        }
        /* Desde guardados */
        if (localStorage.getItem("idguardar")) {
            formData.append('id_eliminar', localStorage.getItem("idguardar"));
            console.log(localStorage.getItem("idguardar"));
            console.log('desde guardar');
        }
        else {
            formData.append('id_eliminar', 'no');
            console.log('desde normal');
        }
        //formData.append('id_eliminar', 'no');
        formData.append('array_datos', JSON.stringify(limpiar_array));
        /* +++++ ENVIAR A API ++++ */
        this.http.post(this.urlfile, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['code'] == 404) { /* Verifica sesion */
                window.location.href = global.php + '/cas-log/cas-login.php';
            }
            else if (response['httpCode'] == "200" || response['httpCode'] == "201") { /* if(response['resultado'] == "200"){ */
                localStorage.removeItem('idguardar');
                form.reset();
                this.msj = "success";
                this.msjSend = "Proceso exitoso";
                //this.array_comunes.concedo_licencia = false;
                console.log("trajo resultados");
                console.log(response);
                if (response['httpCodeCorreo'] == "200" || response['httpCodeCorreo'] == "201") {
                    this.msjEmail = "Se ha enviado email de confirmación a su cuenta";
                }
                else {
                    this.msjEmail = "No hemos podido enviar email de confirmación a su cuenta";
                }
                console.log(i);
            }
            else if (response['httpCode'] == "201") {
                localStorage.removeItem('idguardar');
                form.reset();
                this.msj = "success";
                //this.array_comunes.concedo_licencia= false;
                console.log("Usuario vinculado anteriormente");
                console.log(response);
            }
            else {
                this.msj = "error";
                //form.reset();
                //this.array_comunes.concedo_licencia= false;
                console.log("se conectó pero no trajo httpCode 201");
                console.log(response);
            }
        }, err => {
            this.msj = "error";
            this.cargando = false;
            console.log("¡Disculpa! Hemos perdido la conexión, inténtalo mas tarde");
            console.log(err);
        });
    }
};
UploadRecordComponent = tslib_1.__decorate([
    Component({
        selector: 'app-upload-record',
        templateUrl: './upload-record.component.html',
        styleUrls: ['./upload-record.component.css'],
        animations: [
            trigger('slide', [
                state('t1', style({ transform: 'translateX(0)' })),
                state('t2', style({ transform: 'translateX(-25%)' })),
                state('t3', style({ transform: 'translateX(-50%)' })),
                state('t4', style({ transform: 'translateX(-75%)' })),
                transition('* => *', animate(300))
            ])
        ]
    })
], UploadRecordComponent);
export { UploadRecordComponent };
//# sourceMappingURL=upload-record.component.js.map