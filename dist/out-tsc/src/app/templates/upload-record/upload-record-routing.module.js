import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UploadRecordComponent } from './upload-record.component';
const routes = [
    { path: '', component: UploadRecordComponent }
];
let UploadRecordRoutingModule = class UploadRecordRoutingModule {
};
UploadRecordRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], UploadRecordRoutingModule);
export { UploadRecordRoutingModule };
//# sourceMappingURL=upload-record-routing.module.js.map