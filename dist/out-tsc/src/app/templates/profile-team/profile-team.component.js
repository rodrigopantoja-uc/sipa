import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './profile-team.json';
let ProfileTeamComponent = class ProfileTeamComponent {
    constructor(queriesService) {
        this.queriesService = queriesService;
        this.data = [];
        this.json = json;
    }
    ngOnInit() {
        this.queriesService.queryGet('http://localhost:3000/users?id=00000001').then((data) => {
            this.data['user'] = data[0];
        });
        this.queriesService.queryGet('http://localhost:3000/users').then((data) => { this.data['users'] = data; });
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    }
    printProfile(dataPrint) {
        let printContents = dataPrint.innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
};
ProfileTeamComponent = tslib_1.__decorate([
    Component({
        selector: 'app-profile-team',
        templateUrl: './profile-team.component.html',
        styleUrls: ['./profile-team.component.css']
    })
], ProfileTeamComponent);
export { ProfileTeamComponent };
//# sourceMappingURL=profile-team.component.js.map