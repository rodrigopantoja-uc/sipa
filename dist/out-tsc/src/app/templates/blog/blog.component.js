import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from '../../../assets/json/blog/blog.json';
import { global } from '../../services/global';
let BlogComponent = class BlogComponent {
    constructor(activatedRoute, queriesService) {
        this.activatedRoute = activatedRoute;
        this.queriesService = queriesService;
        this.data = [];
        this.json = json;
        this.urlPhp = global.php;
    }
    ngOnInit() {
        // refresca página
        this.queriesService.getRefresh();
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        // Recibo datos por get
        this.activatedRoute.params.subscribe(params => {
            this.data['id'] = params['id'];
        });
        this.queriesService.queryGet(this.urlPhp + '/api-mailchimp/landing.php?page=' + this.data['id']).then((data) => {
            this.data['blog'] = data['landing'];
            $("#blog").html(this.data['blog']);
        });
    }
};
BlogComponent = tslib_1.__decorate([
    Component({
        selector: 'app-blog',
        templateUrl: './blog.component.html',
        styleUrls: ['./blog.component.css']
    })
], BlogComponent);
export { BlogComponent };
//# sourceMappingURL=blog.component.js.map