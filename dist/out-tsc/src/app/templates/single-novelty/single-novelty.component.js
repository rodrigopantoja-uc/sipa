import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import json from './single-novelty.json';
let SingleNoveltyComponent = class SingleNoveltyComponent {
    constructor(queriesService, _sanitizer) {
        this.queriesService = queriesService;
        this._sanitizer = _sanitizer;
        this.data = [];
        this.json = json;
        this.response = {
            'show': false,
            'data': ''
        };
        this.novelty_id = "0000";
        this.liked = false;
    }
    ngOnInit() {
        this.queriesService.queryGet('http://localhost:3000/news?id=' + this.novelty_id).then((data) => { this.data['news'] = data[0]; });
        this.queriesService.queryGet('http://localhost:3000/publics').then((data) => { this.data['publics'] = data; });
        this.queriesService.queryGet('http://localhost:3000/comments').then((data) => { this.data['comments'] = data; });
        this.likeNovelty();
    }
    likeNovelty() {
        this.queriesService.queryGet('http://localhost:3000/likes').then((data) => {
            this.liked = data['liked'];
            this.queriesService.queryPost('http://localhost:3000/likes', { liked: !data['liked'] });
        });
    }
    resetForm() {
        this.response.show = false;
        this.response.text = '';
    }
    submitComment(form, $event) {
        $event.preventDefault();
        if (form.form.value) {
            let date = new Date();
            let full_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
            let comment = {
                user: "Napoleón Gómez",
                date: full_date,
                text: form.form.value['comment_textarea']
            };
            this.queriesService.queryPost('http://localhost:3000/comments', comment).then((data) => {
                this.queriesService.queryGet('http://localhost:3000/comments').then((data) => { this.data['comments'] = data; });
                this.response.show = true;
                this.response.text = 'Su comentario ha sido publicado con éxito';
            }, (error) => {
                this.response.show = true;
                this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
            });
        }
    }
    getBackgroundImage(image) {
        return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
    }
};
SingleNoveltyComponent = tslib_1.__decorate([
    Component({
        selector: 'app-single-novelty',
        templateUrl: './single-novelty.component.html',
        styleUrls: ['./single-novelty.component.css']
    })
], SingleNoveltyComponent);
export { SingleNoveltyComponent };
//# sourceMappingURL=single-novelty.component.js.map