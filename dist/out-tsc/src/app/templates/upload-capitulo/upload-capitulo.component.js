import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import json from '../../../assets/json/datos-investigacion/datos-capitulo.json';
import compartir from '../../../assets/json/upload-record/02-compartir-investigacion.json';
import { global } from '../../services/global';
let UploadCapituloComponent = class UploadCapituloComponent {
    constructor(uploadService, _queriesService, router, activatedRoute, http) {
        this.uploadService = uploadService;
        this._queriesService = _queriesService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.myFiles = [];
        this.data = [];
        this.json = json;
        this.compartir = compartir;
        this.activePane = 't1';
        this.array_form = {
            publicacion: "capitulo libro",
            clave: this._queriesService.getPass(),
            user: localStorage.getItem("correo"),
            concedo_licencia: null,
            titulo_libro: null,
            titulo_capitulo: null,
            volumen: null,
            pagina_inicio: null,
            pagina_termino: null,
            editores: [{
                    nombre: null
                }],
            lugar_edicion: [{
                    nuevo: null,
                    nombre: null
                }],
            editorial: [{
                    nuevo: null,
                    nombre: null
                }],
            anio_publicacion: null,
            isbn: null,
            financiamiento: "Seleccionar",
            otro_financiamiento: [{
                    nuevo: null,
                    nombre: null
                }],
            afiliacion_uc: "Si",
            url_publicacion: [{
                    url: null
                }],
            autor: [{
                    COD_PERS: null,
                    RUT: null,
                    DV: null,
                    NOMBRE_COMPLETO: null,
                    UNIDAD_ACADEMICA: null,
                    FACULTAD: null
                }],
            autores: [{
                    nombre_autor: [{
                            nuevo: null,
                            nombresAutor: null,
                            apellidosAutor: null,
                            nombre: null,
                            ORCID: null,
                            WOS: null
                        }],
                    pais: [{
                            nuevo: null,
                            nombre: null
                        }],
                    institucion: [{
                            nuevo: null,
                            nombre: null
                        }],
                    autor_uc: "Si",
                    facultad: [{
                            nuevo: null,
                            nombre: null
                        }],
                    unidad_academica: [{
                            nuevo: null,
                            nombre: null
                        }],
                    departamento: [{
                            nuevo: null,
                            nombre: null
                        }]
                }],
            archivos: [{
                    archivo: null,
                    fecha_embargo: ''
                }]
        };
        this.formFile = false;
        this.urlfile = global.php + "/formulario-sipa/formulario.php";
        this.urlSql = global.php + "/formulario-sipa/sql.php";
        this.modal = false;
        this.identity = this._queriesService.getIdentity();
        this.token = this._queriesService.getToken();
        this.password = this._queriesService.getPass();
    }
    ngOnInit() {
        // refresca página
        this._queriesService.getRefresh();
        this.date();
        this.usuario = localStorage.getItem('usuario');
        // JQuery ir arriba
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
        // Select Financiamiento
        var tabla = "tf";
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla)
            .then((data) => {
            this.lista_financiamiento = Object.keys(data).map(i => data[i]);
            this.lista_financiamiento = this.lista_financiamiento[0];
        });
    }
    ngDoCheck() {
        this.password = this._queriesService.getPass();
    }
    date() {
        var pruebaFecha = document.createElement("input");
        //Si tiene soporte: debe aceptar el tipo "date"...
        pruebaFecha.setAttribute("type", "date");
        if (pruebaFecha.type === "date") {
            this.soportadateInput = true;
        }
        else {
            this.soportadateInput = false;
        }
    }
    loguearse() {
        this.login = true;
        this.errLog = false;
    }
    closeLogin() {
        this.login = false;
    }
    seleccionarArchivo(event) {
        this.formFile = true;
        /* Cargo archivos en array myFiles */
        for (var i = 0; i < event.target.files.length; i++) {
            this.myFiles.push(event.target.files[i]);
        }
        /* Escribo archivo en el array con descricion y fecha */
        for (var i = 0; i < event.target.files.length; i++) {
            /* Condiciones para no dejar vacío campo 0 */
            if (this.array_form.archivos[0].archivo == null) {
                this.array_form.archivos[0].archivo = event.target.files[i].name;
                this.array_form.archivos[0].fecha_embargo = "0";
            }
            else {
                this.array_form.archivos.push({
                    archivo: event.target.files[i].name,
                    fecha_embargo: '0'
                });
            }
        }
        console.log(this.myFiles);
        console.log(this.array_form.archivos);
    }
    removeFile(i) {
        /* Condiciones para no dejar vacío campo 0 */
        if (this.array_form.archivos.length == 1) {
            this.array_form.archivos[0].archivo = null;
            this.array_form.archivos[0].fecha_embargo = "0";
        }
        else {
            this.array_form.archivos.splice(i, 1);
        }
        this.myFiles.splice(i, 1);
        console.log(this.myFiles);
        console.log(this.array_form.archivos);
    }
    resetFile() {
        for (var i = 0; i < this.array_form.archivos.length; i++) {
            this.array_form.archivos[i].archivo = null;
        }
    }
    newUpload(form) {
        this.msj = "";
        this.activePane = 't1';
        location.reload();
    }
    toScroll() {
        document.getElementById('pasos').scrollIntoView({ behavior: 'smooth' });
    }
    addAutores() {
        this.array_form.autores.push({
            nombre_autor: [{
                    nuevo: null,
                    nombresAutor: null,
                    apellidosAutor: null,
                    nombre: null,
                    ORCID: null,
                    WOS: null
                }],
            pais: [{
                    nuevo: null,
                    nombre: null
                }],
            institucion: [{
                    nuevo: null,
                    nombre: null
                }],
            autor_uc: "Si",
            facultad: [{
                    nuevo: null,
                    nombre: null
                }],
            unidad_academica: [{
                    nuevo: null,
                    nombre: null
                }],
            departamento: [{
                    nuevo: null,
                    nombre: null
                }]
        });
    }
    removeAutores(i) {
        this.array_form.autores.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addEditores() {
        this.array_form.editores.push({
            nombre: null
        });
    }
    removeEditores(i) {
        this.array_form.editores.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    addUrl() {
        this.array_form.url_publicacion.push({
            url: null
        });
    }
    removeUrl(i) {
        this.array_form.url_publicacion.splice(i, 1); // elimina 1 indice a partir del indice i
    }
    abreModal(data, i) {
        this.modal = true;
        this.tituloModal = data;
        this.numautor = i;
    }
    cerrarModal() {
        this.modal = false;
        this.data['search'] = null;
        this.terminodebusqueda = null;
        this.registrosencontrados = null;
    }
    getSearch(data) {
        this.cargando = true;
        if (this.tituloModal == "Lugar edición") {
            var tabla = "l";
        }
        if (this.tituloModal == "Editorial") {
            var tabla = "e";
        }
        if (this.tituloModal == "Otro financiamiento") {
            var tabla = "of";
        }
        if (this.tituloModal == "Autor") {
            var tabla = "a";
        }
        if (this.tituloModal == "Pais") {
            var tabla = "p";
        }
        if (this.tituloModal == "Institución") {
            var tabla = "i";
        }
        if (this.tituloModal == "Autor UC") {
            var tabla = "auc";
        }
        if (this.tituloModal == "Facultad") {
            var tabla = "f";
        }
        if (this.tituloModal == "Unidad Académica") {
            var tabla = "u";
        }
        if (this.tituloModal == "Departamento") {
            var tabla = "d";
        }
        // ++++++++++++++++++ ENVÍO CONSULTA API Y DEVUELVE REGISTROS ++++++++++++++++++++++
        this._queriesService.queryGet(this.urlSql + '?campo=' + tabla + '&busqueda=' + this.terminodebusqueda)
            .then((data) => {
            this.cargando = false;
            this.data['search'] = Object.keys(data).map(i => data[i]);
            this.registrosencontrados = this.data['search'][0].length;
        });
    }
    registrosCheq(event) {
        if (this.tituloModal == "Lugar edición") {
            if (event.target.checked) {
                /* Condiciones para no dejar vacío campo 0 */
                if (this.array_form.lugar_edicion[0].nombre == null) {
                    this.array_form.lugar_edicion[0].nombre = event.target.value;
                    this.array_form.lugar_edicion[0].nuevo = "no";
                }
                else {
                    this.array_form.lugar_edicion.push({
                        nombre: event.target.value,
                        nuevo: "no"
                    });
                }
            }
            else {
                for (var i = 0; this.array_form.lugar_edicion.length > i; i++) {
                    if (this.array_form.lugar_edicion[i].nombre == event.target.value) {
                        if (this.array_form.lugar_edicion.length == 1) {
                            this.array_form.lugar_edicion[0].nombre = null;
                            this.array_form.lugar_edicion[0].nuevo = null;
                        }
                        else {
                            this.array_form.lugar_edicion.splice(i, 1);
                        }
                    }
                }
            }
        }
        if (this.tituloModal == "Editorial") {
            if (event.target.checked) {
                /* Condiciones para no dejar vacío campo 0 */
                if (this.array_form.editorial[0].nombre == null) {
                    this.array_form.editorial[0].nombre = event.target.value;
                    this.array_form.editorial[0].nuevo = "no";
                }
                else {
                    this.array_form.editorial.push({
                        nombre: event.target.value,
                        nuevo: "no"
                    });
                }
            }
            else {
                for (var i = 0; this.array_form.editorial.length > i; i++) {
                    if (this.array_form.editorial[i].nombre == event.target.value) {
                        if (this.array_form.editorial.length == 1) {
                            this.array_form.editorial[0].nombre = null;
                            this.array_form.editorial[0].nuevo = null;
                        }
                        else {
                            this.array_form.editorial.splice(i, 1);
                        }
                    }
                }
            }
        }
        if (this.tituloModal == "Otro financiamiento") {
            if (event.target.checked) {
                /* Condiciones para no dejar vacío campo 0 */
                if (this.array_form.otro_financiamiento[0].nombre == null) {
                    this.array_form.otro_financiamiento[0].nombre = event.target.value;
                    this.array_form.otro_financiamiento[0].nuevo = "no";
                }
                else {
                    this.array_form.otro_financiamiento.push({
                        nombre: event.target.value,
                        nuevo: "no"
                    });
                }
            }
            else {
                for (var i = 0; this.array_form.otro_financiamiento.length > i; i++) {
                    if (this.array_form.otro_financiamiento[i].nombre == event.target.value) {
                        if (this.array_form.otro_financiamiento.length == 1) {
                            this.array_form.otro_financiamiento[0].nombre = null;
                            this.array_form.otro_financiamiento[0].nuevo = null;
                        }
                        else {
                            this.array_form.otro_financiamiento.splice(i, 1);
                        }
                    }
                }
            }
        }
        if (this.tituloModal == "Institución") {
            if (event.target.checked) {
                /* Condiciones para no dejar vacío campo 0 */
                if (this.array_form.autores[this.numautor].institucion[0].nombre == null) {
                    this.array_form.autores[this.numautor].institucion[0].nombre = event.target.value;
                    this.array_form.autores[this.numautor].institucion[0].nuevo = "no";
                }
                else {
                    this.array_form.autores[this.numautor].institucion.push({
                        nombre: event.target.value,
                        nuevo: "no"
                    });
                }
            }
            else {
                for (var i = 0; this.array_form.autores[this.numautor].institucion.length > i; i++) {
                    if (this.array_form.autores[this.numautor].institucion[i].nombre == event.target.value) {
                        if (this.array_form.autores[this.numautor].institucion.length == 1) {
                            this.array_form.autores[this.numautor].institucion[0].nombre = null;
                            this.array_form.autores[this.numautor].institucion[0].nuevo = null;
                        }
                        else {
                            this.array_form.autores[this.numautor].institucion.splice(i, 1);
                        }
                    }
                }
            }
        }
    }
    selectList(data) {
        if (this.tituloModal == "Lugar edición") {
            this.array_form.lugar_edicion[0].nombre = data;
            this.array_form.lugar_edicion[0].nuevo = "no";
        }
        if (this.tituloModal == "Editorial") {
            this.array_form.editorial[0].nombre = data;
            this.array_form.editorial[0].nuevo = "no";
        }
        if (this.tituloModal == "Otro financiamiento") {
            this.array_form.otro_financiamiento[0].nombre = data;
            this.array_form.otro_financiamiento[0].nuevo = "no";
        }
        if (this.tituloModal == "Pais") {
            this.array_form.autores[this.numautor].pais[0].nombre = data;
            this.array_form.autores[this.numautor].pais[0].nuevo = "no";
        }
        if (this.tituloModal == "Institución") {
            this.array_form.autores[this.numautor].institucion[0].nombre = data;
            this.array_form.autores[this.numautor].institucion[0].nuevo = "no";
        }
        if (this.tituloModal == "Facultad") {
            this.array_form.autores[this.numautor].facultad[0].nombre = data;
            this.array_form.autores[this.numautor].facultad[0].nuevo = "no";
        }
        if (this.tituloModal == "Unidad Académica") {
            this.array_form.autores[this.numautor].unidad_academica[0].nombre = data;
            this.array_form.autores[this.numautor].unidad_academica[0].nuevo = "no";
        }
        if (this.tituloModal == "Departamento") {
            this.array_form.autores[this.numautor].departamento[0].nombre = data;
            this.array_form.autores[this.numautor].departamento[0].nuevo = "no";
        }
        this.cerrarModal();
    }
    registrosNew(data) {
        if (this.tituloModal == "Lugar edición") {
            if (this.array_form.lugar_edicion[0].nombre == null) {
                this.array_form.lugar_edicion[0].nombre = data;
                this.array_form.lugar_edicion[0].nuevo = "si";
            }
            else {
                this.array_form.lugar_edicion.push({
                    nombre: data,
                    nuevo: "si"
                });
            }
            this.insertCampo("Lugar_edicion", data, null);
        }
        if (this.tituloModal == "Editorial") {
            if (this.array_form.editorial[0].nombre == null) {
                this.array_form.editorial[0].nombre = data;
                this.array_form.editorial[0].nuevo = "si";
            }
            else {
                this.array_form.editorial.push({
                    nombre: data,
                    nuevo: "si"
                });
            }
            this.insertCampo(this.tituloModal, data, null);
        }
        if (this.tituloModal == "Otro financiamiento") {
            if (this.array_form.otro_financiamiento[0].nombre == null) {
                this.array_form.otro_financiamiento[0].nombre = data;
                this.array_form.otro_financiamiento[0].nuevo = "si";
            }
            else {
                this.array_form.otro_financiamiento.push({
                    nombre: data,
                    nuevo: "si"
                });
            }
            this.insertCampo("Otro_financiamiento", data, null);
        }
        if (this.tituloModal == "Pais") {
            this.array_form.autores[this.numautor].pais[0].nombre = data;
            this.array_form.autores[this.numautor].pais[0].nuevo = "si";
            this.insertCampo(this.tituloModal, data, null);
        }
        if (this.tituloModal == "Institución") {
            if (this.array_form.autores[this.numautor].institucion[0].nombre == null) {
                this.array_form.autores[this.numautor].institucion[0].nombre = data;
                this.array_form.autores[this.numautor].institucion[0].nuevo = "si";
            }
            else {
                this.array_form.autores[this.numautor].institucion.push({
                    nombre: data,
                    nuevo: "si"
                });
            }
            let pais = this.array_form.autores[this.numautor].pais[0].nombre;
            this.insertCampo("Institucion", data, pais);
        }
        if (this.tituloModal == "Facultad") {
            this.array_form.autores[this.numautor].facultad[0].nombre = data;
            this.array_form.autores[this.numautor].facultad[0].nuevo = "si";
            this.insertCampo(this.tituloModal, data, null);
        }
        if (this.tituloModal == "Unidad Académica") {
            this.array_form.autores[this.numautor].unidad_academica[0].nombre = data;
            this.array_form.autores[this.numautor].unidad_academica[0].nuevo = "si";
            this.insertCampo("Unidad_academ", data, null);
        }
        if (this.tituloModal == "Departamento") {
            this.array_form.autores[this.numautor].departamento[0].nombre = data;
            this.array_form.autores[this.numautor].departamento[0].nuevo = "si";
            this.insertCampo(this.tituloModal, data, null);
        }
    }
    insertCampo(tipo, campo, dpais) {
        /* Insert Nuevo Campo */
        let array_newCampo = {};
        if (dpais == null) {
            array_newCampo = {
                tipo: tipo,
                campo: campo,
            };
        }
        else {
            array_newCampo = {
                tipo: tipo,
                campo: campo,
                pais: dpais
            };
        }
        console.log(JSON.stringify(array_newCampo));
        const formData = new FormData();
        formData.append('array_insert', JSON.stringify(array_newCampo));
        this.http.post(this.urlSql, formData)
            .subscribe(response => {
            if (response == "200") {
                console.log(response);
                this.cerrarModal();
            }
            else {
                console.log("se conectó pero no trajo resultado 200");
                console.log(response);
                alert("No pudo ser incluido a la lista");
            }
        });
    }
    removeListaModal(pagina, i, ind) {
        if (pagina == "Lugar edición") {
            if (this.array_form.lugar_edicion.length == 1) {
                this.array_form.lugar_edicion[0].nombre = null;
                this.array_form.lugar_edicion[0].nuevo = null;
            }
            else {
                this.array_form.lugar_edicion.splice(i, 1);
            }
        }
        if (pagina == "Editorial") {
            if (this.array_form.editorial.length == 1) {
                this.array_form.editorial[0].nombre = null;
                this.array_form.editorial[0].nuevo = null;
            }
            else {
                this.array_form.editorial.splice(i, 1);
            }
        }
        if (pagina == "Otro financiamiento") {
            if (this.array_form.otro_financiamiento.length == 1) {
                this.array_form.otro_financiamiento[0].nombre = null;
                this.array_form.otro_financiamiento[0].nuevo = null;
            }
            else {
                this.array_form.otro_financiamiento.splice(i, 1);
            }
        }
        if (pagina == "Indexación") {
            if (this.array_form.otra_indexacion.length == 1) {
                this.array_form.otra_indexacion[0].nombre = null;
                this.array_form.otra_indexacion[0].nuevo = null;
            }
            else {
                this.array_form.otra_indexacion.splice(i, 1);
            }
        }
        if (pagina == "Pais") {
            if (this.array_form.autores[i].pais.length == 1) {
                this.array_form.autores[i].pais[0].nombre = null;
                this.array_form.autores[i].pais[0].nuevo = null;
            }
            else {
                this.array_form.autores[i].pais.splice(ind, 1);
            }
        }
        if (pagina == "Institución") {
            if (this.array_form.autores[i].institucion.length == 1) {
                this.array_form.autores[i].institucion[0].nombre = null;
                this.array_form.autores[i].institucion[0].nuevo = null;
            }
            else {
                this.array_form.autores[i].institucion.splice(ind, 1);
            }
        }
        if (pagina == "Facultad") {
            if (this.array_form.autores[i].facultad.length == 1) {
                this.array_form.autores[i].facultad[0].nombre = null;
                this.array_form.autores[i].facultad[0].nuevo = null;
            }
            else {
                this.array_form.autores[i].facultad.splice(ind, 1);
            }
        }
        if (pagina == "Unidad Académica") {
            if (this.array_form.autores[i].unidad_academica.length == 1) {
                this.array_form.autores[i].unidad_academica[0].nombre = null;
                this.array_form.autores[i].unidad_academica[0].nuevo = null;
            }
            else {
                this.array_form.autores[i].unidad_academica.splice(ind, 1);
            }
        }
        if (pagina == "Departamento") {
            if (this.array_form.autores[i].departamento.length == 1) {
                this.array_form.autores[i].departamento[0].nombre = null;
                this.array_form.autores[i].departamento[0].nuevo = null;
            }
            else {
                this.array_form.autores[i].departamento.splice(ind, 1);
            }
        }
    }
    // ENVIO FORM
    submitPublica(form) {
        this.login = false;
        this.cargando = true;
        const formData = new FormData();
        /* Quito Seleccionar si no se ingresó nada */
        if (this.array_form.financiamiento == "Seleccionar") {
            this.array_form.financiamiento = "";
        }
        if (this.array_form.archivos[0].archivo == null) {
            this.array_form.archivos.splice(0, 1);
        }
        /* Construyo nombre completo */
        for (var i = 0; i < this.array_form.autores.length; i++) {
            this.array_form.autores[i].nombre_autor[0].nombre = this.array_form.autores[i].nombre_autor[0].apellidosAutor + ', ' + this.array_form.autores[i].nombre_autor[0].nombresAutor;
        }
        /* Entrgo metadatos */
        formData.append('array_datos', JSON.stringify(this.array_form));
        /* Entrego archivos */
        for (var i = 0; i < this.myFiles.length; i++) {
            formData.append("file[]", this.myFiles[i]);
        }
        //console.log(this.myFiles);
        console.log(this.array_form);
        //console.log(JSON.stringify(this.array_form));
        //console.log(formData);
        /* Envío a API y recibo respuesta */
        this.http.post(this.urlfile, formData)
            .subscribe(response => {
            this.cargando = false;
            if (response['resultado'] == "200") { // response['httpCode'] == "201"
                this.msj = "success";
                //form.reset();
                //this.array_form.concedo_licencia= false;
                console.log(response);
            }
            else if (response['resultado'] == "403") {
                this.login = true;
                this.msj = false;
                this.errLog = true;
                this.array_form.clave = null;
                console.log("se conectó pero login es incorrecto");
                console.log(response['resultado']);
            }
            else {
                this.msj = "error";
                //form.reset();
                //this.array_form.concedo_licencia= false;
                console.log("se conectó pero no trajo resultado 200");
                console.log(response);
            }
        });
    }
};
UploadCapituloComponent = tslib_1.__decorate([
    Component({
        selector: 'app-upload-capitulo',
        templateUrl: './upload-capitulo.component.html',
        styleUrls: ['./upload-capitulo.component.css'],
        animations: [
            trigger('slide', [
                state('t1', style({ transform: 'translateX(0)' })),
                state('t2', style({ transform: 'translateX(-25%)' })),
                state('t3', style({ transform: 'translateX(-50%)' })),
                state('t4', style({ transform: 'translateX(-75%)' })),
                transition('* => *', animate(300))
            ])
        ]
    })
], UploadCapituloComponent);
export { UploadCapituloComponent };
//# sourceMappingURL=upload-capitulo.component.js.map