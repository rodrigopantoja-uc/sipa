import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FaqComponent } from './faq.component';
const routes = [
    { path: '', component: FaqComponent }
];
let FaqRoutingModule = class FaqRoutingModule {
};
FaqRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], FaqRoutingModule);
export { FaqRoutingModule };
//# sourceMappingURL=faq-routing.module.js.map