import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageContentRoutingModule } from './page-content-routing.module';
import { PageContentComponent } from './page-content.component';
let PageContentModule = class PageContentModule {
};
PageContentModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            PageContentComponent
        ],
        imports: [
            CommonModule,
            PageContentRoutingModule
        ]
    })
], PageContentModule);
export { PageContentModule };
//# sourceMappingURL=page-content.module.js.map