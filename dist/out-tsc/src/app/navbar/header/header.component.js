import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import menu_repositorio_uc from '../../../assets/json/menu-repositorio.json';
//import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
//import { FormGroup,  FormBuilder,  Validators }   from '@angular/forms';
//import { QueriesService } from '../../services/queries.service';
let HeaderComponent = class HeaderComponent {
    constructor(
    //private queriesService: QueriesService, private _sanitizer: DomSanitizer, private formBuilder: FormBuilder
    ) {
        this.MenuRepos = menu_repositorio_uc['menu-repositorio-uc'];
        this.usuario = localStorage.getItem('usuario');
    }
    ngOnInit() {
        $("#msjHome").hide();
        if (localStorage.getItem("correo")) {
            this.alertas("success", "check_circle", "Sesión abierta");
        }
    }
    ngDoCheck() {
        // COMPUEBO LOGIN
        if (localStorage.getItem('correo')) {
            this.loggedIn = true;
            this.usuario = localStorage.getItem('usuario');
        }
        else {
            this.loggedIn = false;
        }
    }
    loginCas() {
        window.location.href = 'assets/php/cas-log/cas-login.php';
        /* localStorage.setItem('usuario', 'Rodrigo Pantoja');
        localStorage.setItem('correo', 'rodrigo.pantoja@uc.cl'); */
    }
    logout() {
        localStorage.removeItem('usuario');
        localStorage.removeItem('correo');
        localStorage.removeItem('unidad');
        localStorage.removeItem('jsonAcademico');
        window.location.href = 'assets/php/cas-log/logout-cas.php';
    }
    alertas(estilo, icono, texto) {
        $("#msjHome").fadeIn(1000);
        setInterval(() => {
            $("#msjHome").fadeOut(1500);
        }, 2000);
        this.msj = texto;
        this.estilo = estilo;
        this.icono = icono;
    }
};
HeaderComponent = tslib_1.__decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.css']
    })
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map