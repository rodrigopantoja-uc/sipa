import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let SidebarComponent = class SidebarComponent {
    constructor(dataService, router, activatedRoute) {
        this.dataService = dataService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.usuario = localStorage.getItem('usuario');
        this.pag_active = "publicar";
    }
    ngOnInit() {
        /* ACTIVA MENU SEGUN URL */
        if (this.router.url == "subir-publicacion" || this.router.url == "/misdatos") {
            this.pag_active = "misdatos";
        }
        if (this.router.url == "/upload/subir-publicacion" || this.router.url == "/upload/editar") {
            this.pag_active = "publicar";
        }
        if (this.router.url == "/mispublicaciones/confirmado" || this.router.url == "/mispublicaciones/sinenviar") {
            this.pag_active = "mis_publicaciones";
        }
        this.usuario = localStorage.getItem('usuario');
    }
    ngDoCheck() {
        // COMPUEBO LOGIN
        if (localStorage.getItem('correo')) {
            this.loggedIn = true;
            this.usuario = localStorage.getItem('usuario');
        }
        else {
            this.loggedIn = false;
        }
    }
    activarSidebar(menu) {
        this.dataService.activarSidebar = menu;
    }
};
SidebarComponent = tslib_1.__decorate([
    Component({
        selector: 'app-sidebar',
        templateUrl: './sidebar.component.html',
        styleUrls: ['./sidebar.component.css']
    })
], SidebarComponent);
export { SidebarComponent };
//# sourceMappingURL=sidebar.component.js.map