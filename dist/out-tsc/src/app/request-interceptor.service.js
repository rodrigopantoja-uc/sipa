import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/never';
let RequestInterceptorService = class RequestInterceptorService {
    constructor(authService) {
        this.authService = authService;
    }
    login() {
        this.authService.login();
        return Observable.never(); // block until reload
    }
    intercept(req, next) {
        if (!this.authService.bearer) {
            this.login();
        }
        return next.handle(this.authService.requestAddBearer(req))
            .catch(error => {
            if (error.status === 401) {
                if (!req.params.has("onInit")) {
                    alert("Session timeout, will restart...");
                }
                return this.login();
            }
            else {
                return Observable.throw(error);
            }
        });
    }
};
RequestInterceptorService = tslib_1.__decorate([
    Injectable()
], RequestInterceptorService);
export { RequestInterceptorService };
//# sourceMappingURL=request-interceptor.service.js.map