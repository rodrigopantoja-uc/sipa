import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
let SlashDerechaPipe = class SlashDerechaPipe {
    transform(value) {
        let splits = value.split('/');
        if (splits.length > 1) {
            return splits.pop();
        }
        else {
            return '';
        }
    }
};
SlashDerechaPipe = tslib_1.__decorate([
    Pipe({
        name: "slash_derecha"
    })
], SlashDerechaPipe);
export { SlashDerechaPipe };
//# sourceMappingURL=slash-derecha.pipe.js.map