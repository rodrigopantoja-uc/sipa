import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
let SearchPipe = class SearchPipe {
    transform(lista, texto) {
        /* if(!texto) return lista */
        if (texto === "" || texto.length < 3)
            return lista;
        return lista.filter(user => user.toUpperCase().includes(texto.toUpperCase()));
    }
};
SearchPipe = tslib_1.__decorate([
    Pipe({
        name: 'search'
    })
], SearchPipe);
export { SearchPipe };
//# sourceMappingURL=search.pipe.js.map