import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { global } from '../services/global';
import { map } from 'rxjs/operators';
let FileuploadService = class FileuploadService {
    constructor(http) {
        this.http = http;
        this.URL = global.php + "/autoarchivo/";
        this.urlfile = global.php + "/autoarchivo/subirArchivo.php";
        this.urlPhp = global.php;
    }
    uploadFile(archivo) {
        return this.http.post(`${this.URL}subirArchivo.php`, JSON.stringify(archivo));
    }
    subirfile(archivo) {
        return new Promise((resolve, reject) => {
            this.http.post(this.urlfile, JSON.stringify(archivo)).pipe(map((res) => res)).subscribe((data) => {
                resolve(data);
            }, (err) => {
                console.log(err);
                reject();
            });
        });
    }
    register(archivo) {
        let params = JSON.stringify(archivo);
        console.log(archivo);
        //let params = 'json='+json;
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(this.urlfile, params, { headers: headers });
    }
};
FileuploadService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], FileuploadService);
export { FileuploadService };
//# sourceMappingURL=fileupload.service.js.map