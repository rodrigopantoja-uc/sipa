$(document).ready(function(){
  $('.dropbtn.uc-link').click(function(){
    var aindex = $(this)[0].dataset.dtarget;
    $('.uc-dropdown_list').each(function(k, v){
      if(v.dataset.dropdown != aindex) {
        this.classList.remove("show");
      }
    })
  })

  $('.uc-btn.uc-btn_frames').click(function(){
    var x = this.closest('.carousel');
    var y = x.querySelector('[data-current=true]').dataset.index;
    x.querySelector('.current-slide-index').innerHTML = parseInt(y, 10) + 1;
  })

  jQuery.each(jQuery('[data-role="video-player"]'), function () {
    var $this_video = $(this);
    var video = this;
    var $deployer = $($this_video.siblings()).find('[data-role="video-player-deployer"]');

    $deployer.on("click", function () {
      var $video_prev = $this_video.siblings('[data-role="video-prev"]');

      if (video.paused == true) {
        video.play();
        $deployer.addClass('playing');
        $this_video.attr('controls', 'true');
        $video_prev.addClass('hide').removeClass('paused');
      } else {
        video.pause();
        $video_prev.addClass('paused').removeClass('hide').removeAttr('style');
        $deployer.removeClass('playing');
        $this_video.removeAttr('controls');
      }
    });

    if (video.paused == true) {
      $deployer.addClass('check-playing');
    } else {
      $deployer.removeClass('check-playing');
    }
  });

  /** Inicializacion de datepicker, en español y con fechas con eventos marcadas */
  jQuery.each(jQuery('[data-role=datepicker]'), function () {
    $('[data-role=datepicker]').datepicker($.datepicker.regional['es'] = {
      closeText: 'Cerrar',
      prevText: '<Ant',
      nextText: 'Sig>',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
      dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: '',
      showOptions: { direction: "up" }
    });
  });

  function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}

  jQuery.each(jQuery('.wp-block-file a'), function () {
    var url = $(this);
    var request;
    request = $.ajax({
      type: "HEAD",
      url: url[0].href,
      success: function () {
        // console.log("Size is " + formatBytes(request.getResponseHeader("Content-Length"), 3));
        url.append("<span class='uc-subtitle' style='display: block; text-decoration: underline; text-decoration-color: white;'>"+ formatBytes(request.getResponseHeader("Content-Length"), 3) +"</span>");
      }
    });
  });

  jQuery.each(jQuery('[data-role="search-button"]'), function (e) {
    var $this = $(this);
    $this.click(function(){
      $this.toggle();
      $this.siblings('[data-role="search-button"]').toggle();
      $this.siblings('[data-role="search-input"]').toggle();
    });
  });

  jQuery.each(jQuery('[data-role="collapse-header"]'), function (e) {
    var $this = $(this);
    var $this_icon = $($this.find('.uc-icon'));

    $(window).scroll(function(){
      var scroll = $(window).scrollTop();

      if(scroll >= 120){
        $('header').addClass('sticky');
      }else{
        $('header').removeClass('sticky');
      }
    });

    $this.on('click', function(){
      $('body').toggleClass('nav-open');
      $($this.data('target')).toggleClass('active');
      $this.toggleClass('active');

      if ($this_icon.html() === "menu") {
        $this_icon.html('close');
        if($(window).width() <= 768){
          $($this.data('target')).attr('style', 'height: calc(100vh - '+$('header').height()+'px)');
        }else{
          $($this.data('target')).attr('style', 'height: auto');
        }
      } else {
        $this_icon.html('menu');
        $($this.data('target')).attr('style', 'auto');
      }
    });
  });

  jQuery.each(jQuery('[data-role="open-submenu"]'), function (e) {
    var $this = $(this);

    $this.on('click', function(){
      $('body').toggleClass('nav-open');
      $this.toggleClass('active');

      if ($this.html() === "arrow_drop_down") {
        $this.html('arrow_drop_up');
      } else {
        $this.html('arrow_drop_down')
      }
    });
  });
  
  window.setInterval(function(){
    jQuery.each(jQuery('[data-module="slider"]'), function (e) {
      $(this).commonSlider();
      clearInterval();
    })
  }, 1000);

  // jQuery.each(jQuery('#myChart'), function (){
  //   var ctx = document.getElementById('myChart').getContext('2d');
  //   var myChart = new Chart(ctx, {
  //     type: 'bar',
  //     data: {
  //       labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
  //       datasets: [{
  //         label: 'Trabajos archivados',
  //         data: [950, 500, 345, 450, 700, 200, 950, 500, 345, 450, 700, 200],
  //         backgroundColor: 'rgba(54, 162, 235, 1)',
  //         borderColor: 'rgba(54, 162, 235, 1)',
  //         borderWidth: 1
  //       }]
  //     },
  //     options: {
  //       scales: {
  //         yAxes: [{
  //           ticks: {
  //             beginAtZero: true
  //           }
  //         }]
  //       },
  //       responsive: true,
  //       maintainAspectRatio: false,
  //       legend: {
  //         display: false,
  //       }
  //     }
  //   });
  // });
  //
  // jQuery.each(jQuery('#secondChart'), function (){
  //   var secondCtx = document.getElementById('secondChart').getContext('2d');
  //   var secondChart = new Chart(secondCtx, {
  //     type: 'doughnut',
  //     data: {
  //       datasets: [{
  //         data: [6, 25, 27, 42],
  //         backgroundColor: ['rgba(254, 198, 13, 1)', 'rgba(23, 63, 138, 1)', 'rgba(227, 174, 0, 1)', 'rgba(1, 118, 222, 1)'],
  //         borderColor: ['rgba(254, 198, 13, 1)', 'rgba(23, 63, 138, 1)', 'rgba(227, 174, 0, 1)', 'rgba(1, 118, 222, 1)'],
  //       }],
  //       // These labels appear in the legend and in the tooltips when hovering different arcs
  //       labels: [
  //         'Lorem ipsum',
  //         'Dolor sit',
  //         'Amet',
  //         'Consectetur'
  //       ]
  //     },
  //     options: {
  //       scales: {
  //         yAxes: [{
  //           display: false,
  //           ticks: {
  //             beginAtZero: true
  //           }
  //         }]
  //       },
  //       responsive: true,
  //       maintainAspectRatio: false,
  //       legend: {
  //         align: 'start',
  //         position: 'bottom',
  //         labels: {
  //           boxWidth: 20,
  //         }
  //       }
  //     }
  //   });
  // });
  //
  // jQuery.each(jQuery('#thirdChart'), function (){
  //   var thirdCtx = document.getElementById('thirdChart').getContext('2d');
  //   var thirdChart = new Chart(thirdCtx, {
  //     type: 'doughnut',
  //     data: {
  //       datasets: [{
  //         data: [6, 25, 27, 42],
  //         backgroundColor: ['rgba(254, 198, 13, 1)', 'rgba(23, 63, 138, 1)', 'rgba(227, 174, 0, 1)', 'rgba(1, 118, 222, 1)'],
  //         borderColor: ['rgba(254, 198, 13, 1)', 'rgba(23, 63, 138, 1)', 'rgba(227, 174, 0, 1)', 'rgba(1, 118, 222, 1)'],
  //       }],
  //       // These labels appear in the legend and in the tooltips when hovering different arcs
  //       labels: [
  //         'Lorem ipsum',
  //         'Dolor sit',
  //         'Amet',
  //         'Consectetur'
  //       ]
  //     },
  //     options: {
  //       scales: {
  //         yAxes: [{
  //           display: false,
  //           ticks: {
  //             beginAtZero: true
  //           }
  //         }]
  //       },
  //       responsive: true,
  //       maintainAspectRatio: false,
  //       legend: {
  //         align: 'start',
  //         position: 'bottom',
  //         labels: {
  //           boxWidth: 20,
  //         }
  //       }
  //     }
  //   });
  // });
  //
  // jQuery.each(jQuery('#fourthChart'), function (){
  //   var fourthCtx = document.getElementById('fourthChart').getContext('2d');
  //   var fourthChart = new Chart(fourthCtx, {
  //     type: 'doughnut',
  //     data: {
  //       datasets: [{
  //         data: [6, 25, 27, 42],
  //         backgroundColor: ['rgba(254, 198, 13, 1)', 'rgba(23, 63, 138, 1)', 'rgba(227, 174, 0, 1)', 'rgba(1, 118, 222, 1)'],
  //         borderColor: ['rgba(254, 198, 13, 1)', 'rgba(23, 63, 138, 1)', 'rgba(227, 174, 0, 1)', 'rgba(1, 118, 222, 1)'],
  //       }],
  //       // These labels appear in the legend and in the tooltips when hovering different arcs
  //       labels: [
  //         'Lorem ipsum',
  //         'Dolor sit',
  //         'Amet',
  //         'Consectetur'
  //       ]
  //     },
  //     options: {
  //       scales: {
  //         yAxes: [{
  //           display: false,
  //           ticks: {
  //             beginAtZero: true
  //           }
  //         }]
  //       },
  //       responsive: true,
  //       maintainAspectRatio: false,
  //       legend: {
  //         align: 'start',
  //         position: 'bottom',
  //         labels: {
  //           boxWidth: 20,
  //         }
  //       }
  //     }
  //   });
  // });
})
